package com.homepocket.hyumall.mallcustomer.config;

import org.springframework.amqp.core.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author Liang
 * @date 2020/11/23 17:19
 **/
@Configuration
public class WithdrawMqConfig {
    public final static String DELAY_QUEUE_PER_QUEUE_TTL_NAME = "withdraw_delay_queue_ttl";

    public final static String DELAY_PROCESS_QUEUE_NAME = "withdraw_delay_queue";

    public final static String DELAY_EXCHANGE_NAME = "withdraw_delay_exchange";

    public final static String DELAY_QUEUE_PER_MESSAGE_TTL_NAME = "withdraw_delay_message_ttl";

    public final static String DELAY_PROCESS_MESSAGE_NAME = "withdraw_delay_message";

    public final static String DELAY_PROCESS_EXCHANGE_NAME = "withdraw_delay_message_exchange";

    public final static int QUEUE_EXPIRATION = 10000;

    // ==========================延时队列========================
    @Bean
    Queue delayQueuePerQueueTTL() {
        return QueueBuilder.durable(DELAY_QUEUE_PER_QUEUE_TTL_NAME)
                .withArgument("x-dead-letter-exchange", DELAY_EXCHANGE_NAME) // DLX
                .withArgument("x-dead-letter-routing-key", DELAY_PROCESS_QUEUE_NAME) // dead letter携带的routing key
                .withArgument("x-message-ttl", QUEUE_EXPIRATION) // 设置队列的过期时间
                .build();
    }


    @Bean
    Queue delayProcessQueue() {
        return QueueBuilder.durable(DELAY_PROCESS_QUEUE_NAME)
                .build();
    }

    @Bean
    DirectExchange delayExchange() {
        return new DirectExchange(DELAY_EXCHANGE_NAME);
    }

    @Bean
    Binding dlxBinding(Queue delayProcessQueue, DirectExchange delayExchange) {
        return BindingBuilder.bind(delayProcessQueue)
                .to(delayExchange)
                .with(DELAY_PROCESS_QUEUE_NAME);
    }

    // ==========================延时消息========================
    @Bean
    Queue delayMessageQueueTTL() {
        return QueueBuilder.durable(DELAY_QUEUE_PER_MESSAGE_TTL_NAME)
                .withArgument("x-dead-letter-exchange", DELAY_PROCESS_EXCHANGE_NAME) // DLX
                .withArgument("x-dead-letter-routing-key", DELAY_PROCESS_MESSAGE_NAME) // dead letter携带的routing key
                .build();
    }

    @Bean
    Queue delayMessageQueue() {
        return QueueBuilder.durable(DELAY_PROCESS_MESSAGE_NAME)
                .build();
    }

    @Bean
    DirectExchange delayMessageExchange() {
        return new DirectExchange(DELAY_PROCESS_EXCHANGE_NAME);
    }

    @Bean
    Binding dlmBinding(Queue delayMessageQueue, DirectExchange delayMessageExchange) {
        return BindingBuilder.bind(delayMessageQueue)
                .to(delayMessageExchange)
                .with(DELAY_PROCESS_MESSAGE_NAME);
    }

}
