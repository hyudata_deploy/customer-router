package com.homepocket.hyumall.mallcustomer.config;

import org.springframework.amqp.AmqpException;
import org.springframework.amqp.core.Correlation;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessagePostProcessor;

/**
 * @author Liang
 * @date 2020/11/24 11:11
 **/
public class ExpirationMessagePostProcessor implements MessagePostProcessor {

    private final String ttl;

    public ExpirationMessagePostProcessor(String ttl) {
        this.ttl = ttl;
    }

    @Override
    public Message postProcessMessage(Message message) throws AmqpException {
        message.getMessageProperties().setExpiration(ttl);
        return message;
    }

    @Override
    public Message postProcessMessage(Message message, Correlation correlation) {
        return null;
    }
}
