package com.homepocket.hyumall.mallcustomer.adapter;

import com.homepocket.hyumall.mallcustomer.interceptor.AccessLimtInterceptor;
import com.homepocket.hyumall.mallcustomer.interceptor.RepeatSubmitInterceptor;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * MVC配置
 */
@Configuration
public class WebMvcConfig implements WebMvcConfigurer {

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        System.out.println("repeat拦截器注册了");
        registry.addInterceptor(new AccessLimtInterceptor()).addPathPatterns("/**");
        registry.addInterceptor(new RepeatSubmitInterceptor()).addPathPatterns("/**");
    }
}