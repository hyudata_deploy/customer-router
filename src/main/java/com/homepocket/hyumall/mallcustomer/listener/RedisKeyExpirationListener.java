package com.homepocket.hyumall.mallcustomer.listener;

/**
 * @author Liang
 * @date 2020/8/1 14:28
 **/

import com.homepocket.hyumall.common.enums.CoinTypeEnum;
import com.homepocket.hyumall.common.enums.MallJPushEnum;
import com.homepocket.hyumall.common.enums.OrderStatusEnum;
import com.homepocket.hyumall.common.enums.RedisKeyEnum;
import com.homepocket.hyumall.common.vo.order.OrderDetailVo;
import com.homepocket.hyumall.mallcustomer.apiservice.OrderService;
import com.homepocket.hyumall.mallcustomer.apiservice.PushService;
import com.homepocket.hyumall.mallcustomer.apiservice.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.connection.Message;
import org.springframework.data.redis.listener.KeyExpirationEventMessageListener;
import org.springframework.data.redis.listener.RedisMessageListenerContainer;
import org.springframework.stereotype.Component;

//@Component
public class RedisKeyExpirationListener extends KeyExpirationEventMessageListener {

    @Autowired
    private OrderService orderService;

    @Autowired
    private PushService pushService;

    @Autowired
    private UserService userService;

    public RedisKeyExpirationListener(RedisMessageListenerContainer listenerContainer) {
        super(listenerContainer);
    }

    @Override
    public void onMessage(Message message, byte[] pattern) {
        // 用户做自己的业务处理即可,注意message.toString()可以获取失效的key
        String expiredKey = message.toString();
        System.out.println("===================过期的key为 : [" + expiredKey + "] 即将进行处理=====================");
        if (expiredKey != null && expiredKey.length() > 2) {
            String status = expiredKey.substring(0, 2);
            RedisKeyEnum byMsg = RedisKeyEnum.getByMsg(status);
            if (byMsg == null) {
                byMsg = RedisKeyEnum.ERRORKEY;
            }
            switch (byMsg) {
                case ORDEREXPIREKEY:
                    // 订单到期
                    String orderId = expiredKey.substring(2);
                    orderService.changeOrderStatus(orderId, Integer.parseInt(OrderStatusEnum.TIMEOU_CANCEL_ORDER.getStatus()));
                    // 回滚库存
                    orderService.rollBackStock(orderId);
                    OrderDetailVo vo = orderService.getOrderDetailById(orderId);
                    if (vo.getCoinNum() > 0) {
                        userService.giveCoins(vo.getUserId(), vo.getCoinNum(), CoinTypeEnum.CANCEL_RETURN.getCode());
                    }
                    break;
                case GROUPEXPIREKEY:
                    // 团购到期
                    String groupId = expiredKey.substring(2);
                    String i = orderService.groupExpire(groupId);
                    if (!"2".equals(i) && !"1".equals(i)) {
                        OrderDetailVo vo1 = orderService.getOrderDetailById(i);
                        if (vo1.getCoinNum() > 0) {
                            userService.giveCoins(vo1.getUserId(), vo1.getCoinNum(), CoinTypeEnum.CANCEL_RETURN.getCode());
                        }
                    }
                    break;
                case REMINDEXPIREKEY:
                    // 提醒我到期
                    String userId = expiredKey.substring(2, 38);
                    System.out.println("薅羊毛预约到期===========" + userId);
                    String productId = expiredKey.substring(38);
                    pushService.mallJPush(userId, userId,null, productId, MallJPushEnum.PRODUCT_EXAMINE_REJECT.getIndex());
                    break;
                case COMMENTEXPIREKEY:
                    // TODO 评论到期
                    break;
                default:
                    break;
            }
        }
    }


}

