package com.homepocket.hyumall.mallcustomer.aspect;

import com.homepocket.hyumall.common.vo.user.UserFootPrintVo;
import com.homepocket.hyumall.mallcustomer.apiservice.UserService;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
/**
 * @author Liang
 * @date 2020/7/15 13:09
 **/
@Aspect
@Component
public class FootprintAspect {

    @Autowired
    private UserService service;

    /**
     * 前置通知：目标方法执行之前执行以下方法体的内容
     * @param proceedingJoinPoint
     */
    @Around("execution(* com.homepocket.hyumall.mallcustomer.apicontroller.ProductController.getProductById(..))")
    public Object beforeMethod(ProceedingJoinPoint proceedingJoinPoint){
        Object[] args = proceedingJoinPoint.getArgs();
        Object preuserId = args[0];
        if (preuserId != null) {
            String userId = preuserId.toString();
            if (userId != null) {
                String productId = args[1].toString();
                UserFootPrintVo entity = new UserFootPrintVo();
                entity.setDeleteFlag("1");
                entity.setProductId(productId);
                entity.setUserId(userId);
                service.modifyFootPrint(entity);
                service.saveFootPrint(entity);
            }
        }
        Object proceed = null;
        try {
            proceed = proceedingJoinPoint.proceed(args);
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
        return proceed;
    }

}

