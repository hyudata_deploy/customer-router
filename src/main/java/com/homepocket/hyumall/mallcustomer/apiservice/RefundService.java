package com.homepocket.hyumall.mallcustomer.apiservice;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name = "mall-order")
public interface RefundService {

    @GetMapping("findOrderRefund")
    Object findOrderRefund(@RequestParam Integer pageSize,@RequestParam Integer pageNum,@RequestParam String status,@RequestParam String userId);

    @GetMapping("getOrderRefundById")
    Object getOrderRefundById(@RequestParam String refundId);
}
