package com.homepocket.hyumall.mallcustomer.apiservice;

import com.homepocket.hyumall.common.entity.ali.AliPayEntity;
import com.homepocket.hyumall.common.vo.aliRefund.AliRefundVo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * @author Liang
 * @date 2020/11/5 11:03
 **/
@FeignClient(name = "mall-ali")
public interface AliService {

    @PostMapping("aliPay")
    String alipay(@RequestBody AliPayEntity aliPayEntity);

    @PostMapping("aliRefund")
    Boolean aliRefund(@RequestBody AliRefundVo aliRefundVo);
}
