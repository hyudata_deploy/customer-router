package com.homepocket.hyumall.mallcustomer.apiservice;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @Author Liang
 * @Date 2020/8/25 15:19
 * @Description
 **/
@FeignClient(name = "home-common")
public interface PushService {

    @GetMapping("mallJPush")
    void mallJPush(@RequestParam String fromId, @RequestParam String toId, @RequestParam String orderId,
                   @RequestParam String productId, @RequestParam Integer status);

    @GetMapping("sellJPush")
    void sellJPush(@RequestParam String fromId, @RequestParam String toId, @RequestParam String orderId,
                   @RequestParam String productId, @RequestParam Integer status);
    

}
