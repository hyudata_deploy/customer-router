package com.homepocket.hyumall.mallcustomer.apiservice;

import com.homepocket.hyumall.common.vo.pay.WxPreOrderVo;
import com.homepocket.hyumall.common.wxpay.WxProfitVo;
import com.homepocket.hyumall.common.wxpay.WxRefundApplyVo;
import com.homepocket.hyumall.common.wxpay.WxWithDrawVo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * @author Liang
 * @date 2020/11/3 10:42
 **/
@FeignClient(name = "mall-wechat")
public interface WechatService {

    @PostMapping("createPreOrder")
    Object createPreOrder(@RequestBody WxPreOrderVo preOrderVo);

    @PostMapping("createApiPreOrder")
    Object createApiPreOrder(@RequestBody WxPreOrderVo preOrderVo);

    @PostMapping("refundApply")
    String refundApply(@RequestBody WxRefundApplyVo refundApplyVo);

    @PostMapping("profitSharing")
    String profitSharing(@RequestBody WxProfitVo profitVo);

    @PostMapping("withdrawApply")
    String withdrawApply(@RequestBody WxWithDrawVo withDrawVo);
}
