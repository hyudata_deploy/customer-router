package com.homepocket.hyumall.mallcustomer.apiservice;

import com.homepocket.hyumall.common.domain.*;
import com.homepocket.hyumall.common.vo.UserCommonDetailsVo;
import com.homepocket.hyumall.common.vo.UserFollowShopDetailVo;
import com.homepocket.hyumall.common.vo.order.CoinRuleVo;
import com.homepocket.hyumall.common.vo.relationship.AddInviteListVo;
import com.homepocket.hyumall.common.vo.relationship.RelationTagVo;
import com.homepocket.hyumall.common.vo.user.*;
import com.homepocket.hyumall.mallcustomer.domain.UserEntity;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * @Author Anson
 * @Date 2020/6/18
 * @Description
 **/
@FeignClient(name = "mall-user")
public interface UserService {


    /**
     * 用户通过手机号发送验证码
     *
     * @param mobile
     * @param type   type = 1 用户登录发送验证码
     *               type = 2 用户修改手机号原发送验证码
     *               type = 3 用户修改手机号新手机号发送验证码
     *               type = 4 用户新增支付密码发送验证码
     *               type = 5 用户修改支付密码发送验证码
     * @return
     */
    @GetMapping(value = "userCaptchaSend")
    AjaxResult userCaptchaSend(@RequestParam(required = false) String userId, @RequestParam(required = false) String mobile, @RequestParam String type);


    /**
     * 校验用户发送的验证码是否有效，是否过期
     *
     * @param userId
     * @param mobile
     * @param captcha
     * @param checkType
     * @return
     */
    @PostMapping(value = "checkMobleCaptcha")
    AjaxResult checkMobleCaptcha(@RequestParam String userId, @RequestParam(required = false) String mobile, @RequestParam String captcha, @RequestParam String checkType);

    /**
     * 用户手机号并发送验证码
     *
     * @param mobile
     * @param password
     * @return
     */
    @GetMapping(value = "/userMobileCheck")
    AjaxResult userMobileCheck(@RequestParam String mobile, @RequestParam String password);

    /**
     * liyang
     * 验证用户手机号与验证码是否正确，如正确则进行登录，注册信息
     *
     * @param mobile
     * @param captcha
     * @return
     */
    @PostMapping(value = "/doLogin")
    UserCommonDetailsVo doLogin(@RequestParam String mobile, @RequestParam String captcha,
                                @RequestParam String openId, @RequestParam String unionId,
                                @RequestParam String type);

    /**
     * liyang
     * 根据用户ID返回用户所有的收货地址
     *
     * @param userId
     * @return
     */
    @GetMapping(value = "/findUserAddrAll")
    List<UserAddrVo> findUserAddrAll(@RequestParam String userId);


    /**
     * liyang
     * 根据用户ID,查询用户详细信息
     *
     * @param userId
     * @return
     */
    @GetMapping(value = "/findUserDetail")
     UserCommonDetailsVo findUserDetail(@RequestParam String userId);

    /**
     * 新增或修改收货地址
     * 根据主键ID判断,为空则新增，不空则修改
     *
     * @param userId      用户ID
     * @param name        收货人昵称
     * @param mobile      收货人电话号
     * @param province    收货人所属省
     * @param city        市
     * @param area        区
     * @param area        县
     * @param defaultFlag 是否为默认收货地址
     * @param id          收货地址主键ID,如果传入则为修改方法
     * @return
     */
    @PostMapping(value = "/saveUserAddr")
    UserAddress saveUserAddr(@RequestParam String userId,
                             @RequestParam String name,
                             @RequestParam String mobile,
                             @RequestParam String province,
                             @RequestParam String city,
                             @RequestParam String area,
                             @RequestParam String county,
                             @RequestParam String detailAddr,
                             @RequestParam String defaultFlag,
                             @RequestParam String id
    );


    /**
     * 删除用户收货地址
     *
     * @param id
     * @return
     */
    @GetMapping(value = "/deleteUserAddr")
    boolean deleteUserAddr(@RequestParam String id);

    /**
     * 用户修改手机号查询用户手机并加密返回前台方法modifyMobileNumCaptcha
     *
     * @param userId
     * @return
     */
    @GetMapping(value = "/findModilyPhoneNum")
    UserAccontVo findModilyPhoneNum(@RequestParam String userId);

    /**
     * 验证修改手机号的验证码，如正确，则进行手机信息修改
     *
     * @param userId
     * @param mobile
     * @return
     */
    @GetMapping(value = "binderNewMobileCheck")
    AjaxResult binderNewMobileCheck(@RequestParam String userId, @RequestParam String mobile);

    /**
     * 新增用户关注店铺信息
     *
     * @return
     */
    @PostMapping(value = "insertUserShop")
    UserShopRelationVo insertUserShop(@RequestParam String userId, @RequestParam String businessId);

    /**
     * 用户关注店铺，取消关注
     *
     * @return
     */
    @PostMapping(value = "/deleteUserShop")
    boolean deleteUserShop(@RequestParam String userId, @RequestParam String businessId);

    /**
     * liyang
     * 查询用户关注店铺分页方法
     *
     * @param userId
     * @param pageSize
     * @param pageNum
     * @return
     */
    @GetMapping(value = "getUserShopAll")
    BasePage<UserFollowShopDetailVo> getUserShopAll(@RequestParam String userId,
                                                           @RequestParam Integer pageSize,
                                                           @RequestParam Integer pageNum);


    /**
     * 查询轮询图方法返回集合
     *
     * @param bannerType
     * @return
     */
    @GetMapping(value = "findBannerPage")
    List<BannerPicVo> findBannerPage(@RequestParam String bannerType);

    /**
     * 保存用户支付密码
     *
     * @param userId
     * @param password
     * @return
     */
    @PostMapping(value = "savePayPassword")
    AjaxResult savePayPassword(@RequestParam String userId, @RequestParam String password, @RequestParam String uniqueKey);

    /**
     * 校验输入的密码是否与数据库原密码一致
     *
     * @param userId
     * @param password
     * @return
     */
    @PostMapping(value = "/checkPayPassword")
     AjaxResult checkPayPassword(@RequestParam String userId, @RequestParam String password);


    /**
     * 修改支付密码
     *
     * @param userId
     * @param password
     * @return
     */
    @PostMapping(value = "/updatePayPassword")
     AjaxResult updatePayPassword(@RequestParam String userId, @RequestParam String oldPassword, @RequestParam String password);


    /**
     * 用户个人资料录入方法
     *
     * @param picId
     * @param nickname
     * @param sex
     * @return
     */
    @PostMapping(value = "/userData")
     AjaxResult userData(
             @RequestBody String userId, @RequestParam(required = false) String picId,
             @RequestParam(required = false) String nickname, @RequestParam(required = false) String sex,
             @RequestParam(required = false) String homeBack, @RequestParam(required = false) String autograph,
             @RequestParam(required = false) String openId, @RequestParam(required = false) String unionId
    );


    /**
     * 查询用户详情方法
     *
     * @param userId
     * @return
     */
    @GetMapping(value = "/findUserAndDetail")
     AjaxResult findUserDetailAndUser(@RequestParam String userId);


    @GetMapping("getPayPwd")
    int getPayPwd(@RequestParam String userId);

    /**
     * 查询用户足迹分页方法
     *
     * @param pageSize
     * @param pageNum
     * @return
     */
    @GetMapping(value = "/footPrintPage")
     BasePage<FootPrintVo> footPrintPage(@RequestParam String userId, @RequestParam Integer pageSize,
                                               @RequestParam Integer pageNum);


    /**
     * 根据用户ID,查询用户首页信息详情
     *
     * @param userId
     * @return
     */
    @GetMapping(value = "findUserFirstPageData")
     UserFirstPageVo findUserFirstPageData(@RequestParam String userId);


    /**
     * 新增用户意见反馈信息
     *
     * @param userId
     * @param idea
     * @param contact
     * @return
     */
    @PostMapping(value = "saveFeedBackData")
     boolean saveFeedBackData(@RequestParam(required = false) String userId, @RequestParam String idea, @RequestParam(required = false) String contact);

    /**
     * 根据用户信息查询意见反馈信息
     *
     * @param userId
     * @return
     */
    @GetMapping(value = "findFeedBack")
     UserFeedBackVo findFeedBack(@RequestParam String userId);


    /**
     * 修改用户反馈信息
     *
     * @param userId
     * @param idea
     * @return
     */
    @PostMapping(value = "updateFeedBack")
     boolean updateFeedBack(@RequestParam String userId, @RequestParam(required = false) String idea, @RequestParam(required = false) String contact);


    @PostMapping(value = "deleteFeedBack")
     boolean deleteFeedBack(@RequestParam String userId);


    /**
     * 省市区县三级菜单查询
     *
     * @param version
     * @return
     */
    @GetMapping(value = "findAllCityTree")
     AllCityVersionVo findAllCityTree(@RequestParam(required = false) String version);


    /**
     * 根据收货地址ID,查询收货地址
     *
     * @param id
     * @return
     */
    @GetMapping(value = "findUserAddrById")
     UserAddrVo findUserAddrById(@RequestParam String id);

    @PostMapping("userMobileRegister")
    UserEntity userMobileRegister(@RequestParam String mobile, @RequestParam String password);

    @PostMapping("userMobilLogin")
    UserEntity userMobileLogin(@RequestParam String mobile, @RequestParam String password);

    @PostMapping("getUserByMobile")
    UserEntity getUserByMobile(@RequestParam String mobile);

    @GetMapping("getUserById")
    UserEntity getUserById(@RequestParam String id);

    @PostMapping("modifyUserPassword")
    Integer modifyUserPassword(@RequestParam String id, @RequestParam String pass);

    @PostMapping("modifyUserInfo")
    UserEntity modifyUserInfo(@RequestParam UserEntity user);


    /**
     * 获取亲情号筛选条件(已同意用户)
     *
     * @param userId 用户ID
     * @return 昵称和ID的list
     */
    @GetMapping("getRelationshipsByUserId")
    AjaxResult getRelationshipsByUserId(@RequestParam String userId);


    /**
     * 创建新情组
     *
     * @param userId 用户ID
     * @return
     */
    @PostMapping("addRelationshipGroup")
    AjaxResult addRelationshipGroup(@RequestBody String userId);

    /**
     * 获取亲情号家庭组
     *
     * @param userId 用户ID
     * @return 亲情号家庭组和成员列表
     */
    @GetMapping("getRelationGroupByUserId")
     AjaxResult getRelationGroupByUserId(@RequestParam String userId);

    /**
     * 获取标签列表
     *
     * @param userId 用户ID
     * @return 标签列表
     */
    @GetMapping("getRelationTags")
     AjaxResult getRelationTags(@RequestParam String userId);

    @PostMapping("addInviteList")
     AjaxResult addInviteList(@RequestBody AddInviteListVo addInviteListVo, @RequestParam String userId);

    @GetMapping("listInvitedInfo")
     AjaxResult listInvitedInfo(@RequestParam String userId);

    @PostMapping("acceptInvite")
     AjaxResult acceptInvite(@RequestBody String itemId, @RequestParam String userId);

    @PostMapping("refuseInvite")
     AjaxResult refuseInvite(@RequestBody String itemId, @RequestParam String userId);

    @PostMapping("quitAndJoin")
     AjaxResult quitAndJoin(@RequestBody String itemId, @RequestParam String userId);

    @PostMapping("cancelInvite")
     AjaxResult cancelInvite(@RequestBody String itemId, @RequestParam String userId);

    @PostMapping("deleteInvite")
     AjaxResult deleteInvite(@RequestBody String itemId, @RequestParam String userId);

    @PostMapping("expelRelation")
     AjaxResult expelRelation(@RequestBody String itemId, @RequestParam String userId);

    @PostMapping("modifyRelation")
     AjaxResult modifyRelation(@RequestBody RelationTagVo relationTagVo, @RequestParam String userId);

    /**
     * 用户足迹新增方法
     * 传入字段为，userId,productId
     *
     * @param vo
     * @return
     */
    @PostMapping(value = "saveFootPrint")
    int saveFootPrint(@RequestBody UserFootPrintVo vo);

    @PostMapping("modifyFootPrint")
    void modifyFootPrint(@RequestBody UserFootPrintVo vo);

    @GetMapping("getCoinRules")
    List<CoinRuleVo> getCoinRules();

    @GetMapping("getParamsByKey")
    String getParamsByKey(@RequestParam String paramKey);

    /**
     * 获取user详情
     * @param userId
     * @return
     */
    @GetMapping("findTokenUserDetail")
    UserCommonDetailsVo findTokenUserDetail(@RequestParam String userId);

    @GetMapping("findUserAccontByMobile")
    boolean findUserAccontByMobile(@RequestParam String mobile);

    @GetMapping("updateUserAccount")
    boolean updateUserAccount(@RequestParam String userId,@RequestParam String mobile);

    @PostMapping(value = "/userPicUpload")
    AjaxResult userPicUpload(@RequestParam String path , @RequestParam String userId);

    @PostMapping(value = "userBackUpload")
    AjaxResult userBackUpload(@RequestParam String path, @RequestParam String userId);

    @PostMapping(value = "updateUserCoins")
    int updateUserCoins(@RequestBody String userId , @RequestParam Integer money);

    @GetMapping("getUserMessageList")
    BasePage<PushHistory> getUserMessageList(
            @RequestParam Integer pageSize,  @RequestParam Integer pageNum,  @RequestParam String userId);

    @PostMapping("giveCoins")
    boolean giveCoins(@RequestBody String userId, @RequestParam Integer num, @RequestParam Integer type);

    @GetMapping("getHistoryByUserId")
    BasePage<CoinHistoryVo> getHistoryByUserId(
            @RequestParam Integer pageSize,  @RequestParam Integer pageNum, @RequestParam String userId);

    @PostMapping("toLoginOther")
    AjaxResult toLoginOther(@RequestBody String openId,  @RequestParam String unionId,  @RequestParam String type,
                            @RequestParam String method);

    @PostMapping("unbindOther")
    AjaxResult unbindOther(@RequestBody String userId, @RequestParam String type);
}