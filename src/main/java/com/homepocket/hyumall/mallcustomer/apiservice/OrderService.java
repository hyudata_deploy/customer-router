package com.homepocket.hyumall.mallcustomer.apiservice;

import com.homepocket.hyumall.common.domain.AjaxResult;
import com.homepocket.hyumall.common.domain.BasePage;
import com.homepocket.hyumall.common.domain.*;
import com.homepocket.hyumall.common.vo.GroupUserVo;
import com.homepocket.hyumall.common.vo.IdNameVo;
import com.homepocket.hyumall.common.vo.OrderStatusVo;
import com.homepocket.hyumall.common.vo.order.*;
import com.homepocket.hyumall.common.vo.pay.CombinePrePayVo;
import com.homepocket.hyumall.common.vo.pay.PayResponseVo;
import com.homepocket.hyumall.common.vo.pay.PayVo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;


/**
 * @Author demon
 * @Date 2020/7/16
 * @Description
 **/
@FeignClient(name = "mall-order")
public interface OrderService {

    @PostMapping("addOrder")
    PayVo addOrder(
            @RequestBody CalculateVo vo,
            @RequestParam String userId);

    @GetMapping("findOrderAll")
    BasePage<ROrderListVo> findOrderAll(@RequestParam String userId, @RequestParam String orderStatus,
                                        @RequestParam Integer pageNum,
                                        @RequestParam Integer pageSize);

    @PostMapping("changeOrder")
    int changeOrder(@RequestBody String orderId , @RequestParam Integer status ,
                                 @RequestParam String message);

    @PostMapping("deleteOrder")
    int deleteOrder(@RequestBody String orderId);

    @PostMapping("deleteRefundApply")
    int deleteRefundApply(@RequestBody String refundNo);

    @PostMapping("changeOrderStatus")
    int changeOrderStatus(@RequestBody String orderId , @RequestParam Integer status);

    @GetMapping("cancelReason")
    List<String> cancelReason();

    @GetMapping("findOrderProductVo")
    OrderProductVo findOrderProductVo(@RequestParam String orderId);

    @GetMapping("getOrderDetailById")
    OrderDetailVo getOrderDetailById(@RequestParam String orderId);

    @GetMapping("getOrderDetailReturnVoById")
    OrderDetailReturnVo getOrderDetailReturnVoById(@RequestParam String orderId);

    @PostMapping("dealOrder")
    int dealOrder(@RequestBody PayResponseVo vo);

    @GetMapping("findOrderStatus")
    List<OrderStatusVo> findOrderStatus();

    @GetMapping("groupExpire")
    String groupExpire(@RequestParam String groupId);

    @PostMapping("commentOrder")
    int commentOrder(@RequestBody List<CommentVo> commentVoList);

    @PostMapping("changeRefundStatus")
    int changeRefundStatus(@RequestBody String orderId, @RequestParam String refundId, @RequestParam Integer refundFee);

    @GetMapping("getProductSnapshots")
    ProductSnapshotsVo getProductSnapshots(@RequestParam String orderProductId);

    @PostMapping("rollBackStock")
    void rollBackStock(@RequestBody String orderId);

    @PostMapping("cancelOrder")
    int cancelOrder (@RequestBody String orderId);

    @GetMapping("getProductGroup")
    List<GroupUserVo> getProductGroup(@RequestParam String productId);

    @PostMapping("addShopWater")
    int addShopWater(@RequestBody ShopWater shopWater);

    @GetMapping("getShopWaterByOrderId")
    ShopWater getShopWaterByOrderId(@RequestParam String orderId);

    @PostMapping("addTransport")
    int addTransport(@RequestBody String orderId);

    @GetMapping("getTransport")
    List<OrderTransport> getTransport(@RequestParam String orderId);

    @GetMapping("getTransportProduct")
    List<OrderTransportProduct> getTransportProduct(@RequestParam String orderId);

    @GetMapping("getReportTypeList")
    List<IdNameVo> getReportTypeList();

    @PostMapping("addReport")
    int addReport(
            @RequestBody String commentId, @RequestParam String userId,
            @RequestParam String name, @RequestParam String remark);

    @PostMapping("addCartProduct")
    int addCartProduct(@RequestBody String productId, @RequestParam String shopId, @RequestParam String sizeDetailId, @RequestParam Integer num, @RequestParam Integer cartPrice, @RequestParam String userId);

    @GetMapping("getCartDetail")
    CartVo getCartDetail(@RequestParam String userId);

    @GetMapping("delCartProduct")
    int delCartProduct(@RequestParam String cartProductId, @RequestParam String userId);

    @PostMapping("updateCartProduct")
    int updateCartProduct(@RequestBody String cartProductId, @RequestParam Integer num);

    @PostMapping("updateCartSize")
    int updateCartSize(@RequestBody String cartProductId,
                       @RequestParam String sizeDetailId,
                       @RequestParam Integer cartPrice,
                       @RequestParam String userId
    );

    @GetMapping("syncCart")
    int syncCart(@RequestParam String formId, @RequestParam String toId);

    @GetMapping("getOrderDetailByParentOrderId")
    CombinePrePayVo getCombinePrePayOrder(@RequestParam String orderId);

    @GetMapping ("getOrderProductByOrderId")
    List<ProductVo> getOrderProductByOrderId(@RequestParam String orderId);

    @GetMapping ("getTransportsList")
    AjaxResult getTransportsList(@RequestParam String orderId);

    @GetMapping("getRefundsByOrderId")
    List<OrderRefund> getRefundsByOrderId(@RequestParam String orderId);

    @PostMapping("addRefund")
    int addRefund(@RequestBody OrderRefund entity);

    @PostMapping("addRefundMethod")
    int addRefundMethod(@RequestBody RefundMethod entity);

    @GetMapping("getOrderEntity")
    Order getOrderEntity(@RequestParam String orderId);

    @PostMapping("addWithdrawHistory")
    int addWithdrawHistory(@RequestBody WithdrawHistory withdrawEntity);

}
