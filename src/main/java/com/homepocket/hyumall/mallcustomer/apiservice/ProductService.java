package com.homepocket.hyumall.mallcustomer.apiservice;

import com.homepocket.hyumall.common.domain.*;
import com.homepocket.hyumall.common.vo.*;
import com.homepocket.hyumall.common.vo.bookmark.BookmarkProductVo;
import com.homepocket.hyumall.common.vo.brand.BrandBusinessVo;
import com.homepocket.hyumall.common.vo.brand.BrandDetailVo;
import com.homepocket.hyumall.common.vo.city.CityVersionVo;
import com.homepocket.hyumall.common.vo.product.ProductCommentVo;
import com.homepocket.hyumall.common.vo.product.ProductDetailVo;
import com.homepocket.hyumall.common.vo.product.ProductViewOnsaleVo;
import com.homepocket.hyumall.common.vo.product.SizePriceVo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * @Author Liang
 * @Date 2020/6/16 13:26
 * @Description
 **/
@FeignClient(name = "mall-product")
public interface ProductService {


    @GetMapping("getProductList")
    BasePage<ProductView> getProductList(
            @RequestParam String priceOrder, @RequestParam String boughtOrder,
            @RequestParam String fromPrice, @RequestParam String toPrice,
            @RequestParam String condition,
            @RequestParam String province, @RequestParam String city, @RequestParam String brandId,
            @RequestParam String typeId,
            @RequestParam String categoryId, @RequestParam String centerId, @RequestParam String businessId,
            @RequestParam String searchName, @RequestParam Integer pageSize, @RequestParam Integer pageNum);

//    @GetMapping("getProductListByBusinessId")
//    BasePage<ProductViewEntity> getProductListByBusinessId(@RequestParam String categoryId, @RequestParam String businessId, @RequestParam Integer pageSize, @RequestParam Integer pageNum);

    @GetMapping("getProductById")
    ProductDetailVo getProductById(@RequestParam String productId);

    @GetMapping("getProductCommentByProductId")
    BasePage<ProductCommentVo> getProductCommentByProductId(
            @RequestParam String id,  @RequestParam String type,
            @RequestParam Integer pageSize, @RequestParam Integer pageNum);

    @GetMapping("getProductOnSaleList")
    BasePage<ProductViewOnsaleVo> getProductOnSaleList(
            @RequestParam String province,@RequestParam  String city,
            @RequestParam String type, @RequestParam String status,
            @RequestParam Integer pageSize, @RequestParam Integer pageNum);

//    @GetMapping("getTopProductPicByBusinessId")
//    BasePage<TopProductPicViewEntity> getTopProductPicByBusinessId(@RequestParam String businessId, @RequestParam Integer pageSize, @RequestParam Integer pageNum);

    @GetMapping("getIndexProducts")
    IndexPageVo getIndexProducts(
            @RequestParam String province, @RequestParam  String city, @RequestParam  String categoryId);

    @GetMapping("getHotSearchList")
    List<HotSearch> getHotSearchList();


    @GetMapping("getCenterListByIds")
    List<BusinessCenter> getCenterListByIds(@RequestParam String ids);

    @GetMapping("getBrandListByIds")
    List<Brand> getBrandListByIds(@RequestParam String ids);

    @GetMapping("getCategoryListByIds")
    List<ProductCategory> getCategoryListByIds(@RequestParam String ids);


    @GetMapping("getSearchList")
    List<ConditionVo> getSearchList(
            @RequestParam  String province, @RequestParam String city,@RequestParam String brandId,
            @RequestParam String categoryId,
            @RequestParam String centerId, @RequestParam String businessId,@RequestParam String searchName);

    @GetMapping("getBrandDetailById")
    BrandDetailVo getBrandDetailById( @RequestParam String brandId,@RequestParam String lat,@RequestParam String lng);

    @GetMapping("getCategories")
    List<CategoryVo> getCategories();

    @GetMapping("getParentCategories")
    List<CategoryVo> getParentCategories(@RequestParam String parentId);

    @GetMapping("getBusinessCenterByCityCode")
    List<BusinessCenterVo> getBusinessCenterByCityCode(@RequestParam String province, @RequestParam String city);

    @GetMapping("getBrandByCategoryId")
    BasePage<BrandVo> getBrandByCategoryId(
            @RequestParam String id, @RequestParam Integer pageSize, @RequestParam Integer pageNum);

    @GetMapping("getCountCommentsById")
    List<CommonVo> getCountCommentsById( @RequestParam String id);

    @GetMapping("getOpenedCity")
    CityVersionVo getOpenedCity(@RequestParam String version);

    @GetMapping("getHistoryCityByUserId")
    List<HistoryCity> getHistoryCityByUserId(@RequestParam String userId);

    @PostMapping("addHistoryCity")
    int addHistoryCity(@RequestBody HistoryCity city);

    @GetMapping("getShopsByBrandId")
    BasePage<BrandBusinessVo> getShopsByBrandId(
            @RequestParam String id,
            @RequestParam String lat,@RequestParam String lng,
            @RequestParam String province, @RequestParam String city, @RequestParam String block,
            @RequestParam Integer pageNum, @RequestParam Integer pageSize);

    @GetMapping("getFreightByModel")
    List<ProductFreight> getFreightByModel( 
            @RequestParam String id,
            @RequestParam String province,
            @RequestParam String city,
            @RequestParam String block,
            @RequestParam String country);

    @PostMapping("addBookmark")
    int addBookmark(@RequestBody String userId,  @RequestParam String productId);

    @PostMapping("addBookmarks")
    int addBookmarks(@RequestBody String userId,  @RequestParam String productIds);

    @GetMapping("deleteBookmark")
    int deleteBookmark(@RequestParam String userId,  @RequestParam String bookmarkId);

    @GetMapping("delBookByProductId")
    int delBookByProductId(@RequestParam String userId,  @RequestParam String productIds);

    @GetMapping("getBookmarkProducts")
    BasePage<BookmarkProductVo> getBookmarkProducts(
            @RequestParam String userId,@RequestParam  String categoryId,
            @RequestParam String status,@RequestParam Integer pageSize,
            @RequestParam Integer pageNum);

    @PostMapping("removeBookmark")
    int removeBookmark(@RequestBody String userId,@RequestParam String bookmarkId);

    @PostMapping("recoverBookmark")
    int recoverBookmark(@RequestBody String userId,@RequestParam String bookmarkId,@RequestParam String productId);

    @GetMapping("getSizeById")
    SizePriceVo getSizeById(@RequestParam String sizeId);

    @GetMapping("changeStock")
    int changeStock(@RequestParam String sizeDetailId, @RequestParam Integer changeNum);

    @GetMapping("checkAppointment")
    int checkAppointment(@RequestParam String userId, @RequestParam String businessId);

    @PostMapping("addAppointment")
    int addAppointment(@RequestBody String userId, @RequestParam String productId, @RequestParam String businessId);
}
