package com.homepocket.hyumall.mallcustomer.apiservice;

import com.homepocket.hyumall.common.domain.BasePage;
import com.homepocket.hyumall.common.vo.CategoryVo;
import com.homepocket.hyumall.common.vo.shop.ShopActivityVo;
import com.homepocket.hyumall.common.vo.shop.ShopDetailVo;
import com.homepocket.hyumall.common.vo.shop.ShopLabelVo;
import com.homepocket.hyumall.common.vo.shop.ShopProductPicVo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * @Author Anson
 * @Date 2020/6/17
 * @Description
 **/
@FeignClient(name = "mall-shop")
public interface BusinessService {

    /**
     * 获取店铺详情页面方法
     *
     * @param businessId
     * @return
     */
    @GetMapping("getBusinessById")
    public List<ShopDetailVo> getBusinessById(@RequestParam(required=false) String userId, @RequestParam String businessId);


    /**
     *
     * 根據店鋪ID查詢店鋪推薦商品圖片及商品ID
     * @param businessId
     * @param pageSize
     * @param pageNum
     * @returnfindPropagandaPage
     */
    @GetMapping(value = "findShopRecommPic")
    public BasePage<ShopProductPicVo> findShopRecommPic(@RequestParam String businessId,
                                                        @RequestParam Integer pageSize,
                                                        @RequestParam Integer pageNum);


    /**
     *
     * 根据店铺id查询店铺所属活动及活动图片详情
     * @param businessId
     * @param pageSize
     * @param pageNum
     * @return
     */
    @GetMapping(value = "findPropagandaPage")
    public BasePage<ShopActivityVo> findPropagandaPage(@RequestParam String businessId,
                                                   @RequestParam Integer pageSize,
                                                   @RequestParam Integer pageNum);


    /**
     *
     * 获取店铺商品树表信息
     * @param businessId
     * @return
     */
    @GetMapping(value = "findShopCommodityData")
    public List<CategoryVo> findShopCommodityData(@RequestParam String businessId);



    /**
     *
     * 获取店铺标签信息
     * @param businessId
     * @return
     */
    @GetMapping(value = "getShopLabelData")
    public List<ShopLabelVo> getShopLabelData(@RequestParam String businessId);
//
//    @GetMapping("getBusinessById")
//    BusinessViewEntity getBusinessById(@RequestParam String businessId);
//
//    @GetMapping("getBusinessPageList")
//    BasePage<BusinessViewEntity> getBusinessPageList(@RequestParam String province, @RequestParam String city, @RequestParam String centerId, @RequestParam Integer pageNum, @RequestParam Integer pageSize);
//
//    @GetMapping("getBusinessPics")
//    List<PicEntity> getBusinessPics(@RequestParam String businessId);

}
