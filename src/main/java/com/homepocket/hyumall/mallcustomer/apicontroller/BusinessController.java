package com.homepocket.hyumall.mallcustomer.apicontroller;

import com.homepocket.hyumall.common.annotation.CheckToken;
import com.homepocket.hyumall.common.annotation.TokenUser;
import com.homepocket.hyumall.common.domain.AjaxResult;
import com.homepocket.hyumall.common.domain.BasePage;
import com.homepocket.hyumall.common.vo.shop.ShopDetailVo;
import com.homepocket.hyumall.common.vo.shop.ShopProductPicVo;
import com.homepocket.hyumall.mallcustomer.apiservice.BusinessService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @Author Anson
 * @Date 2020/6/16
 * @Description
 **/
@RestController
public class BusinessController {
    @Autowired
    private BusinessService businessService;

    /**
     * 获取店铺详情页面方法
     *
     * @param businessId
     * @return
     */
    @CheckToken
    @GetMapping("getBusinessById")
    public AjaxResult getBusinessById(@TokenUser String userId,@RequestParam String businessId){
        List<ShopDetailVo> businessById = businessService.getBusinessById(userId, businessId);
        if(businessById != null && businessById.size() > 0){
            return AjaxResult.success("查询数据成功",businessById.get(0));
        }
            return AjaxResult.error("查无此数据");
    }

    /**
     *
     * 根據店鋪ID查詢店鋪推薦商品圖片及商品ID
     * @param businessId
     * @param pageSize
     * @param pageNum
     */
    @GetMapping(value = "findShopRecommPic")
    public AjaxResult findShopRecommPic(@RequestParam String businessId,
                                                @RequestParam(name="pageSize", defaultValue = "10", required = false) Integer pageSize,
                                                @RequestParam(name="pageNum", defaultValue = "1", required = false) Integer pageNum){
        BasePage<ShopProductPicVo> shopRecommPic = businessService.findShopRecommPic(businessId, pageSize, pageNum);
        return AjaxResult.success(shopRecommPic);
    }

    /**
     *
     * 根据店铺id查询店铺所属活动及活动图片详情
     * @param businessId
     * @param pageSize
     * @param pageNum
     * @return
     */
    @GetMapping(value = "findPropagandaPage")
    public AjaxResult findPropagandaPage(@RequestParam String businessId,
                                                 @RequestParam(name="pageSize", defaultValue = "10", required = false) Integer pageSize,
                                                 @RequestParam(name="pageNum", defaultValue = "1", required = false) Integer pageNum){

        return AjaxResult.success(businessService.findPropagandaPage(businessId,pageSize,pageNum));
    }

    /**
     *
     * 获取店铺商品树表信息
     * @param businessId
     * @return
     */
    //@CheckToken
    @GetMapping(value = "findShopCommodityData")
    public AjaxResult findShopCommodityData(/*@TokenUser String userId,*/@RequestParam String businessId){
        return AjaxResult.success(businessService.findShopCommodityData(businessId));
    }

    /**
     *
     * 根据店铺ID，回去它的标签
     * @param userId
     * @param businessId
     * @return
     */
    @CheckToken
    @GetMapping("getShopLabelData")
    public AjaxResult getShopLabelData(@TokenUser String userId,@RequestParam String businessId){
        return AjaxResult.success(businessService.getShopLabelData(businessId));
    }
}
