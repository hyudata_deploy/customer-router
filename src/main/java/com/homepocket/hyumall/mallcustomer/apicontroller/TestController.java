package com.homepocket.hyumall.mallcustomer.apicontroller;

import com.homepocket.hyumall.mallcustomer.config.WithdrawMqConfig;
import com.homepocket.hyumall.mallcustomer.service.ProfitApplyService;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Liang
 * @date 2020/11/24 9:52
 **/
@RestController
public class TestController {

    @Autowired
    private AmqpTemplate amqpTemplate;

    @Autowired
    private ProfitApplyService profitApplyService;

//    @GetMapping("testSendOrderExpire")
//    public void testSendOrderExpire(String orderId) {
//        amqpTemplate.convertAndSend(WithdrawMqConfig.DELAY_QUEUE_PER_QUEUE_TTL_NAME, orderId);
//    }

    @RabbitListener(queues = WithdrawMqConfig.DELAY_PROCESS_QUEUE_NAME)
    @RabbitHandler
    public void receiver(String orderId) {
        System.out.println("收到消息==============" + orderId);
        // TODO 定时任务 确认收货七天后分账
        profitApplyService.profitSharing(orderId);
    }

//    @GetMapping("testSendOrderExpireMessage")
//    public void testSendOrderExpireMessage(String orderId) {
//        amqpTemplate.convertAndSend(WithdrawMqConfig.DELAY_QUEUE_PER_MESSAGE_TTL_NAME, (Object) orderId,
//                new ExpirationMessagePostProcessor("5"));
//    }
//
//    @RabbitListener(queues = WithdrawMqConfig.DELAY_PROCESS_MESSAGE_NAME)
//    @RabbitHandler
//    public void testReceive(String orderId) {
//        System.out.println("收到延时消息==============" + orderId);
//    }
}
