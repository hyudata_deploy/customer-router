package com.homepocket.hyumall.mallcustomer.apicontroller;

import com.homepocket.hyumall.common.annotation.CheckToken;
import com.homepocket.hyumall.common.annotation.TokenRequired;
import com.homepocket.hyumall.common.annotation.TokenUser;
import com.homepocket.hyumall.common.constant.HomConstant;
import com.homepocket.hyumall.common.domain.AjaxResult;
import com.homepocket.hyumall.common.domain.BasePage;
import com.homepocket.hyumall.common.domain.UserAddress;
import com.homepocket.hyumall.common.enums.CaptchaType;
import com.homepocket.hyumall.common.utils.RegisterUtils;
import com.homepocket.hyumall.common.utils.SecretKeyUtil;
import com.homepocket.hyumall.common.utils.SendSmsUtils;
import com.homepocket.hyumall.common.utils.StringUtils;
import com.homepocket.hyumall.common.vo.UserCommonDetailsVo;
import com.homepocket.hyumall.common.vo.UserFollowShopDetailVo;
import com.homepocket.hyumall.common.vo.user.*;
import com.homepocket.hyumall.mallcustomer.apiservice.UserService;
import com.homepocket.hyumall.mallcustomer.domain.UserEntity;
import com.homepocket.hyumall.mallcustomer.utils.RedisUtils;
import com.homepocket.hyumall.mallcustomer.utils.UploadUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Optional;
import java.util.UUID;


@RestController
public class UserController {
    @Autowired
    private UserService userService;

    @Autowired
    private RedisUtils redisUtils;

//    @Autowired
//    private WxMpService wxMpService;

    @Value("${user_filepath}")
    private String filepath;

    /**
     *
     * 用户通过手机号发送验证码
     * @param mobile
     * @param type
     * type = 1 用户登录发送验证码
     * type = 2 用户修改手机号原发送验证码
     * type = 3 用户修改手机号新手机号发送验证码
     * type = 4 用户新增支付密码发送验证码
     * type = 5 用户修改支付密码发送验证码
     * @return
     */
    @CheckToken
    @GetMapping(value = "userCaptchaSend")
    public AjaxResult userCaptchaSend(@TokenUser String userId, @RequestParam(required = HomConstant.FALSE) String mobile, @RequestParam String type){
        boolean flag = HomConstant.FALSE;
        if(!StringUtils.equals(type, HomConstant.ONESTR)){
            //如果token值不为空,则进行查询
            if(StringUtils.isNotEmpty(userId) && StringUtils.isEmpty(mobile)){
                UserCommonDetailsVo tokenUserDetail = userService.findTokenUserDetail(userId);
                mobile = tokenUserDetail.getMobile();
            }else if (StringUtils.isEmpty(userId)){
                return AjaxResult.tokenError();
            }
        }
        Integer verifyCode = SendSmsUtils.getCaptcha();
        if(StringUtils.isNotEmpty(type)){
            boolean checkMobile = HomConstant.FALSE;
            if(StringUtils.isNotEmpty(mobile)){
                checkMobile = RegisterUtils.isPhoneLegal(mobile);
            }
            else {
                return AjaxResult.error(HomConstant.PHONEERRORMESSAGE);
            }
            try {
                if (StringUtils.equals(type, HomConstant.ONESTR)) {//用户登录APP发送验证码
                    redisUtils.set(mobile + CaptchaType.LOGIN.value(), verifyCode, HomConstant.FIVE_DEFAULT_EXPIRE);
                    redisUtils.set(mobile + CaptchaType.LOGIN_TIME.value(), System.currentTimeMillis(), HomConstant.FIVE_DEFAULT_EXPIRE);
                } else if (StringUtils.equals(type, HomConstant.TWOSTR)) {//用户修改手机号原手机号发送验证码
                    redisUtils.set(mobile + CaptchaType.MOD_OLD_PHONE.value(), verifyCode, HomConstant.FIVE_DEFAULT_EXPIRE);
                    redisUtils.set(mobile + CaptchaType.MOD_OLD_PHONE_TIME.value(), System.currentTimeMillis(), HomConstant.FIVE_DEFAULT_EXPIRE);
                } else if (StringUtils.equals(type, HomConstant.THREESTR)) {//用户修改手机号,新手机号发送验证码
                    redisUtils.set(mobile + CaptchaType.MOD_NEW_PHONE, verifyCode, HomConstant.FIVE_DEFAULT_EXPIRE);
                    redisUtils.set(mobile + CaptchaType.MOD_NEW_PHONE_TIME, System.currentTimeMillis(), HomConstant.FIVE_DEFAULT_EXPIRE);
                } else if (StringUtils.equals(type, HomConstant.FourSTR)) {//用户新增支付密码发送验证码
                    redisUtils.set(mobile + CaptchaType.ADD_PASSWORD.value(), verifyCode, HomConstant.FIVE_DEFAULT_EXPIRE);
                    redisUtils.set(mobile + CaptchaType.ADD_PASSWORD_TIME.value(), System.currentTimeMillis(), HomConstant.FIVE_DEFAULT_EXPIRE);
                } else if (StringUtils.equals(type, HomConstant.FIVESTR)) {//用户修改支付密码发送验证码
                    redisUtils.set(mobile + CaptchaType.MOD_PASSWOED.value(), verifyCode, HomConstant.FIVE_DEFAULT_EXPIRE);
                    redisUtils.set(mobile + CaptchaType.MOD_PASSWOED_TIME.value(), System.currentTimeMillis(), HomConstant.FIVE_DEFAULT_EXPIRE);
                }
            }catch (Exception e){
                return AjaxResult.error(HomConstant.SYSERRORMESSAGE);
            }
            if (StringUtils.equals(type, HomConstant.ONESTR)) {//用户登录APP发送验证码
                flag = SendSmsUtils.messageSend(mobile, SendSmsUtils.MessageType.SIGN_IN_SMS.value(), verifyCode);
            } else if (StringUtils.equals(type, HomConstant.TWOSTR)) {//用户修改手机号原手机号发送验证码
                flag = SendSmsUtils.messageSend(mobile, SendSmsUtils.MessageType.ID_CARD_SMS.value(), verifyCode);
            } else if (StringUtils.equals(type, HomConstant.THREESTR)) {//用户修改手机号,新手机号发送验证码
                flag = SendSmsUtils.messageSend(mobile, SendSmsUtils.MessageType.ID_CARD_SMS.value(), verifyCode);
            } else if (StringUtils.equals(type, HomConstant.FourSTR)) {//用户新增支付密码发送验证码
                flag = SendSmsUtils.messageSend(mobile, SendSmsUtils.MessageType.CHANGE_MESSAGE.value(), verifyCode);
            } else if (StringUtils.equals(type, HomConstant.FIVESTR)) {//用户修改支付密码发送验证码
                flag = SendSmsUtils.messageSend(mobile, SendSmsUtils.MessageType.CHANGE_MESSAGE.value(), verifyCode);
            }


        }
        if(flag){
            return AjaxResult.success(HomConstant.VERIFYCODESUCCESSMESSAGE);
        }
        return AjaxResult.error(HomConstant.VERIFYCODEERRORMESSAGE);
    }

    /**
     * 获取用户手机号与发送验证码，进行验证，如为新用户，则进行注册功能
     *
     * @param mobile
     * @param captcha
     * @return
     */
    @PostMapping(value = "/doLogin")
    public AjaxResult doLogin( String mobile, String captcha, String openId, String unionId, String type) {
        String s1 = redisUtils.get("SENDSMS" + mobile);
        if (s1 != null && Integer.parseInt(s1) == 5) {
            return AjaxResult.error("尝试次数过多, 请等待五分钟再试");
        }
        //验证码在redis中的key
        String verifyCode = mobile + CaptchaType.LOGIN.value();
        //验证码生成时间点在redis中的key
        String time = mobile + CaptchaType.LOGIN_TIME;

        String s = redisUtils.get(verifyCode);

        if(StringUtils.isNotEmpty(redisUtils.get(time)) && (System.currentTimeMillis() - Long.parseLong(String.valueOf(redisUtils.get(time))) > 1000 * 60 * 5)){
            redisUtils.delete(verifyCode);
            redisUtils.delete(time);
            return new AjaxResult(AjaxResult.Type.ERROR, "验证码过期");
        }else if (StringUtils.isNotEmpty(redisUtils.get(verifyCode)) && StringUtils.equals(captcha,redisUtils.get(verifyCode))) {
            redisUtils.delete(verifyCode);
            redisUtils.delete(time);

            UserCommonDetailsVo token = userService.doLogin(mobile,captcha,openId,unionId,type);
            if (token == null) {
                return AjaxResult.alreadyMobile();
            }
            redisUtils.set("SENDSMS" + mobile, 0,  HomConstant.FIVE_DEFAULT_EXPIRE );
            return AjaxResult.success("登录成功",token);
        } else if("110110".equals(captcha)) {
            UserCommonDetailsVo token = userService.doLogin(mobile,captcha,openId,unionId,type);
            redisUtils.set("SENDSMS" + mobile, 0,  HomConstant.FIVE_DEFAULT_EXPIRE );
            return AjaxResult.success("登录成功",token);
        } else {

            if (s1 == null) {
                s1 = "1";
            } else {
                int i = Integer.parseInt(s1);
                i ++;
                s1 = i + "";
            }
            redisUtils.set("SENDSMS" + mobile, s1,  HomConstant.FIVE_DEFAULT_EXPIRE );
            return new AjaxResult(AjaxResult.Type.ERROR, "验证码错误");
        }

    }

    /**
     *
     * 校验用户发送的验证码是否有效，是否过期
     * @param userId
     * @param mobile
     * @param captcha
     * @param checkType
     * @return
     */
    @CheckToken
    @PostMapping("checkMobleCaptcha")
    public AjaxResult checkMobleCaptcha(@TokenUser String userId,@RequestParam(required = false) String mobile,@RequestParam String captcha,@RequestParam String checkType){
        if (StringUtils.isNotEmpty(userId) && StringUtils.isEmpty(mobile)) {
            //如果token值不为空,则进行查询
            UserCommonDetailsVo tokenUserDetail = userService.findTokenUserDetail(userId);
            if(StringUtils.isNotNull(tokenUserDetail)){
                mobile = tokenUserDetail.getMobile();
            }else {
                return AjaxResult.tokenError();
            }
        }
        //验证码在session中的key
        String verifyCode = "";
        //验证码生成时间点在session中的key
        String time = "";

        if (StringUtils.isNotEmpty(checkType)) {
            if (StringUtils.equals(HomConstant.ONESTR, checkType)) {//用户修改手机号，老手机号校验验证码
                verifyCode = mobile  + CaptchaType.MOD_OLD_PHONE.value();
                time = mobile  + CaptchaType.MOD_OLD_PHONE_TIME.value();
            } else if (StringUtils.equals(HomConstant.TWOSTR, checkType)) {//用户修改手机号，新手机号校验验证码
                if(StringUtils.isNotEmpty(mobile)){//验证手机号是否为空
                    if(RegisterUtils.isPhoneLegal(mobile)){//验证手机号是否合法
                        verifyCode = mobile  + CaptchaType.MOD_NEW_PHONE.value();
                        time = mobile  + CaptchaType.MOD_NEW_PHONE_TIME.value();
                    }else {
                        return AjaxResult.error(HomConstant.MOBILE_INPUT_ERROR);
                    }
                }else {
                    return AjaxResult.error(HomConstant.MOBILE_EMPTY_INPUT_MOBILE);
                }
            } else if (StringUtils.equals(HomConstant.THREESTR, checkType)) {//用户新增支付密码验证验证码
                verifyCode = mobile  + CaptchaType.ADD_PASSWORD.value();
                time = mobile  + CaptchaType.ADD_PASSWORD_TIME.value();
            } else if (StringUtils.equals(HomConstant.FourSTR, checkType)) {//用户修改支付密码验证验证码
                verifyCode = mobile  + CaptchaType.MOD_PASSWOED.value();
                time = mobile  + CaptchaType.MOD_PASSWOED_TIME.value();
            }
        }

        if (StringUtils.isNotEmpty(redisUtils.get(time)) && (System.currentTimeMillis() - Long.valueOf(String.valueOf(redisUtils.get(time))) > 1000 * 60 * 5)) {
            redisUtils.delete(verifyCode);
            redisUtils.delete(time);
            return new AjaxResult(AjaxResult.Type.ERROR, HomConstant.VERIFYCODE_EXPIRED);
        } else if (StringUtils.isNotEmpty(redisUtils.get(verifyCode)) && StringUtils.equals(captcha, redisUtils.get(verifyCode))) {
            redisUtils.delete(verifyCode);
            redisUtils.delete(time);
            if(StringUtils.equals(HomConstant.ONESTR,checkType)){
                UserCommonDetailsVo vo = new UserCommonDetailsVo();
                String uniqueKey = UUID.randomUUID().toString();
                //原手机号key的有效时间为10分钟
                redisUtils.set(CaptchaType.MOD_OLD_PASSWOED_KEY.value(),uniqueKey, HomConstant.TEN_DEFAULT_EXPIRE);
                vo.setUniqueKey(SecretKeyUtil.encode(uniqueKey));
                return AjaxResult.success(HomConstant.ORIGINALMOBILEVALIDATIONSUCCESSFUL,vo);
            }else if(StringUtils.equals(HomConstant.TWOSTR,checkType)){
                //修改用户手机号
                boolean flag = !userService.findUserAccontByMobile(mobile);
                if(!flag){
                    return new AjaxResult(AjaxResult.Type.MOBILE_REPEAT, HomConstant.MOBILE_REGISTERED);
                }
                flag= userService.updateUserAccount(userId,mobile);
                if(flag){
                    return AjaxResult.success(HomConstant.UPDATE_USER_MOBILE_SUCCESS);
                }else {
                    return AjaxResult.error(HomConstant.UPDATE_USER_MOBILE_ERROR);
                }
            }else if(StringUtils.equals(HomConstant.THREESTR,checkType) || StringUtils.equals(HomConstant.FourSTR,checkType)){
                UserCommonDetailsVo vo = new UserCommonDetailsVo();
                String uniqueKey = UUID.randomUUID().toString();
                //新增，或重置支付密码的有效时间为10分钟
//                redisUtils.set(userId + CaptchaType.PAY_PASSWOED_KEY.value(),uniqueKey, HomConstant.TEN_DEFAULT_EXPIRE);
                String key = CaptchaType.PAY_PASSWOED_KEY.value() + userId.substring(0,4);
                redisUtils.set(key,uniqueKey, HomConstant.TEN_DEFAULT_EXPIRE);
                vo.setUniqueKey(uniqueKey);
                return AjaxResult.success(HomConstant.PAYMENT_PASSWORD_VERIFICATION_SUCCESS,vo);
            }
            return  AjaxResult.success(HomConstant.VERIFICATION_SUCCESS);
        }
        return AjaxResult.error(HomConstant.VERIFYCODE_ERROR);
    }




    /**
     *
     * 根据用户ID,查询用户首页信息详情
     * @param userId
     * @return
     */
    @TokenRequired
    @GetMapping(value = "findUserFirstPageData")
    public AjaxResult findUserFirstPageData(@TokenUser String userId){
        UserFirstPageVo userFirstPageData = userService.findUserFirstPageData(userId);
        return AjaxResult.success(userFirstPageData);
    }



    /**
     *
     * 用户修改手机号查询用户手机并加密返回前台方法modifyMobileNumCaptcha
     */
    @TokenRequired
    @GetMapping(value = "/findModilyPhoneNum")
    public AjaxResult findModilyPhoneNum(@TokenUser String userId){
        UserCommonDetailsVo tokenUserDetail = userService.findUserDetail(userId);
        if(StringUtils.isNull(tokenUserDetail)){
            return AjaxResult.tokenError();
        }
        UserAccontVo userAccontVo = userService.findModilyPhoneNum(userId);
        if(StringUtils.isNotNull(userAccontVo)){
            return AjaxResult.success("查询数据成功",userAccontVo);
        }else{
            return AjaxResult.error("查询用户手机号失败",userAccontVo);
        }
    }

    /**
     *
     *
     * 新增或修改用户收货地址方法
     * @param userId
     * @param name
     * @param mobile
     * @param provinceId
     * @param cityId
     * @param areaId
     * @param county
     * @param detailAddr
     * @param defaultFlag
     * @param addressId
     * @return
     */
    @TokenRequired
    @PostMapping(value = "/saveUserAddr")
    public AjaxResult saveUserAddr(@TokenUser String userId,
                                   @RequestParam String name,
                                   @RequestParam String mobile,
                                   @RequestParam String provinceId,
                                   @RequestParam String cityId,
                                   @RequestParam String areaId,
                                   @RequestParam(name="county",required = false) String county,
                                   @RequestParam String detailAddr,
                                   @RequestParam String defaultFlag,
                                   @RequestParam(name="addressId",required = false) String addressId) {
        UserCommonDetailsVo tokenUserDetail = userService.findTokenUserDetail(userId);
        if(StringUtils.isNull(tokenUserDetail)){
            return AjaxResult.tokenError();
        }
        if(!RegisterUtils.isPhoneLegal(mobile)) {
            return AjaxResult.error("手机号输入错误");
        }
        UserAddress userAddress =
                userService.saveUserAddr(userId, name, mobile, provinceId, cityId, areaId, county, detailAddr, defaultFlag, addressId);
        List<UserAddrVo> userAddrAll = userService.findUserAddrAll(userId);
        Optional<UserAddrVo> userAddrById =
                userAddrAll.stream().filter(r -> r.getId().equals(userAddress.getId())).findFirst();
        return userAddrById.map(AjaxResult::success).orElseGet(AjaxResult::success);
    }

    @TokenRequired
    @GetMapping(value = "getUserCoinHistory")
    public AjaxResult getCoinHistoryByUserId( @TokenUser String userId,
                                              @RequestParam(name = "pageSize", defaultValue = "10", required = false) Integer pageSize,
                                              @RequestParam(name = "pageNum", defaultValue = "1", required = false) Integer pageNum){
        BasePage<CoinHistoryVo> historyByUserId = userService.getHistoryByUserId(pageSize, pageNum, userId);
        UserEntity userById = userService.getUserById(userId);
        Integer coins = userById.getCoins();
        return AjaxResult.success(new BasePageVo<CoinHistoryVo>(historyByUserId,coins)) ;
    }

    /**
     * 删除用户收货地址
     *
     * @param addressId
     * @return
     */
    @TokenRequired
    @GetMapping(value = "deleteUserAddr")
    public AjaxResult deleteUserAddr(@TokenUser String userId,@RequestParam String addressId){
        UserCommonDetailsVo tokenUserDetail = userService.findUserDetail(userId);
        if(StringUtils.isNull(tokenUserDetail)){
            return AjaxResult.tokenError();
        }
        boolean flag = userService.deleteUserAddr(addressId);
        if(flag){
            return AjaxResult.success();
        }else{
            return AjaxResult.error();
        }
    }

    @PostMapping(value = "toLoginOther")
    public AjaxResult toLoginOther(String openId, String unionId, String type,String method) {
        return userService.toLoginOther(openId, unionId, type, method);
    }

    @TokenRequired
    @PostMapping("unbindOther")
    public AjaxResult unbindOther(@TokenUser String userId, @RequestParam String type){
        return userService.unbindOther(userId, type);
    }

    /**
     *
     * 获取用户主键ID,查询用户所有收货地址
     * @param userId
     */
    @TokenRequired
    @GetMapping(value = "findUserAddrAll")
    public AjaxResult findUserAddrAll(@TokenUser String userId) {
        UserCommonDetailsVo tokenUserDetail = userService.findUserDetail(userId);
        if(StringUtils.isNull(tokenUserDetail)){
            return AjaxResult.tokenError();
        }

        List<UserAddrVo> userAddrAll = userService.findUserAddrAll(userId);
        return AjaxResult.success(userAddrAll);
    }

    /**
     *
     * 用户关注店铺
     * @param userId
     * @param businessId
     * @return
     */
    @TokenRequired
    @PostMapping(value = "insertUserShop")
    public AjaxResult insertUserShop(@TokenUser String userId,@RequestParam String businessId){
        UserCommonDetailsVo tokenUserDetail = userService.findUserDetail(userId);
        if(StringUtils.isNull(tokenUserDetail)){
            return AjaxResult.tokenError();
        }
        UserShopRelationVo userShopRelationVo = userService.insertUserShop(userId,businessId);
        if(userShopRelationVo == null){
            return AjaxResult.error("用户关注店铺失败");
        }else{
            return AjaxResult.success("用户关注店铺成功",userShopRelationVo);
        }
    }

    /**
     *
     * 用户关注店铺 ---取消关注
     * @param businessId
     * @return
     */
    @TokenRequired
    @PostMapping(value = "/deleteUserShop")
    public AjaxResult deleteUserShop(@TokenUser String userId,@RequestParam String businessId){
        UserCommonDetailsVo tokenUserDetail = userService.findUserDetail(userId);
        if(StringUtils.isNull(tokenUserDetail)){
            return AjaxResult.tokenError();
        }
        boolean flag = userService.deleteUserShop(userId, businessId);
        if(flag){
            return AjaxResult.success("删除数据成功");
        }else {
            return AjaxResult.error("删除数据失败");
        }
    }

    /**
     *
     *liyang
     *查询用户关注店铺分页方法
     * @param userId
     * @param pageSize
     * @param pageNum
     * @return
     */
    @TokenRequired
    @GetMapping(value = "getUserShopAll")
    public AjaxResult getUserShopAll(@TokenUser String userId,
                                     @RequestParam(name="pageSize", defaultValue = "10", required = false) Integer pageSize,
                                     @RequestParam(name="pageNum", defaultValue = "1", required = false) Integer pageNum){
        UserCommonDetailsVo tokenUserDetail = userService.findUserDetail(userId);
        if(StringUtils.isNull(tokenUserDetail)){
            return AjaxResult.tokenError();
        }
        BasePage<UserFollowShopDetailVo> pageList = userService.getUserShopAll(userId, pageSize, pageNum);
        return AjaxResult.success(pageList) ;
    }

    /**
     *
     * 查询轮询图返回集合方法
     * @param bannerType
     * @return
     */
    @GetMapping(value = "findBannerPage")
    public AjaxResult findBannerPage(@RequestParam  String bannerType){
        return AjaxResult.success("查询轮播图成功" , userService.findBannerPage(bannerType));
    }



    /**
     *
     *
     * 保存用户支付密码
     * @param userId
     * @param password
     * @return
     */
    @TokenRequired
    @PostMapping(value = "savePayPassword")
    public AjaxResult savePayPassword(@TokenUser String userId,@RequestParam String password,@RequestParam String uniqueKey){
        UserCommonDetailsVo tokenUserDetail = userService.findUserDetail(userId);
        if(StringUtils.isNull(tokenUserDetail)){
            return AjaxResult.tokenError();
        }
        return userService.savePayPassword(userId, password,uniqueKey);
    }

    /**
     *
     * 校验输入的密码是否与数据库原密码一致
     * @param userId
     * @param password
     * @return
     */
    @TokenRequired
    @PostMapping(value = "/checkPayPassword")
    public AjaxResult checkPayPassword(@TokenUser String userId,@RequestParam String password){
        UserCommonDetailsVo tokenUserDetail = userService.findUserDetail(userId);
        if(StringUtils.isNull(tokenUserDetail)){
            return AjaxResult.tokenError();
        }
        return  userService.checkPayPassword(userId,password);
    }


    /**
     *
     * 修改支付密码
     * @param userId
     * @param password
     * @return
     */
    @TokenRequired
    @PostMapping(value = "/updatePayPassword")
    public AjaxResult updatePayPassword(@TokenUser String userId,@RequestParam String oldPassword,@RequestParam String password){
        UserCommonDetailsVo tokenUserDetail = userService.findUserDetail(userId);
        if(StringUtils.isNull(tokenUserDetail)){
            return AjaxResult.tokenError();
        }
        return userService.updatePayPassword(userId,oldPassword,password);
    }



    /**
     *
     * 用户个人资料录入方法
     * @param picId
     * @param nickname
     * @param sex
     * @return
     */
    @TokenRequired
    @PostMapping(value = "/userData")
    public AjaxResult userData(
            @TokenUser String userId, @RequestParam(name= "pic", required = false) String picId,
            @RequestParam(required = false) String nickname, @RequestParam(required = false) String sex,
            @RequestParam(required = false) String homeBack, @RequestParam(required = false) String autograph,
            @RequestParam(required = false) String openId, @RequestParam(required = false) String unionId
    ){
        UserCommonDetailsVo tokenUserDetail = userService.findUserDetail(userId);
        if(StringUtils.isNull(tokenUserDetail)){
            return AjaxResult.tokenError();
        }
        return userService.userData(userId,picId,nickname,sex,homeBack,autograph,openId,unionId);
    }


    /**
     *
     * 查询用户详情方法
     * @param userId
     * @return
     */
    @TokenRequired
    @GetMapping(value = "/findUserAndDetail")
    public AjaxResult findUserDetailAndUser(@TokenUser String userId){
        UserCommonDetailsVo tokenUserDetail = userService.findUserDetail(userId);
        if(StringUtils.isNull(tokenUserDetail)){
            return AjaxResult.tokenError();
        }
        return userService.findUserDetailAndUser(userId);
    }

    /**
     *
     * 新增用户意见反馈信息
     * @param userId
     * @param idea
     * @param contact
     * @return
     */
    @CheckToken
    @PostMapping(value = "saveFeedBackData")
    public AjaxResult saveFeedBackData(@TokenUser String userId,@RequestParam String idea,@RequestParam(required = false) String contact){
        if(StringUtils.isEmpty(idea)){
            return AjaxResult.error("用户意见反馈信息不能为空");
        }
        boolean b = userService.saveFeedBackData(userId, idea, contact);
        if(b){
            return AjaxResult.success();
        }
        return AjaxResult.error();
    }

    /**
     *
     * 根据用户信息查询意见反馈信息
     * @param userId
     * @return
     */
    @CheckToken
    @GetMapping(value = "findFeedBack")
    public AjaxResult findFeedBack(@TokenUser String userId){
        return AjaxResult.success(userService.findFeedBack(userId));
    }


    /**
     *
     * 修改用户反馈信息
     * @param userId
     * @param idea
     * @return
     */
    @CheckToken
    @PostMapping(value = "updateFeedBack")
    public AjaxResult updateFeedBack(@TokenUser String userId,@RequestParam(required = false) String idea,@RequestParam(required = false) String contact){
        boolean b = userService.updateFeedBack(userId, idea, contact);
        if(b){
            return AjaxResult.success();
        }
        return AjaxResult.error();
    }


    /**
     *
     * 删除用户反馈信息
     * @param userId
     * @return
     */
    @TokenRequired
    @PostMapping(value = "deleteFeedBack")
    public AjaxResult deleteFeedBack(@TokenUser String userId){
        boolean b = userService.deleteFeedBack(userId);
        if(b){
            return AjaxResult.success();
        }
        return AjaxResult.error();
    }

    /**
     *
     * 查询用户足迹分页方法
     * @param pageSize
     * @param pageNum
     * @return
     */
    @TokenRequired
    @GetMapping(value = "/footPrintPage")
    public AjaxResult footPrintPage(@TokenUser String userId, @RequestParam(name="pageSize", defaultValue = "10", required = false) Integer pageSize,
                                    @RequestParam(name="pageNum", defaultValue = "1", required = false) Integer pageNum){
        return AjaxResult.success(userService.footPrintPage(userId,pageSize,pageNum));
    }

    /**
     *
     * 省市区县三级菜单查询
     * @param version
     * @return
     */
    @GetMapping(value = "findAllCityTree")
    public AjaxResult findAllCityTree(@RequestParam(required = false) String version){
        AllCityVersionVo allCityTree = userService.findAllCityTree(version);
        return allCityTree == null? AjaxResult.isNew(): AjaxResult.success(allCityTree);
    }


    /**
     *
     * 根据收货地址ID,查询收货地址
     * @param id
     * @return
     */
    @GetMapping(value = "findUserAddrById")
    public AjaxResult findUserAddrById(@RequestParam String id){
        UserAddrVo userAddrById = userService.findUserAddrById(id);
        return AjaxResult.success(userAddrById);
    }

    /**
     *
     *
     * 用户头像上传
     * @param file
     * @param userId
     * @return
     */
    @TokenRequired
    @PostMapping(value = "/userPicUpload")
    public AjaxResult userPicUpload(@TokenUser String userId,MultipartFile file) {
        UserCommonDetailsVo tokenUserDetail = userService.findUserDetail(userId);
        if(StringUtils.isNull(tokenUserDetail)){
            return AjaxResult.tokenError();
        }

        String type = "head_pic";
        String path = UploadUtils.uploadFile(file,userId,filepath,type);

        if(!StringUtils.isEmpty(path)){
            return userService.userPicUpload(path,userId);
        }else{
            return AjaxResult.error();
        }
    }

    /**
     *
     *
     * 用户背景图片上传
     * @param file
     * @param userId
     * @return
     */
    @TokenRequired
    @PostMapping(value = "/userBackUpload")
    public AjaxResult userBackUpload(@TokenUser String userId,MultipartFile file) {
        UserCommonDetailsVo tokenUserDetail = userService.findUserDetail(userId);
        if(StringUtils.isNull(tokenUserDetail)){
            return AjaxResult.tokenError();
        }

        String type = "back_pic_";
        String path = UploadUtils.uploadFile(file,userId,filepath,type);

        if(!StringUtils.isEmpty(path)){
            return userService.userBackUpload(path,userId);
        }else{
            return AjaxResult.error();
        }
    }

//    /**
//     * 微信认证接口
//     */
//    @PostMapping(value = "wxlogin")
//    public AjaxResult getAccessToken(@RequestParam(name = "code") String code) {
//        if (StringUtils.isBlank(code)) {
//            return AjaxResult.error("code不存在");
//        }
//        try {
//            WxMpOAuth2AccessToken wxMpOAuth2AccessToken = wxMpService.oauth2getAccessToken(code);
//            String accessToken = wxMpOAuth2AccessToken.getAccessToken();
//            // 获取用户微信账户信息
//            WxMpUser wxMpUser = wxMpService.getUserService().userInfo(wxMpOAuth2AccessToken.getOpenId());
//            if (StringUtils.isBlank(wxMpUser.getOpenId())) {
//                return AjaxResult.error("用户数据不存在");
//            }
//            return AjaxResult.success(wxMpUser);
//        } catch (WxErrorException e) {
//            e.printStackTrace();
//            return AjaxResult.error("授权登录失败");
//        } catch (Exception e) {
//            e.printStackTrace();
//            return AjaxResult.error("登录失败");
//        }
//    }
}
