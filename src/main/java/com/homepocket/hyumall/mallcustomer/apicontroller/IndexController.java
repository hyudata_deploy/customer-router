package com.homepocket.hyumall.mallcustomer.apicontroller;

import com.homepocket.hyumall.common.domain.AjaxResult;
import com.homepocket.hyumall.mallcustomer.apiservice.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Liang
 * @date 2020/6/17 20:11
 **/
@RestController
public class IndexController {
    @Autowired
    private ProductService productService;

    @GetMapping("getIndexProducts")
    public AjaxResult getIndexProducts(String province, String city,String categoryId) {
        return AjaxResult.success(productService.getIndexProducts(province,city,categoryId));
    }

    @GetMapping("getConfig")
    public AjaxResult getConfig( ) {
        return AjaxResult.success();
    }

//    @GetMapping("index")
//    public AjaxResult
}
