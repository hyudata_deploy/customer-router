package com.homepocket.hyumall.mallcustomer.apicontroller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.homepocket.hyumall.common.annotation.CheckToken;
import com.homepocket.hyumall.common.annotation.TokenRequired;
import com.homepocket.hyumall.common.annotation.TokenUser;
import com.homepocket.hyumall.common.constant.HomConstant;
import com.homepocket.hyumall.common.domain.AjaxResult;
import com.homepocket.hyumall.common.enums.CoinTypeEnum;
import com.homepocket.hyumall.common.enums.OrderStatusEnum;
import com.homepocket.hyumall.common.enums.RedisKeyEnum;
import com.homepocket.hyumall.common.utils.StringUtils;
import com.homepocket.hyumall.common.vo.OrderStatusVo;
import com.homepocket.hyumall.common.vo.order.*;
import com.homepocket.hyumall.common.vo.pay.PayVo;
import com.homepocket.hyumall.common.vo.user.UserAddrVo;
import com.homepocket.hyumall.mallcustomer.apiservice.OrderService;
import com.homepocket.hyumall.mallcustomer.apiservice.ProductService;
import com.homepocket.hyumall.mallcustomer.apiservice.UserService;
import com.homepocket.hyumall.mallcustomer.config.WithdrawMqConfig;
import com.homepocket.hyumall.mallcustomer.service.CusOrderService;
import com.homepocket.hyumall.mallcustomer.service.ProfitApplyService;
import com.homepocket.hyumall.mallcustomer.utils.RedisUtils;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * @author Liang
 * @date 2020/6/9 12:53
 **/
@RestController
public class OrderController {

    @Autowired
    private OrderService orderService;

    @Autowired
    private UserService userService;

    @Autowired
    private ProductService productService;

    @Autowired
    private ProfitApplyService profitApplyService;

    @Autowired
    private CusOrderService cusOrderService;

    @Autowired
    private AmqpTemplate amqpTemplate;

    @Autowired
    private RedisUtils redisUtils;

    @Value("${comment_coins}")
    private Integer commentCoins;


    @Value("${pay_coins}")
    private Double payCoins;

    @TokenRequired
    @PostMapping("addOrder")
    public AjaxResult addOrder(
            @TokenUser String userId, String addressId, String jsonData,
            String couponId, Integer coinFlag
    ) {
//        userService.findUserAddrAll(addressId)
        // 先查询收货地址是否为本人收货地址
        if (addressId == null) {
            List<UserAddrVo> userAddrAll = userService.findUserAddrAll(userId);
            if (userAddrAll != null && userAddrAll.size() > 0) {
                Optional<UserAddrVo> first =
                        userAddrAll.stream().filter(r -> "1".equals(r.getDefaultFlag())).findFirst();
                if (first.isPresent()) {
                    UserAddrVo userAddrVo = first.get();
                    addressId = userAddrVo.getId();
                }
            }
        }
        Integer couponFlag = couponId != null
                ? 1
                : 0;
        // 计算价格
        CalculateVo calculateVo =
                cusOrderService.calculatePrice(
                        userId, addressId, jsonData, couponFlag, couponId, coinFlag).getCalculateVo();
//        ProductDetailVo productDetailVo = productService.getProductById(productId);
//        SizePriceVo sizeVo = productService.getSizeById(sizeDetailId);

        String errorCode = calculateVo.getErrorCode();
        // 无错误 订单可以正常生成
        if ("0".equals(errorCode)) {
            PayVo payVo = orderService.addOrder(calculateVo, userId);
            if (payVo == null) {
                return AjaxResult.error("新增失败");
            } else {
//                // 扣除库存
//                productService.changeStock(sizeDetailId, -boughtNum);
                List<OrderProductVo> list1 = JSON.parseArray(jsonData, OrderProductVo.class);
                StringBuilder cartIds = new StringBuilder();
                for (OrderProductVo orderProductVo : list1) {
                    if (null == orderProductVo.getCartProductId()) {
                        cartIds = null;
                        break;
                    }
                    cartIds.append(orderProductVo.getCartProductId());
                    cartIds.append(",");
                }
                if (cartIds != null) {
                    cartIds = new StringBuilder(cartIds.substring(0, cartIds.length() - 1));
                    orderService.delCartProduct(cartIds.toString(), userId);
                }

                if (coinFlag == 1) {
                    // 扣除口袋币
                    userService.giveCoins(userId, -calculateVo.getCoinNumber(), CoinTypeEnum.BUY_TAKE.getCode());
                }
                return AjaxResult.success("创建订单成功", payVo);
            }
        } else {
            return AjaxResult.calculateError(calculateVo);
        }
    }

    /**
     * @param addressId
     * @return
     */
    @TokenRequired
    @GetMapping("calculateMoney")
    public AjaxResult calculateMoney(
            @TokenUser String userId, String addressId, String orderJsonData,
            String couponId, Integer coinFlag) {
        if (addressId == null) {
            List<UserAddrVo> userAddrAll = userService.findUserAddrAll(userId);
            if (userAddrAll != null && userAddrAll.size() > 0) {
                Optional<UserAddrVo> first =
                        userAddrAll.stream().filter(r -> "1".equals(r.getDefaultFlag())).findFirst();
                if (first.isPresent()) {
                    UserAddrVo userAddrVo = first.get();
                    addressId = userAddrVo.getId();
                }
            }
        }
        Integer couponFlag = couponId != null
                ? 1
                : 0;
        CalVo calculateVo =
                cusOrderService.calculatePrice(
                        userId, addressId, orderJsonData, couponFlag, couponId, coinFlag).getCalVo();
        int payPwd = userService.getPayPwd(userId);
        calculateVo.setPayFlag(payPwd);
        return AjaxResult.success(calculateVo);
    }

    @TokenRequired
    @GetMapping("findOrderAll")
    public AjaxResult findOrderAll(
            @TokenUser String userId, @RequestParam(name = "orderStatus", required = false) String orderStatus,
            @RequestParam(name = "pageSize", defaultValue = "10", required = false) Integer pageSize,
            @RequestParam(name = "pageNum", defaultValue = "1", required = false) Integer pageNum) {

        return AjaxResult.success(orderService.findOrderAll(userId, orderStatus, pageNum, pageSize));
    }

    @TokenRequired
    @GetMapping("getRefundProducts")
    public AjaxResult getRefundProducts(@TokenUser String userId, String orderId) {
        return AjaxResult.success();
    }

    @TokenRequired
    @PostMapping("changeOrder")
    public AjaxResult changeOrder(@TokenUser String userId, @RequestParam String orderId,
                                  @RequestParam Integer status, String message) {
        int count = 0;
        if (HomConstant.Four == status) {
            amqpTemplate.convertAndSend(WithdrawMqConfig.DELAY_QUEUE_PER_QUEUE_TTL_NAME, orderId);
        }
        try {
            count = orderService.changeOrder(orderId, status, message);
        } catch (Exception e) {
            e.printStackTrace();
        }
        OrderDetailReturnVo vo = orderService.getOrderDetailReturnVoById(orderId);
        if (count < 1) {
            if (status.equals(HomConstant.Four)) {
                return AjaxResult.error("确定收货失败");
            } else if (HomConstant.THREE == status) {

                return AjaxResult.success("提醒商家发货失败");
            } else {
                return AjaxResult.success(HomConstant.CANCEL_ORDER_SUCCESS_MSG);
            }
        } else {
            if (HomConstant.ONE == status) {
                // 退款
                int i = orderService.cancelOrder(orderId);
                if (i == 0) {
                    return AjaxResult.error("取消失败");
                } else {
                    // 回滚库存 口袋币
                    orderService.rollBackStock(orderId);
                    if (vo.getCoinNum() > 0) {
                        userService.giveCoins(userId, vo.getCoinNum(), CoinTypeEnum.CANCEL_RETURN.getCode());
                    }
                    redisUtils.delete(RedisKeyEnum.ORDEREXPIREKEY.getMsg() + orderId);
                    return AjaxResult.success(HomConstant.CANCEL_ORDER_SUCCESS_MSG);
                }
            }
            if (HomConstant.Four == status) {
                // 添加物流信息
                orderService.addTransport(orderId);
                Integer sum = vo.getRealPay();
                System.out.println("paycoins====" + payCoins);
                System.out.println("用户实付金额为+================" + sum);
                BigDecimal num = new BigDecimal(payCoins).multiply(new BigDecimal(sum)).divide(new BigDecimal(100), 0
                        , BigDecimal.ROUND_DOWN);
                if (num.intValue() > 0) {
                    System.out.println("开始发放口袋币=====金额为+=====" + num);
                    userService.giveCoins(userId, num.intValue(), CoinTypeEnum.BUY_GIVE.getCode());
                }
                return AjaxResult.success("确定收货成功");
            } else if (HomConstant.TWO == status) {
                return AjaxResult.success("删除订单成功");
            } else if (HomConstant.THREE == status) {
                return AjaxResult.success("提醒商家发货成功");
            } else {
                return AjaxResult.success(HomConstant.CANCEL_ORDER_SUCCESS_MSG);
            }
        }
    }

    @GetMapping("cancelReason")
    public AjaxResult cancelReason(@RequestParam(value = "type", defaultValue = "0" , required = false) Integer type) {
        List<String> cancelReason = new ArrayList<>();
        String a = "退运费";
        String b = "包装/商品破损";
        String c = "商品描述不符";
        String d = "少件(含缺少配件)";
        String e = "商品发错货";
        String f = "其他";
        String g = "多拍/错拍/不想要";
        String h = "缺货";
        String i = "未按约定发货";
        if (type == 0) {
            cancelReason = orderService.cancelReason();
        } else if (type == 1) {
            cancelReason.add(g);
            cancelReason.add(h);
            cancelReason.add(i);
            cancelReason.add(f);
        } else if (type == 2) {
            cancelReason.add(b);
            cancelReason.add(c);
            cancelReason.add(d);
            cancelReason.add(e);
            cancelReason.add(f);
        } else if (type == 3) {
            cancelReason.add(a);
            cancelReason.add(b);
            cancelReason.add(c);
            cancelReason.add(d);
            cancelReason.add(e);
            cancelReason.add(f);
        }
        return AjaxResult.success(cancelReason);
    }

    @GetMapping("findOrderStatus")
    public AjaxResult findOrderStatus() {
        List<OrderStatusVo> orderStatus = orderService.findOrderStatus();
        return AjaxResult.success(orderStatus);
    }


    @TokenRequired
    @GetMapping("getOrderDetailById")
    public AjaxResult getOrderDetailById(@TokenUser String userId, String orderId) {
        OrderDetailReturnVo vo = orderService.getOrderDetailReturnVoById(orderId);
        if (userId.equals(vo.getUserId())) {
            return AjaxResult.success(vo);
        } else {
            return AjaxResult.mustToken();
        }
    }

    @TokenRequired
    @GetMapping("getOrderProductByOrderId")
    public AjaxResult getOrderProductByOrderId(@RequestParam String orderId){
        return AjaxResult.success(orderService.getOrderProductByOrderId(orderId));
    }

    @PostMapping("deleteOrder")
    public AjaxResult deleteOrder(@RequestParam String orderId){

        return orderService.deleteOrder(orderId) > 0? AjaxResult.success("删除订单成功") : AjaxResult.error("删除订单失败");
    }

    @PostMapping("deleteRefundApply")
    public AjaxResult deleteRefundApply(@RequestParam String refundNo){
        return orderService.deleteRefundApply(refundNo) > 0? AjaxResult.success("删除退款申请成功") : AjaxResult.error("删除退款申请失败");
    }

    @TokenRequired
    @PostMapping("commentOrder")
    public AjaxResult commentOrder(
                @TokenUser String userId, @RequestParam String orderId,
            @RequestParam(required = false) Integer logisticsScore,
            @RequestParam(required = false) Integer serviceScore,
                @RequestParam String comment
    ) {
        JSONArray jsonArray = JSONObject.parseArray(comment);
        List<CommentVo> commentVos = new ArrayList<>();
        boolean flag = false;
        int coin = 0;
        if (logisticsScore != null && serviceScore != null) {
            flag = true;
        }
        for (int i = 0 ; i<jsonArray.size() ; i++){
            CommentVo vo = new CommentVo();
            JSONObject jsonObject = jsonArray.getJSONObject(i);
            if (logisticsScore != null && serviceScore != null) {
                int commentScore = Integer.parseInt(jsonObject.get("commentScore").toString());
                vo.setTotalScore((double)commentScore);
                vo.setCommentScore(commentScore);
                vo.setLogisticsScore(logisticsScore);
                vo.setServiceScore(serviceScore);
                if (commentScore > 3) {
                    vo.setType("0");
                } else if (commentScore > 1) {
                    vo.setType("1");
                } else {
                    vo.setType("2");
                }
            } else {
                vo.setType("3");
            }
            String detail = jsonObject.get("detail").toString();
            String pics = jsonObject.get("pics").toString();

            if (StringUtils.isNotEmpty(detail) && detail.length() >= 10 && StringUtils.isNotEmpty(pics) && pics.length() > 0) {
                coin += commentCoins;
            }
            vo.setDetail(detail);
            vo.setProductId(jsonObject.get("productId").toString());
            vo.setOrderId(orderId);
            vo.setUserId(userId);
            vo.setUrls(pics);
            commentVos.add(vo);
        };

        int i = orderService.commentOrder(commentVos);
        if (i > 0) {
            int status = flag
                    ? Integer.parseInt(OrderStatusEnum.HAVE_COMMENTS.getStatus())
                    : Integer.parseInt(OrderStatusEnum.HAS_BEEN_COMPLETED.getStatus());
            int e = orderService.changeOrder(orderId, status, null);
            if (e == 1) {
                userService.giveCoins(userId, coin, CoinTypeEnum.COMMENT_GIVE.getCode());
                return AjaxResult.success();
            } else {
                return AjaxResult.error();
            }
        }
        return AjaxResult.error();
    }

    @GetMapping("getProductSnapshots")
    public AjaxResult getProductSnapshots(String orderProductId) {
        return AjaxResult.success(orderService.getProductSnapshots(orderProductId));
    }

    @GetMapping("getReportTypeList")
    public AjaxResult getReportTypeList() {
        return AjaxResult.success(orderService.getReportTypeList());
    }

    @CheckToken
    @PostMapping("addReport")
    public AjaxResult addReport(
            @TokenUser String userId, @RequestParam String commentId,
            @RequestParam(name = "type") String name, @RequestParam(required = false) String remark) {
        return AjaxResult.success(orderService.addReport(commentId, userId, name, remark));
    }


    @TokenRequired
    @PostMapping("addCartProduct")
    public AjaxResult addCartProduct(
            @TokenUser String userId, @RequestParam String productId,
            @RequestParam String shopId, @RequestParam String sizeDetailId,
            @RequestParam Integer num, @RequestParam Integer cartPrice) {
        return orderService.addCartProduct(productId, shopId, sizeDetailId, num, cartPrice, userId) == 0
                ? AjaxResult.error("添加失败"): AjaxResult.success() ;
    }


    @TokenRequired
    @GetMapping("getCartDetail")
    public AjaxResult getCartDetail(
            @TokenUser String userId) {
        return AjaxResult.success(orderService.getCartDetail(userId));
    }

    @TokenRequired
    @GetMapping("delCartProduct")
    public AjaxResult delCartProduct(@TokenUser String userId, @RequestParam String cartProductId) {
        return orderService.delCartProduct(cartProductId, userId) >= 1
                ? AjaxResult.success() : AjaxResult.error("删除失败");
    }

    @TokenRequired
    @PostMapping("updateCartProduct")
    public AjaxResult updateCartProduct(@RequestParam String cartProductId, @RequestParam Integer num) {
        return orderService.updateCartProduct(cartProductId, num) >= 1
                ? AjaxResult.success() : AjaxResult.error("更新失败");
    }

    @TokenRequired
    @PostMapping("updateCartSize")
    public AjaxResult updateCartSize(@TokenUser String userId,
                                     @RequestParam String cartProductId,
                                     @RequestParam String sizeDetailId,
                                     @RequestParam Integer cartPrice) {
        return orderService.updateCartSize(cartProductId, sizeDetailId, cartPrice,userId) >= 1
                ? AjaxResult.success() : AjaxResult.error("规格更新失败");
    }


    // 离线 加入购物车 start
    @PostMapping("addCartProductOffline")
    public AjaxResult addCartProductOffline(
            @RequestParam String userId, @RequestParam String productId,
            @RequestParam String shopId, @RequestParam String sizeDetailId,
            @RequestParam Integer num, @RequestParam Integer cartPrice) {
        return orderService.addCartProduct(productId, shopId, sizeDetailId, num, cartPrice, userId) >= 1 ?
                AjaxResult.success() : AjaxResult.error("添加失败");
    }

    @GetMapping("getCartDetailOffline")
    public AjaxResult getCartDetailOffline(
            @RequestParam String userId) {
        return AjaxResult.success(orderService.getCartDetail(userId));
    }

    @GetMapping("delCartProductOffline")
    public AjaxResult delCartProductOffline(@RequestParam String userId, @RequestParam String cartProductId) {
        return orderService.delCartProduct(cartProductId, userId) >= 1
                ? AjaxResult.success() : AjaxResult.error("删除失败");
    }

    @PostMapping("updateCartProductOffline")
    public AjaxResult updateCartProductOffline(@RequestParam String cartProductId, @RequestParam Integer num) {
        return orderService.updateCartProduct(cartProductId, num) >= 1
                ? AjaxResult.success() : AjaxResult.error("更新失败");
    }

    @PostMapping("updateCartSizeOffline")
    public AjaxResult updateCartSizeOffline(@RequestParam String userId,
                                            @RequestParam String cartProductId,
                                            @RequestParam String sizeDetailId,
                                            @RequestParam Integer cartPrice) {
        return orderService.updateCartSize(cartProductId, sizeDetailId, cartPrice,userId) >= 1
                ? AjaxResult.success() : AjaxResult.error("规格更新失败");
    }

    @TokenRequired
    @GetMapping("syncCart")
    public AjaxResult syncCart(@TokenUser String userId, @RequestParam String formId) {
        return orderService.syncCart(formId, userId) >= 1 ?
                AjaxResult.success() : AjaxResult.error("同步失败");
    }

    @TokenRequired
    @GetMapping("getTransportsList")
    public AjaxResult getTransportsList(@RequestParam String orderId) {
        return orderService.getTransportsList(orderId);
    }
}
