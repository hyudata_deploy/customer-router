package com.homepocket.hyumall.mallcustomer.apicontroller;

import com.homepocket.hyumall.common.domain.AjaxResult;
import com.homepocket.hyumall.common.domain.HistoryCity;
import com.homepocket.hyumall.common.vo.city.CityVersionVo;
import com.homepocket.hyumall.mallcustomer.apiservice.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author Liang
 * @date 2020/7/4 10:41
 **/
@RestController
public class CityController {

    @Autowired
    private ProductService service;

    @GetMapping("getOpenedCity")
    public AjaxResult getOpenedCity(String version){
        CityVersionVo openedCity = service.getOpenedCity(version);
        return openedCity == null? AjaxResult.isNew(): AjaxResult.success(openedCity);
    }

    @GetMapping("getHistoryCityByUserId")
    public AjaxResult getHistoryCityByUserId(String userId){
        List<HistoryCity> openedCity = service.getHistoryCityByUserId(userId);
        return AjaxResult.success(openedCity);
    }

    @PostMapping("addHistoryCity")
    public AjaxResult addHistoryCity(HistoryCity city) {
        int i = service.addHistoryCity(city);
        return i > 0? AjaxResult.success(): AjaxResult.error();
    }
}
