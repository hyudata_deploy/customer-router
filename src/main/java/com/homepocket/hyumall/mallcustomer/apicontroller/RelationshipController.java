package com.homepocket.hyumall.mallcustomer.apicontroller;

import com.homepocket.hyumall.common.annotation.CheckToken;
import com.homepocket.hyumall.common.annotation.TokenRequired;
import com.homepocket.hyumall.common.annotation.TokenUser;
import com.homepocket.hyumall.common.domain.AjaxResult;
import com.homepocket.hyumall.common.utils.StringUtils;
import com.homepocket.hyumall.common.vo.relationship.AddInviteListVo;
import com.homepocket.hyumall.common.vo.relationship.RelationTagVo;
import com.homepocket.hyumall.mallcustomer.apiservice.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


/**
 * @author Liang
 * @date 2020/7/13 9:19
 **/
@RestController
public class RelationshipController {

    @Autowired
    private UserService userService;

    @TokenRequired
    @PostMapping("addRelationshipGroup")
    public AjaxResult addRelationshipGroup(@TokenUser String userId) {
        return userService.addRelationshipGroup(userId);

    }

    @CheckToken
    @GetMapping("getRelationshipsByUserId")
    public AjaxResult getRelationshipsByUserId(@TokenUser String userId) {
        return userService.getRelationshipsByUserId(userId);
    }

    @TokenRequired
    @GetMapping("getRelationGroupByUserId")
    public AjaxResult getRelationGroupByUserId(@TokenUser String userId) {
        return userService.getRelationGroupByUserId(userId);
    }

    @CheckToken
    @GetMapping("getRelationTags")
    public AjaxResult getRelationTags(@TokenUser String userId) {
        return userService.getRelationTags(userId);
    }

    @TokenRequired
    @PostMapping("addInviteList")
    public AjaxResult addInviteList(@TokenUser String userId, AddInviteListVo addInviteListVo) {
        return userService.addInviteList(addInviteListVo, userId);

    }


    /**
     * 查看邀请的列表
     *
     * @param userId 当前用户ID
     * @return
     */
    @CheckToken
    @GetMapping("listInvitedInfo")
    public AjaxResult listInvitedInfo(@TokenUser String userId) {
        if (StringUtils.isEmpty(userId)) {
            return AjaxResult.tokenWarn(null);
        }
        return userService.listInvitedInfo(userId);
    }

    /**
     * 同意接受家庭
     *
     * @param itemId 邀请id
     * @param userId 用户id
     * @return
     */
    @TokenRequired
    @PostMapping("acceptInvite")
    public AjaxResult acceptInvite(@TokenUser String userId, String itemId) {
        return userService.acceptInvite(itemId, userId);

    }

    @TokenRequired
    @PostMapping("refuseInvite")
    public AjaxResult refuseInvite(@TokenUser String userId, String itemId) {
        return userService.refuseInvite(itemId, userId);

    }


    @TokenRequired
    @PostMapping("quitAndJoin")
    public AjaxResult quitAndJoin(@TokenUser String userId, String itemId) {
        return userService.quitAndJoin(itemId, userId);

    }

    @CheckToken
    @PostMapping("cancelInvite")
    public AjaxResult cancelInvite(@TokenUser String userId, String itemId) {
        return userService.cancelInvite(itemId, userId);

    }

    @TokenRequired
    @PostMapping("deleteInvite")
    public AjaxResult deleteInvite(@TokenUser String userId, String itemId) {
        return userService.deleteInvite(itemId, userId);

    }

    @TokenRequired
    @PostMapping("expelRelation")
    public AjaxResult expelRelation(@TokenUser String userId, String itemId) {
        return userService.expelRelation(itemId, userId);

    }

    @TokenRequired
    @PostMapping("modifyRelation")
    public AjaxResult modifyRelation(@TokenUser String userId, RelationTagVo relationTagVo) {
        return userService.modifyRelation(relationTagVo, userId);

    }
}
