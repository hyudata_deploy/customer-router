package com.homepocket.hyumall.mallcustomer.apicontroller;

import com.homepocket.hyumall.common.domain.AjaxResult;
import com.homepocket.hyumall.mallcustomer.apiservice.RotationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author Anson
 * @Date 2020/6/19
 * @Description
 **/
@RestController
public class RotationController {
    @Autowired
    private RotationService rotationService;

    @GetMapping("getRotationListBySize")
    public AjaxResult getRotationListBySize(String position, String city, Integer size) {
//        List<RotationPicViewEntity> collect = rotationService.getRotationListBySize(position, city, size);
//        return AjaxResult.success(collect);
        return null;
    }

    @GetMapping("getRotationList")
    public AjaxResult getRotationList(String position, String city) {
//        List<RotationPicViewEntity> collect = rotationService.getRotationList(position, city);
//        return AjaxResult.success(collect);
        return null;
    }
}