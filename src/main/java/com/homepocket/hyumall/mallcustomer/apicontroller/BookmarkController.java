package com.homepocket.hyumall.mallcustomer.apicontroller;

import com.homepocket.hyumall.common.annotation.TokenRequired;
import com.homepocket.hyumall.common.annotation.TokenUser;
import com.homepocket.hyumall.common.domain.AjaxResult;
import com.homepocket.hyumall.common.utils.JPushShopUtils;
import com.homepocket.hyumall.common.utils.StringUtils;
import com.homepocket.hyumall.mallcustomer.apiservice.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;

/**
 * @author Liang
 * @date 2020/7/9 8:40
 **/
@RestController
public class BookmarkController {

    @Autowired
    private JPushShopUtils jPushUtils;

    @Autowired
    private ProductService productService;

//    @TokenRequired
//    @GetMapping("")

    @GetMapping("testpush")
    public AjaxResult testPush() {
        int i = jPushUtils.sendToBieMing("13424349836","测试title", "测试TITLE", "测试内容", new HashMap<>());
        return AjaxResult.success(i);
    }

    @TokenRequired
    @PostMapping("addBookmark")
    public AjaxResult addBookmark (@TokenUser String userId, String productId) {
        int i = productService.addBookmark(userId, productId);
        if (i == 0) {
            return  AjaxResult.error("添加心愿单失败");
        } else if (i == 99) {
            return  AjaxResult.isExist();
        } else {
            return AjaxResult.success("添加心愿单成功");
        }
    }

    @TokenRequired
    @PostMapping("addBookmarks")
    public AjaxResult addBookmarks (@TokenUser String userId, String productIds) {
        int i = productService.addBookmarks(userId, productIds);
        if (i == 0) {
            return  AjaxResult.error("批量添加心愿单失败");
        } else {
            return AjaxResult.success("批量添加心愿单成功");
        }
    }

    @TokenRequired
    @GetMapping("deleteBookmark")
    public AjaxResult deleteBookmark (@TokenUser String userId, @RequestParam("bookmarkId") String bookmarkId) {
        int i = productService.deleteBookmark(userId, bookmarkId);
        if (i == 0) {
            return  AjaxResult.error("删除回收站商品失败");
        } else if (i == 999) {
            return AjaxResult.tokenError();
        }  else {
            return AjaxResult.success("删除回收站商品成功");
        }
    }

    @TokenRequired
    @GetMapping("delBookByProductId")
    public AjaxResult delBookByProductId (@TokenUser String userId, @RequestParam("productIds") String productIds) {
        int i = productService.delBookByProductId(userId, productIds);
        if (i == 0) {
            return  AjaxResult.error("删除心愿单失败");
        } else if (i == 999) {
            return AjaxResult.tokenError();
        }  else {
            return AjaxResult.success("删除心愿单成功");
        }
    }

    @TokenRequired
    @GetMapping("getBookmarks")
    public AjaxResult getBookmarks (
            @TokenUser String userId,
            @RequestParam(name = "categoryId", defaultValue = "-1", required = false)  String categoryId,
            String ownerId, @RequestParam(name = "status", defaultValue = "0", required = false) String status,
            @RequestParam(name = "pageSize", defaultValue = "10",required = false) Integer pageSize,
            @RequestParam(name = "pageNum", defaultValue = "1",required = false) Integer pageNum
            ) {
        if ("-1".equals(categoryId)) {
            categoryId = null;
        }
        if (StringUtils.isNotEmpty(ownerId)) {
            return AjaxResult.success(productService.getBookmarkProducts(ownerId,categoryId,status,pageSize,pageNum));
        }
        return AjaxResult.success(productService.getBookmarkProducts(userId,categoryId,status,pageSize,pageNum));
    }

    @TokenRequired
    @PostMapping("removeBookmark")
    public AjaxResult removeBookmark (@TokenUser String userId, String bookmarkId) {
        int i = productService.removeBookmark(userId,bookmarkId);
        if (i == 0) {
            return  AjaxResult.error("加入回收站失败");
        } else if (i == 999) {
            return AjaxResult.tokenError();
        } else {
            return AjaxResult.success("加入回收站成功");
        }
    }

    @TokenRequired
    @PostMapping("recoverBookmark")
    public AjaxResult recoverBookmark (@TokenUser String userId, String bookmarkId, String productId) {
        int i = productService.recoverBookmark(userId,bookmarkId,productId);
        if (i == 0) {
            return  AjaxResult.error("恢复心愿单失败");
        } else if (i == 99) {
            return  AjaxResult.isExist();
        } else if (i == 999) {
            return AjaxResult.tokenError();
        } else {
            return AjaxResult.success("恢复心愿单成功");
        }
    }
}
