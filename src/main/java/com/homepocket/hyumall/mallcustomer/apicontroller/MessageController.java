package com.homepocket.hyumall.mallcustomer.apicontroller;

import com.homepocket.hyumall.common.annotation.TokenRequired;
import com.homepocket.hyumall.common.annotation.TokenUser;
import com.homepocket.hyumall.common.domain.AjaxResult;
import com.homepocket.hyumall.mallcustomer.apiservice.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Liang
 * @date 2020/8/31 16:18
 **/
@RestController
public class MessageController {

    @Autowired
    private UserService userService;

    @TokenRequired
    @GetMapping("getPushList")
    public AjaxResult getPushList(
            @TokenUser String userId,
            @RequestParam(name = "pageSize", defaultValue = "10", required = false) Integer pageSize,
            @RequestParam(name = "pageNum", defaultValue = "1", required = false) Integer pageNum) {
        return AjaxResult.success(userService.getUserMessageList(pageSize, pageNum, userId));
    }
}
