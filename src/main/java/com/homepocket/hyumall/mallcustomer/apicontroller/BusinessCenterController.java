package com.homepocket.hyumall.mallcustomer.apicontroller;

import com.homepocket.hyumall.common.domain.AjaxResult;
import com.homepocket.hyumall.mallcustomer.apiservice.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author Liang
 * @Date 2020/6/15 23:29
 * @Description
 **/
@RestController
public class BusinessCenterController {
    @Autowired
    private ProductService service;
    @GetMapping("getCenterByCity")
    public AjaxResult getCenterByCity(String province, String city) {
        return AjaxResult.success(service.getBusinessCenterByCityCode(province, city));
    }
}
