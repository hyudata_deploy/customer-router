package com.homepocket.hyumall.mallcustomer.apicontroller;

import com.google.common.base.Joiner;
import com.homepocket.hyumall.common.annotation.CheckToken;
import com.homepocket.hyumall.common.annotation.TokenRequired;
import com.homepocket.hyumall.common.annotation.TokenUser;
import com.homepocket.hyumall.common.domain.*;
import com.homepocket.hyumall.common.enums.RedisKeyEnum;
import com.homepocket.hyumall.common.utils.StringUtils;
import com.homepocket.hyumall.common.vo.*;
import com.homepocket.hyumall.common.vo.product.ProductDetailVo;
import com.homepocket.hyumall.common.vo.product.ProductViewOnsaleVo;
import com.homepocket.hyumall.common.vo.user.UserAddrVo;
import com.homepocket.hyumall.mallcustomer.apiservice.OrderService;
import com.homepocket.hyumall.mallcustomer.apiservice.ProductService;
import com.homepocket.hyumall.mallcustomer.apiservice.UserService;
import com.homepocket.hyumall.mallcustomer.utils.RedisUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @Author Liang
 * @Date 2020/6/16 14:54
 * @Description
 **/
@RestController
public class ProductController {
    @Autowired
    private ProductService productService;

    @Autowired
    private OrderService orderService;

    @Autowired
    private UserService userService;

    @Autowired
    private RedisUtils redisUtils;

//    @Crypto
    @GetMapping("getProductList")
    public AjaxResult getProductList(
            String priceOrder, String boughtOrder,String fromPrice, String toPrice,String condition,
            String province, String city, String brandId,String typeId,
            String categoryId, String centerId, String businessId,
            String searchName,
            @RequestParam(name = "pageSize", defaultValue = "10", required = false) Integer pageSize,
            @RequestParam(name = "pageNum", defaultValue = "1", required = false) Integer pageNum) {
        if (searchName != null) {
            searchName = searchName.trim();
        }
//         获取第一页数据
        BasePage<ProductView> productList = productService.
                getProductList(priceOrder, boughtOrder,
                        fromPrice, toPrice,condition,province, city,
                        brandId,typeId, categoryId, centerId, businessId,
                        searchName, pageSize, pageNum);
//        Object productList = new Object();
        return AjaxResult.success(productList);
    }

    @GetMapping("getSearches")
    public AjaxResult getSearches(String priceOrder, String boughtOrder,
                                  String province, String city, String brandId,String typeId,
                                  String categoryId, String centerId, String businessId,
                                  String searchName) {
        if (searchName != null) {
            searchName = searchName.trim();
        }
        if (StringUtils.isEmpty(brandId)) {
            brandId = null;
        }
        // 获取全部数据
        BasePage<ProductView> whereList = productService.
                getProductList(priceOrder, boughtOrder, null,null, null, province, city,
                        brandId,typeId, categoryId, centerId, businessId,
                        searchName, Integer.MAX_VALUE, 1);
        // 获取全部数据的list
        List<ProductView> records = whereList.getRecords();
        // 获取三级分类ID
        List<String> categoryIdList = new ArrayList<>();
        List<String> brandIdList = new ArrayList<>();
        List<String> centerIdList = new ArrayList<>();
        records.forEach(
                r->{
                    String tempTypeId = r.getTypeId();
                    String tempBrandId = r.getBrandId();
                    String tempCenterId = r.getCenterId();
                    if(StringUtils.isNotEmpty(tempTypeId) && categoryIdList.indexOf(tempTypeId) == -1) {
                        categoryIdList.add(tempTypeId);
                    }
                    if(StringUtils.isNotEmpty(tempBrandId)  && brandIdList.indexOf(tempBrandId) == -1) {
                        brandIdList.add(tempBrandId);
                    }
                    if(StringUtils.isNotEmpty(tempCenterId)  && centerIdList.indexOf(tempCenterId) == -1) {
                        centerIdList.add(tempCenterId);
                    }
                }
        );
        List<IdNameVo> centerVoList = new ArrayList<>();
        List<IdNameVo> brandVoList = new ArrayList<>();
        List<IdNameVo> categoryVoList = new ArrayList<>();
        if (categoryIdList.size() > 0) {
            String categoryIds = Joiner.on(",").join(categoryIdList);
            List<ProductCategory> categoryList = productService.getCategoryListByIds(categoryIds);
            // 去重并且封装为前台需要的键值对格式
            categoryVoList = categoryList.stream().map(r -> {
                IdNameVo vo = new IdNameVo();
                vo.setId(r.getId());
                vo.setName(r.getName());
                return vo;
            }).collect(Collectors.toList());
        }
        if (brandIdList.size() > 0) {
            String brandIds = Joiner.on(",").join(brandIdList);
            List<Brand> brandList = productService.getBrandListByIds(brandIds);
            // 去重并且封装为前台需要的键值对格式
            brandVoList = brandList.stream().map(r -> {
                IdNameVo vo = new IdNameVo();
                vo.setId(r.getId());
                vo.setName(r.getName());
                return vo;
            }).collect(Collectors.toList());
        }
        if (centerIdList.size() > 0) {
            String centerIds = Joiner.on(",").join(centerIdList);
            List<BusinessCenter> centerList = productService.getCenterListByIds(centerIds);
            // 去重并且封装为前台需要的键值对格式
            centerVoList = centerList.stream().map(r -> {
                IdNameVo vo = new IdNameVo();
                vo.setId(r.getId());
                vo.setName(r.getName());
                return vo;
            }).distinct().collect(Collectors.toList());
        }
        List<ConditionVo> vos =
                productService.getSearchList(province, city, brandId, categoryId, centerId, businessId, searchName);
        // 查询材质
        List<IdNameVo> materialVoList = vos.stream().map(r -> {
            IdNameVo vo = new IdNameVo();
            vo.setId(r.getId());
            vo.setName(r.getName());
            return vo;
        }).filter(r-> null != r.getName())
                .collect(Collectors.collectingAndThen(
                Collectors.toCollection(
                        ()-> new TreeSet<>(
                                Comparator.comparing(IdNameVo::getName)
                        )
                ), ArrayList::new));
        SearchConditionVo vo = new SearchConditionVo(centerVoList,brandVoList,categoryVoList,materialVoList);
        if (brandId != null) {
            vo = new SearchConditionVo(centerVoList,null,categoryVoList,materialVoList);
        }

        return AjaxResult.success(vo);
//        return null;
    }

    @CheckToken
    @GetMapping("getProductOnSaleList")
    public AjaxResult getProductOnSaleList(
            @TokenUser String userId,
            String province, String city,
            String type,  @RequestParam(name = "status", defaultValue = "0", required = false) String status,
            @RequestParam(name = "pageSize", defaultValue = "10", required = false) Integer pageSize,
            @RequestParam(name = "pageNum", defaultValue = "1", required = false) Integer pageNum) {
        BasePage<ProductViewOnsaleVo> productOnSaleList =
                productService.getProductOnSaleList(province, city, type, status, pageSize, pageNum);
        if (userId != null) {
            List<ProductViewOnsaleVo> records = productOnSaleList.getRecords();
            List<String> ids = redisUtils.get("remind" + userId, List.class);
            System.out.println("==========redis中的ids为+" + ids);
            for (ProductViewOnsaleVo record : records) {
                if (ids != null) {
                    for (String id : ids) {
                        if (id.equals(record.getProductId())) {
                            record.setIsRemind("1");
                            break;
                        }
                        record.setIsRemind("0");
                    }
                } else {
                    record.setIsRemind("0");
                }
            }
            productOnSaleList.setRecords(records);
        }
        return AjaxResult.success(productOnSaleList);
//        return null;
    }

    @GetMapping("getHotSearchList")
    public AjaxResult getHotSearchList() {
        List<String> collect = productService.getHotSearchList().stream().map(
                HotSearch::getKeyWord).collect(Collectors.toList());
        return AjaxResult.success(collect);
//        return null;
    }

    @GetMapping("getBrandDetailById")
    public AjaxResult getBrandDetailById(@RequestParam("brandId") String brandId,
                                         @RequestParam(name = "lat", required = false) String lat,
                                         @RequestParam(name = "lng", required = false) String lng)   {
        return AjaxResult.success(productService.getBrandDetailById(brandId,lat,lng));
    }

    @TokenRequired
    @GetMapping("remindMe")
    public AjaxResult remindMe(@TokenUser String userId, @RequestParam("productId") String id, @RequestParam("type") String type) {
        ProductDetailVo vo = productService.getProductById(id);
        Date startTime = vo.getStartTime();
        Long expireTime = startTime.getTime() - (5 * 60 * 1000) ;
        Long fin = (expireTime - System.currentTimeMillis()) / 1000;
        if (fin <= 0) {
            return AjaxResult.error("商品已过提醒时间");
        }
        String pre = RedisKeyEnum.REMINDEXPIREKEY.getMsg();
        String hget = redisUtils.get(pre + userId + id);
        List<String> ids = redisUtils.get("remind" + userId, List.class);
        if (hget == null && "1".equals(type)) {
            // 尚未设置提醒
            if (ids != null) {
                ids.add(id);
            } else {
                ids = new ArrayList<>();
                ids.add(id);
            }
            redisUtils.set("remind" + userId, ids);
            redisUtils.set(pre + userId + id, id, fin);
            return AjaxResult.success("设置提醒成功");
        } else if (hget != null && "0".equals(type)){
            ids.remove(id);
            redisUtils.set("remind" + userId, ids);
            redisUtils.delete(pre + userId + id);
            return AjaxResult.success("取消提醒成功");
        } else {
            return AjaxResult.error("参数错误");
        }
    }

    @CheckToken
    @GetMapping("getProductById")
    public AjaxResult getProductById(@TokenUser String userId, @RequestParam("productId") String id) {
        ProductDetailVo vo = productService.getProductById(id);
        if (vo != null ) {
            String province = "22";
            String city = "01";
            String block = "02";
            String country = null;
            vo.setAddress("吉林省长春市南关区");
            if ( userId != null && userId.length() > 0) {
                List<UserAddrVo> userAddrAll = userService.findUserAddrAll(userId);
                // 有收货地址
                if (userAddrAll != null && userAddrAll.size() > 0) {
                    UserAddrVo userAddrVo = userAddrAll.get(0);
                    province = userAddrVo.getProvince();
                    city = userAddrVo.getCity();
                    block = userAddrVo.getArea();
                    country = userAddrVo.getCounty();
                    vo.setAddressId(userAddrVo.getId());
                    String addressAddr = userAddrVo.getProvinceCn() + userAddrVo.getCityCn() + userAddrVo.getAreaCn();
                    vo.setAddress(addressAddr);
                }
            }

            List<ProductFreight> freights = productService.getFreightByModel(id, province,city,block,country);
            if (freights != null && freights.size() > 0) {
                ProductFreight productFreightEntity = freights.get(0);
                String freight = productFreightEntity.getPrice();
                vo.setFreight(freight);
                vo.setIsFreight("1");
            } else {
                vo.setFreight("当前地址不支持配送,请与卖家联系");
                vo.setIsFreight("0");
            }
            if ("1".equalsIgnoreCase(vo.getType())) {
                List<GroupUserVo> productGroup = orderService.getProductGroup(vo.getProductId());
                vo.setGroupUsers(productGroup);
                int size = productGroup.size();
                if (size > 0) {
                    vo.setOrderMessage("等" + size + "人正在拼团, 可直接参与");
                } else {
                    vo.setOrderMessage("快来参与拼团吧");
                }
            }
        }
        return AjaxResult.success(vo);
    }

    @TokenRequired
    @GetMapping("checkAppointment")
    public AjaxResult checkAppointment(@TokenUser String userId, String businessId){
        int i = productService.checkAppointment(userId, businessId);
//        if (i> 0) {
            Object a = 0;
            return AjaxResult.success(a);
//        } else {
//            Object a = 1;
//            return AjaxResult.success(a);
//        }
    }


    @TokenRequired
    @PostMapping("addAppointment")
    public AjaxResult addAppointment(@TokenUser String userId, String businessId, String productId) {
        int i = productService.addAppointment(userId, productId, businessId);
        return i>0? AjaxResult.success(): AjaxResult.error();
    }

    @GetMapping("getFreight")
    public AjaxResult getFreight(@TokenUser User user,
                                 @RequestParam("id") String id,
                                 @RequestParam("provinceId") String province,
                                 @RequestParam("cityId") String city,
                                 @RequestParam(name = "areaId", required = false) String block,
                                 @RequestParam(name = "countryId", required = false) String country) {
        List<ProductFreight> freights = productService.getFreightByModel(id, province,city,block,country);
        ProductDetailVo vo = new ProductDetailVo();
        if (freights != null && freights.size() > 0) {
            ProductFreight productFreightEntity = freights.get(0);
            String freight = productFreightEntity.getPrice();
            vo.setIsFreight("1");
            vo.setFreight(freight);
        } else {
            vo.setFreight("当前地址不支持配送,请与卖家联系");
            vo.setIsFreight("0");
        }
        return AjaxResult.success(vo);
    }

    @GetMapping("getProductCommentByProductId")
    public AjaxResult getProductCommentByProductId(
            @RequestParam("id")String id,String type,
            @RequestParam(name = "pageSize", defaultValue = "10", required = false)Integer pageSize,
            @RequestParam(name = "pageNum", defaultValue = "1", required = false)Integer pageNum) {
        return AjaxResult.success(productService.getProductCommentByProductId(id, type, pageSize, pageNum));
    }


    @GetMapping("selectCategoryList")
    public AjaxResult selectCategoryList(String parentId){
        List<CategoryVo> categoryVos;
        if (null != parentId) {
            categoryVos= productService.getParentCategories(parentId);
        } else {
            categoryVos= productService.getCategories();
        }
        return AjaxResult.success(categoryVos);
    }

    @GetMapping("getBrandByCategoryId")
    public AjaxResult getBrandByCategoryId(@RequestParam(name = "id", defaultValue = "0")  String id,
                                           @RequestParam(name="pageSize", defaultValue = "10", required = false) Integer pageSize,
                                           @RequestParam(name="pageNum", defaultValue = "1", required = false) Integer pageNum) {
        BasePage<BrandVo> vos = productService.getBrandByCategoryId(id, pageSize, pageNum);
        return AjaxResult.success(vos);
    }

    @GetMapping("getCountCommentsById")
    public AjaxResult getCountCommentsById(@RequestParam String id) {
        return AjaxResult.success(productService.getCountCommentsById(id));
    }
    @GetMapping("getShopsByBrandId")
    public AjaxResult getShopsByBrandId( @RequestParam String brandId,
                                        String lat, String lng,
                                        String province,String city,String block,
                                        @RequestParam(name="pageNum", defaultValue = "1") Integer pageNum,
                                        @RequestParam(name="pageSize", defaultValue = "10") Integer pageSize ) {
        return AjaxResult.success(
                productService.getShopsByBrandId(brandId,lat,lng,province,city,block,pageNum,pageSize));
    }
}
