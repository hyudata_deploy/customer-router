package com.homepocket.hyumall.mallcustomer.apicontroller;

import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.alipay.api.AlipayApiException;
import com.alipay.api.internal.util.AlipaySignature;
import com.github.binarywang.wxpay.bean.request.WxPayRefundRequest;
import com.github.binarywang.wxpay.bean.request.WxPayUnifiedOrderRequest;
import com.github.binarywang.wxpay.bean.result.WxPayRefundResult;
import com.github.binarywang.wxpay.bean.result.WxPayUnifiedOrderResult;
import com.github.binarywang.wxpay.exception.WxPayException;
import com.github.binarywang.wxpay.service.WxPayService;
import com.homepocket.hyumall.common.alipay.AlipayConfig;
import com.homepocket.hyumall.common.annotation.TokenRequired;
import com.homepocket.hyumall.common.annotation.TokenUser;
import com.homepocket.hyumall.common.domain.*;
import com.homepocket.hyumall.common.entity.ali.AliPayEntity;
import com.homepocket.hyumall.common.enums.OrderRefundEnum;
import com.homepocket.hyumall.common.enums.RedisKeyEnum;
import com.homepocket.hyumall.common.vo.aliRefund.AliRefundVo;
import com.homepocket.hyumall.common.vo.order.OrderDetailVo;
import com.homepocket.hyumall.common.vo.order.ProductVo;
import com.homepocket.hyumall.common.vo.order.RefundPreVo;
import com.homepocket.hyumall.common.vo.pay.CombinePrePayVo;
import com.homepocket.hyumall.common.vo.pay.PayMethodVo;
import com.homepocket.hyumall.common.vo.pay.PayResponseVo;
import com.homepocket.hyumall.common.vo.pay.WxPreOrderVo;
import com.homepocket.hyumall.common.vo.refund.CalculateRefundVo;
import com.homepocket.hyumall.common.wxpay.*;
import com.homepocket.hyumall.mallcustomer.apiservice.AliService;
import com.homepocket.hyumall.mallcustomer.apiservice.OrderService;
import com.homepocket.hyumall.mallcustomer.apiservice.UserService;
import com.homepocket.hyumall.mallcustomer.apiservice.WechatService;
import com.homepocket.hyumall.mallcustomer.config.WeChatPayProperties;
import com.homepocket.hyumall.mallcustomer.utils.RedisUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author Liang
 * @date 2020/7/24 16:29
 **/
@RestController
public class PayController {

    @Value("${wechat_pay}")
    private Integer wechatPay;

    @Value("${ali_pay}")
    private Integer aliPay;


    @Autowired
    private OrderService orderService;

    @Autowired
    private AliService aliService;

    @Autowired
    private RedisUtils redisUtils;

    @Autowired
    private WxPayService wxService;

    @Autowired
    private WechatService wechatService;

    @Autowired
    private UserService userService;

    @Autowired
    WeChatPayProperties weChatPayProperties;

    @GetMapping("getPayMethods")
    public AjaxResult getPayMethods() {
        PayMethodVo vo = new PayMethodVo();
        vo.setPayCode("1");
        vo.setPayName("alipay");
        vo.setSupport(aliPay);
        PayMethodVo vo2 = new PayMethodVo();
        vo2.setPayCode("0");
        vo2.setPayName("wechatPay");
        vo2.setSupport(wechatPay);
        List<PayMethodVo> vos = new ArrayList<>();
        vos.add(vo);
        vos.add(vo2);
        return AjaxResult.success(vos);
    }

    @TokenRequired
    @GetMapping("aliPay")
    public AjaxResult alipay(@TokenUser String userId, @RequestParam String orderId) {
        Order order1 = orderService.getOrderEntity(orderId);
        if (!order1.getUserId().equals(userId) ) {
            return AjaxResult.error("支付失败");
        }
        CombinePrePayVo prePayOrder = orderService.getCombinePrePayOrder(orderId);
        AliPayEntity aliPayEntity = prePayOrder.getAliPayEntity();
        Object alipay = aliService.alipay(aliPayEntity);
        return AjaxResult.success(alipay);
    }

    @PostMapping("aliPayResponse")
    public String aliPayResponse(HttpServletRequest request) {
        //获取支付宝POST过来反馈信息
        Map<String,String> paramMap = new HashMap<String,String>();
        Map requestParams = request.getParameterMap();
        for (Iterator iter = requestParams.keySet().iterator(); iter.hasNext();) {
            String name = (String) iter.next();
            String[] values = (String[]) requestParams.get(name);
            String valueStr = "";
            for (int i = 0; i < values.length; i++) {
                valueStr = (i == values.length - 1) ? valueStr + values[i]
                        : valueStr + values[i] + ",";
            }
            //乱码解决，这段代码在出现乱码时使用。
            //valueStr = new String(valueStr.getBytes("ISO-8859-1"), "utf-8");
            paramMap.put(name, valueStr);
        }
        //切记alipaypublickey是支付宝的公钥，请去open.alipay.com对应应用下查看。
        //boolean AlipaySignature.rsaCertCheckV1(Map<String, String> params, String publicKeyCertPath, String charset,String signType)
        try{
            boolean flag = AlipaySignature.rsaCertCheckV1(paramMap, AlipayConfig.ALIPAY_CERT_PATH, AlipayConfig.CHARSET,"RSA2");
            if (flag) {
                //
                String tradeNo = request.getParameter("out_trade_no");
                String aliTradeNo = request.getParameter("trade_no");
                String tradeStatus = request.getParameter("trade_status");
                String refundFee = request.getParameter("refund_fee");
                // 退款的ID
                String trade_no = request.getParameter("out_biz_no");
                String payment = "1";
                if ("TRADE_FINISHED".equals(tradeStatus) || "TRADE_SUCCESS".equals(tradeStatus) || "TRADE_CLOSED".equalsIgnoreCase(tradeStatus)) {
                    if (refundFee != null && Double.parseDouble(refundFee) != 0) {
                        int i = (int) (Double.parseDouble(refundFee) * 100);
                        if (dealRefund(tradeNo, trade_no,i)) {
                            return "success";
                        } else {
                            return "error";
                        }
                    } else {
                        String pay = request.getParameter("buyer_pay_amount");
                        BigDecimal bigDecimal = new BigDecimal(pay);
                        BigDecimal bigDecimal1 = bigDecimal.multiply(new BigDecimal(100));
                        Integer payAmount = bigDecimal1.intValue();
                        dealOrder(tradeNo, payAmount, payment, aliTradeNo);
                        return "success";
                    }
                } else {
                    // 支付失败
                    return "error";
                }
            }
        } catch (AlipayApiException ex) {
            System.out.println("错误了===="+ex.getErrCode()+"======");
        }
        return "error";
    }

    private boolean dealRefund(String orderId, String refundId, Integer refundFee) {
        int i = orderService.changeRefundStatus(orderId, refundId, refundFee);
        attemptProfit(orderId, refundFee);
        return i == 2;
    }

    private String attemptProfit(String orderId, Integer refundFee) {
        ShopWater shopWaterByOrderId = orderService.getShopWaterByOrderId(orderId);
        if (shopWaterByOrderId == null) {
            return null;
        }
        Order orderEntity = orderService.getOrderEntity(orderId);
        String payment = shopWaterByOrderId.getPayment();
        if ("1".equals(payment)) {
            return null;
        }
        String id = shopWaterByOrderId.getId();
        Integer amount = shopWaterByOrderId.getAmount();
        Integer totalAmount = amount - refundFee;
        WxProfitVo wxProfitVo = new WxProfitVo();
        wxProfitVo.setWxOrderId(orderEntity.getWxOrderId());
        wxProfitVo.setType(payment);
        wxProfitVo.setOutOrderNo(shopWaterByOrderId.getId());
        wxProfitVo.setSubMchId(orderEntity.getWxMchId());
        wxProfitVo.setPlatformAmount(calculateMoney(totalAmount));
        wxProfitVo.setDescription("平台抽佣" + totalAmount + "分");
        return wechatService.profitSharing(wxProfitVo);
    }

    /**
     * 计算平台佣金
     * @return
     */
    private Integer calculateMoney(Integer totalAmount) {
        if (totalAmount >= 10) {
            return new BigDecimal(totalAmount)
                    .multiply(new BigDecimal("3"))
                    .divide(new BigDecimal(100), 0, BigDecimal.ROUND_UP).intValue();
        }
        return 0;
    }

    @TokenRequired
    @GetMapping("wxapiPay")
    public AjaxResult wxapiPay(@TokenUser String userId,@RequestParam String orderId, @RequestParam String openId) {
        Order order1 = orderService.getOrderEntity(orderId);
        if (!order1.getUserId().equals(userId) ) {
            return AjaxResult.error("支付失败");
        }
        WxPreOrderVo preOrderVo = orderService.getCombinePrePayOrder(orderId).getWxPreOrderVo();
        preOrderVo.setParentOrderId(orderId);
        preOrderVo.setOpenId(openId);
        return AjaxResult.success(wechatService.createApiPreOrder(preOrderVo));
    }

    @TokenRequired
    @GetMapping("wxPay")
    public AjaxResult wxpay(@TokenUser String userId,@RequestParam String orderId){
        Order orderDetail = orderService.getOrderEntity(orderId);
        if (!orderDetail.getUserId().equals(userId) ) {
            return AjaxResult.error("支付失败");
        }
        WxPreOrderVo preOrderVo = orderService.getCombinePrePayOrder(orderId).getWxPreOrderVo();
        preOrderVo.setParentOrderId(orderId);
        return AjaxResult.success(wechatService.createPreOrder(preOrderVo));
//        private String subOrderId;
//
//        private String subMchId;
//
//        private String productId;
//
//        private String productDescription;
//
//        private Integer orderAmount;

//        WxPayUnifiedOrderRequest request = new WxPayUnifiedOrderRequest();
//        request.setBody(order.getProductName());
//        request.setSpbillCreateIp(clientIp);
//        request.setOutTradeNo(orderId);
//        request.setTotalFee(order.getShouldPay());
//        if (StringUtils.isNotEmpty(tradeType)) {
//            request.setOpenid(openId);
//            request.setTradeType(tradeType);
//            request.setAppid("wxf3d90af724dafe87");
//        }
//        request.setNotifyUrl("http://www.thehomepocket.com/cus/wxPayResponse");
//        try {
//            if(StringUtils.isEmpty(tradeType)){
//                WxPayAppOrderResult wxPayUnifiedOrderResult = this.wxService.createOrder(request);
//                return AjaxResult.success(wxPayUnifiedOrderResult);
//            }else{
//                WxPayMpOrderResult wxPayUnifiedOrderResult = this.wxService.createOrder(request);
//                return AjaxResult.success(wxPayUnifiedOrderResult);
//            }
//
//
//        } catch (WxPayException e) {
//            e.printStackTrace();
//        }
//        return AjaxResult.error();
    }

//    private void wxProfitSharing() {
//        ProfitSharingService profitSharingService = wxService.getProfitSharingService();
//        profitSharingService.
//    }

    public static String getClientIP(HttpServletRequest httpservletrequest) {
        if (httpservletrequest == null){
            return null;
        }
        String s = httpservletrequest.getHeader("X-Forwarded-For");
        if (s == null || s.length() == 0 || "unknown".equalsIgnoreCase(s)){
            s = httpservletrequest.getHeader("Proxy-Client-IP");}
        if (s == null || s.length() == 0 || "unknown".equalsIgnoreCase(s)){
            s = httpservletrequest.getHeader("WL-Proxy-Client-IP");}
        if (s == null || s.length() == 0 || "unknown".equalsIgnoreCase(s)){
            s = httpservletrequest.getHeader("HTTP_CLIENT_IP");}
        if (s == null || s.length() == 0 || "unknown".equalsIgnoreCase(s)){
            s = httpservletrequest.getHeader("HTTP_X_FORWARDED_FOR");}
        if (s == null || s.length() == 0 || "unknown".equalsIgnoreCase(s)){
            s = httpservletrequest.getRemoteAddr();}
        return s;
    }
//    @PostMapping("wxPayResponse")
//    public WxPayNotifyResponse parseOrderNotifyResult(@RequestBody String xmlData) throws WxPayException {
//        final WxPayOrderNotifyResult notifyResult = this.wxService.parseOrderNotifyResult(xmlData);
//        System.out.println("微信支付回调成功======结果为:" + notifyResult.toString());
//        WxPayOrderNotifyResult wxPayOrderNotifyResult = wxService.parseOrderNotifyResult(xmlData);
//        WxPayNotifyResponse wxPayNotifyResponse = new WxPayNotifyResponse();
//        if ("success".equalsIgnoreCase(wxPayOrderNotifyResult.getResultCode())) {
//            String orderId = notifyResult.getOutTradeNo();
//            Integer payAmount = notifyResult.getTotalFee();
//            String payment = "0";
//            dealOrder(orderId, payAmount, payment);
//            wxPayNotifyResponse.setReturnCode("200");
//            wxPayNotifyResponse.setReturnMsg(null);
//            return wxPayNotifyResponse;
//        } else {
//            wxPayNotifyResponse.setReturnCode("500");
//            wxPayNotifyResponse.setReturnMsg("错误");
//            return wxPayNotifyResponse;
//        }
//    }

    @PostMapping("wxPayResponse")
    public JSONObject wxPayResponse(@RequestBody String requestJsonStr, @RequestHeader HttpHeaders headers, HttpServletResponse response) {
        System.out.println("微信支付结果通知接受成功，header为：" + headers + "\n body为：" + requestJsonStr);
        //返回通知的应答报文，code(32)：SUCCESS为清算机构接收成功；message(64)：错误原因
        JSONObject responseJson = new JSONObject();
        responseJson.put("code", "FAIL");
        //支付通知http应答码为200或204才会当作正常接收，当回调处理异常时，应答的HTTP状态码应为500，或者4xx。
        response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);

        if (!headers.containsKey("Wechatpay-Serial") || !headers.containsKey("Wechatpay-Timestamp")
                || !headers.containsKey("Wechatpay-Nonce") || !headers.containsKey("Wechatpay-Signature")) {
            System.out.println("回调请求header缺失");
            responseJson.put("message", "回调请求header缺失");
            return responseJson;
        }

        String WechatpaySerial = headers.getFirst("Wechatpay-Serial");//平台证书序列号
        String WechatpayTimestamp = headers.getFirst("Wechatpay-Timestamp");//应答时间戳
        String WechatpayNonce = headers.getFirst("Wechatpay-Nonce");//应答随机串
        String WechatpaySignature = headers.getFirst("Wechatpay-Signature"); //应答签名
        if (!WechatpaySerial.equals(WxPayConfig.api_v3_cert_serial_no)) {
            System.out.println("回调请求证书序列化不一致");
            responseJson.put("message", "回调请求证书序列化不一致");
            return responseJson;
        }

        //获取签名串，验签
        String srcData = WechatpayTimestamp + "\n" + WechatpayNonce + "\n" + requestJsonStr + "\n";
        if (!WxPayUtils.verify(srcData, WechatpaySignature)) {
            System.out.println("验签失败");
            responseJson.put("message", "验签失败");
            return responseJson;
        }

        JSONObject requestJson = JSONObject.parseObject(requestJsonStr);
        //通知的类型，支付成功通知的类型为 TRANSACTION.SUCCESS
        String event_type = requestJson.get("event_type").toString();
        //通知数据
        JSONObject resource = JSONObject.parseObject(requestJson.get("resource").toString());
        //数据密文，Base64编码后的开启/停用结果数据密文
        String ciphertext = resource.get("ciphertext").toString();
        String associated_data = resource.get("associated_data").toString();
        String nonce = resource.get("nonce").toString();

        //对密文串进行解密
        String verify = WxPayUtils.getNotifyData(associated_data, nonce, ciphertext);
        JSONObject paySuccess = JSONObject.parseObject(verify);
        System.out.println("paySuccess====" + paySuccess);
        JSONArray sub_orders = JSON.parseArray(paySuccess.get("sub_orders").toString());
        //合单商户订单号
        String orderNo = paySuccess.get("combine_out_trade_no").toString();
        if (StrUtil.isBlank(orderNo)) {
            responseJson.put("message", "微信支付成功，未接受到合单商户订单号");
            return responseJson;
        }
        for (Object subOrder : sub_orders) {
            JSONObject sub_order = JSONObject.parseObject(subOrder.toString());
            //微信子单订单号
            String wxOrderNo = sub_order.get("transaction_id").toString();
            String subOrderNo = sub_order.get("out_trade_no").toString();
            if (StrUtil.isBlank(wxOrderNo)) {
                responseJson.put("message", "微信支付成功，未接受到微信订单号");
                System.out.println();
                responseJson.put("message", "微信支付成功，未接受到微信订单号");
                return responseJson;
            }
            String pay_info = sub_order.get("amount").toString();
            JSONObject payInfo = JSONObject.parseObject(pay_info);
            Integer payAmount = Integer.parseInt(payInfo.get("payer_amount").toString());
            //支付失败
            if (!"TRANSACTION.SUCCESS".equals(event_type)) {
                System.out.println("微信支付失败");
                responseJson.put("message", "微信支付失败，商户处理失败");
                return responseJson;
            }
            String payment = "0";
            //支付成功
            dealOrder(subOrderNo, payAmount, payment, wxOrderNo);
        }


        response.setStatus(HttpServletResponse.SC_OK);
        responseJson.put("code", "SUCCESS");
        responseJson.put("message", "微信支付成功，商户处理成功");
        return responseJson;
    }

    @TokenRequired
    @GetMapping("preRefund")
    public AjaxResult preRefund(@TokenUser String userId, String orderId, String orderProductId) {
        OrderDetailVo orderDetailById = orderService.getOrderDetailById(orderId);
        String userId1 = orderDetailById.getUserId();
        String sizeDetailId = "";
        if (!userId.equals(userId1)) {
            return AjaxResult.tokenError();
        }
        RefundPreVo vo = new RefundPreVo();
        List<ProductVo> orderProducts = orderService.getOrderProductByOrderId(orderId);
        Optional<ProductVo> first = orderProducts.stream()
                .filter(r -> orderProductId.equals(r.getOrderProductId()))
                .findFirst();
        if (first.isPresent()) {
            ProductVo productVo = first.get();
            sizeDetailId = productVo.getSizeDetailId();
            vo.setProductName(productVo.getProductName());
            vo.setProductPrice(productVo.getPrice());
            vo.setSizeName(productVo.getSizeName());
            vo.setProductPic(productVo.getProductPic());
            vo.setNum(productVo.getNum());
            vo.setSizeDetailId(sizeDetailId);
        }
        List<OrderTransportProduct> transportVos = orderService.getTransportProduct(orderId);
        String sizeId = sizeDetailId;
        vo.setIfFast(1);
        if (transportVos.stream().anyMatch(r->r.getSizeId().equals(sizeId))) {
            vo.setIfFast(0);
        }
        return AjaxResult.success(vo);
    }

//    @GetMapping("calculateRefund")
//    public AjaxResult calculateRefund(String orderId , String sizeDetailId, Integer num,  Integer amount){
//
//    }

    // 弃用
    @PostMapping("profitSharing")
    public AjaxResult profitSharing(@RequestParam String orderId) {
        Order orderEntity = orderService.getOrderEntity(orderId);
        if (orderEntity == null) {
            return AjaxResult.error("订单号错误");
        }
        List<OrderRefund> refunds = orderService.getRefundsByOrderId(orderId);
        int reduceAmount = 0;
        String id = UUID.randomUUID().toString();
        ShopWater shopWater = new ShopWater();
        shopWater.setId(id);
        shopWater.setAmount(orderEntity.getRealPay() - reduceAmount);
        shopWater.setDeductMoney(1);
        shopWater.setOrderId(orderId);
        shopWater.setUserId(orderEntity.getUserId());
        shopWater.setShopId(orderEntity.getShopId());
        shopWater.setIsPay("0");
        shopWater.setStatus("0");
        shopWater.setPayment("1".equalsIgnoreCase(orderEntity.getPayment())? "1" : "0");
        shopWater.setType("0");
        shopWater.setIfRefund("0");
        for (OrderRefund refund : refunds) {
            if (
                    refund.getRefundStatus().equals(OrderRefundEnum.REFUND_GOODS_SUCCESS.getIndex() + "")
                    ||  refund.getRefundStatus().equals(OrderRefundEnum.REFUND_PRE.getIndex() + "")
                    ||  refund.getRefundStatus().equals(OrderRefundEnum.REFUND_ING.getIndex() + "")
            ) {
                shopWater.setIsPay("1");
                orderService.addShopWater(shopWater);
                return AjaxResult.error("订单未完成, 无法分账");
            } else if (
                    refund.getRefundStatus().equals(OrderRefundEnum.REFUND_SUCCESS.getIndex() + "")
            ) {
                // 退款成功 分账金额相应减少
                reduceAmount += refund.getRefundMoney();
                shopWater.setIfRefund("1");
                orderService.addShopWater(shopWater);
            }
        }
        WxProfitVo profitVo = new WxProfitVo();
        profitVo.setOutOrderNo(id);
        profitVo.setPlatformAmount(1);
        profitVo.setSubMchId(orderEntity.getWxMchId());
        String wxOrderId = orderEntity.getWxOrderId();
        String aliOrderId = orderEntity.getAliOrderId();
        String payment = orderEntity.getPayment();
        if ("0".equals(payment)) {
            profitVo.setType("0");
            profitVo.setWxOrderId(wxOrderId);
            profitVo.setPlatformAmount(1);
            wechatService.profitSharing(profitVo);
        } else if ("1".equals(payment)) {

        } else {
            profitVo.setType("1");
            profitVo.setWxOrderId(wxOrderId);
            profitVo.setPlatformAmount(1);
            wechatService.profitSharing(profitVo);
        }
        return AjaxResult.success("请求分账成功");
    }

    private CalculateRefundVo calculateDetail(String orderId, String sizeDetailId, Integer num, Integer amount){
        CalculateRefundVo vo = new CalculateRefundVo();
        OrderDetailVo orderDetailById = orderService.getOrderDetailById(orderId);
        List<ProductVo> orderProducts = orderService.getOrderProductByOrderId(orderId);
        // ==================验证当前待退款商品数量是否足够==========================
        Optional<ProductVo> first = orderProducts.stream()
                .filter(r -> r.getSizeDetailId().equals(sizeDetailId)).findFirst();
        ProductVo productVo = new ProductVo();
        if(first.isPresent()) {
            productVo = first.get();
        }
        int price = productVo.getPrice();
        int boughtNum = productVo.getNum();
        Integer coinNum = orderDetailById.getCoinNum();
        Integer coinDiscount = orderDetailById.getCoinDiscount();
        return vo;
    }

    /**
     *
     * @param userId 用户ID
     * @param orderId 订单ID
     * @param sizeDetailId 规格详情ID
     * @param num 退货数量
     * @param remark 退款说明
     * @param reason 退款原因
     * @param imgs 凭证
     * @param type 退货方式(仅退货退款必传)
     * @param refundType 退款方式 0极速退款 1 退款未收到货 2 退款收到货 3 退货退款
     * @param collectDate 上门取件时间
     * @param content 联系人
     * @param tel 电话
     * @param address 地址
     * @param expressName 快递公司
     * @param expressNo 快递单号
     * @param freight 快递运费
     * @param returnDate 自行送回时间
     * @return
     */
    @TokenRequired
    @GetMapping("refundOrder")
    public AjaxResult refundOrder(
            @TokenUser String userId, String orderId ,
            String sizeDetailId, Integer num, String remark,Integer amount,
            String reason, String imgs,String type, String refundType,
            String collectDate, String content, String tel, String address,
            String expressName, String expressNo, Integer freight, String returnDate)throws ParseException {
        Order order = orderService.getOrderEntity(orderId);
        String userId1 = order.getUserId();
        if (!userId.equals(userId1)) {
            return AjaxResult.tokenError();
        }
        String result = "";
        switch (refundType) {
            case "0" :
                // 极速退款
                result = fastRefund(orderId, sizeDetailId, refundType, num,reason, remark, imgs);
                break;
            case "1" :
                // 仅退款 未收到货
                result = refundMoney(orderId, sizeDetailId, refundType, num, reason, remark, imgs);
                break;
            case "2" :
                // 仅退款 已收到货
                result = refundMoney(orderId, sizeDetailId, refundType, num, reason, remark, imgs);
                break;
            case "3" :
                // 退货退款
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                Date cDate = null;
                Date rDate = null;
                if (collectDate != null) {
                    cDate = sdf.parse(collectDate);
                }
                if (returnDate != null) {
                    rDate = sdf.parse(returnDate);
                }
                result = refundGoods(orderId ,sizeDetailId, num,refundType,
                        reason, remark, imgs, type,amount,
                        cDate, content, tel, address,
                        expressName, expressNo, freight, rDate);
                break;
            default:
                break;
        }
        return AjaxResult.success(result);
    }

    /**
     * 极速退款方法
     * @param orderId
     * @param sizeDetailId
     * @param num
     * @param remark
     * @param imgs
     * @return
     */
    private String fastRefund(
            String orderId ,String sizeDetailId,String refundType,
            Integer num,String reason, String remark, String imgs) {
        List<OrderTransportProduct> transportVos = orderService.getTransportProduct(orderId);
        if (transportVos.stream().anyMatch(r->r.getSizeId().equals(sizeDetailId))) {
            return "卖家已发货, 无法极速退款";
        }
        OrderDetailVo orderDetailById = orderService.getOrderDetailById(orderId);
        List<ProductVo> orderProducts = orderService.getOrderProductByOrderId(orderId);
        List<OrderRefund> refunds = orderService.getRefundsByOrderId(orderId);
        // ==================验证当前待退款商品数量是否足够==========================
        Optional<ProductVo> first = orderProducts.stream()
                .filter(r -> r.getSizeDetailId().equals(sizeDetailId)).findFirst();
        ProductVo productVo = new ProductVo();
        if(first.isPresent()) {
            productVo = first.get();
        }
        List<OrderRefund> collect = refunds.stream()
                .filter(r -> sizeDetailId.equals(r.getSizeDetailId()))
                .collect(Collectors.toList());
        ProductVo finalProductVo = productVo;
        int a = 0;
        for (OrderRefund orderRefund : collect) {
            if ( "16".equals(orderRefund.getRefundStatus()) || "18".equals(orderRefund.getRefundStatus())) {
                a +=1;
            } else {
                return "该商品已申请退款, 请勿重复申请";
            }
        }
        if (a > 2) {
            return "退款次数超过上限, 请联系客服处理";
        }
        if (finalProductVo.getNum() < num) {
            return "退货数量错误";
        }
        // ==================================验证完毕=======================================

//        refunds.forEach(r->{
//            if ( !"16".equals(r.getRefundStatus()) && !"18".equals(r.getRefundStatus())) {
//                if (r.getRefundCoinNum() != null) {
//                    orderDetailById.setCoinNum(orderDetailById.getCoinNum() - r.getRefundCoinNum());
//                }
//                if (r.getRefundCoinAmount() != null) {
//                    orderDetailById.setCoinDiscount(orderDetailById.getCoinDiscount() - r.getRefundCoinAmount());
//                }
//                if (r.getRefundMoney() != null) {
//                    orderDetailById.setRealPay(orderDetailById.getRealPay() - r.getRefundMoney());
//                }
//            }
//        });
        String result = "";
//        int price = finalProductVo.getPrice();
//        Integer refundRealPay = orderDetailById.getRealPay();
//        Integer refundCoinNum = orderDetailById.getCoinNum();
//        String coinValue = userService.getParamsByKey("coin_value");
//        String coinLimit = userService.getParamsByKey("coin_limit");
//        Integer maxCoin = (int)(refundRealPay * Double.parseDouble(coinLimit));
//        Integer returnCoin = 0;
//        Integer returnMoney = price * num;
//        Integer returnCoinValue = 0;
//        if (maxCoin < refundCoinNum) {
//            // 返口袋币
//            returnCoin = refundCoinNum - maxCoin;
//            returnCoinValue = Integer.parseInt(coinValue) * returnCoin;
//            returnMoney -= returnCoinValue;
//        }
        OrderRefund refund = new OrderRefund();
        String id = System.currentTimeMillis() + "" + (int)((Math.random()*9+1)*1000000);
        refund.setId(id);
        refund.setOrderId(orderId);
        refund.setSizeDetailId(sizeDetailId);
        refund.setProductNum(num);
        refund.setRefundMoney(productVo.getProductPrice() * productVo.getNum());
        refund.setRefundCoinNum(orderDetailById.getCoinNum());
        refund.setPics(imgs);
        refund.setType(refundType);
        refund.setReason(reason);
        refund.setRefundCoinAmount(orderDetailById.getCoinDiscount());
        refund.setRefundStatus(OrderRefundEnum.REFUND_ING.getIndex() + "");
        refund.setShopId(orderDetailById.getBusinessId());
        refund.setUserId(orderDetailById.getUserId());
        refund.setDeleteFlag("0");
        refund.setCreateTime(new Date());
        refund.setRemark(remark);
        orderService.addRefund(refund);
        if ("0".equals(orderDetailById.getPayment())) {
            WxRefundApplyVo vo = new WxRefundApplyVo();
            vo.setAmount(refund.getRefundMoney());
            vo.setRefundNo(id);
            vo.setOriTotal(orderDetailById.getShouldPay());
            vo.setOrderId(orderId);
            vo.setMchId(orderDetailById.getWxMchId());
            vo.setReason(remark);
            result = wechatService.refundApply(vo);
        } else if ("1".equals(orderDetailById.getPayment())) {
            AliRefundVo vo = new AliRefundVo();
            int aaa = refund.getRefundMoney();
            BigDecimal bbb = new BigDecimal(aaa).divide(new BigDecimal(100), 2 , BigDecimal.ROUND_HALF_DOWN);
            vo.setAmount(bbb.toString());
            vo.setOrderId(orderId);
            vo.setReason(remark);
            vo.setRefundId(id);
            Boolean aBoolean = aliService.aliRefund(vo);
            if (aBoolean) {
                result = "退款申请成功";
            } else {
                result = "退款申请失败";
            }
        } else {
            return "订单支付信息错误";
        }
        return result;
    }

    /**
     * 退款方法
     * @param orderId
     * @param sizeDetailId
     * @param num
     * @param remark
     * @param imgs
     * @return
     */
    private String refundMoney(
            String orderId ,String sizeDetailId, String refundType,
            Integer num,String reason, String remark, String imgs) {
        OrderDetailVo orderDetailById = orderService.getOrderDetailById(orderId);
        List<ProductVo> orderProducts = orderService.getOrderProductByOrderId(orderId);
        List<OrderRefund> refunds = orderService.getRefundsByOrderId(orderId);
        // ==================验证当前待退款商品数量是否足够==========================
        Optional<ProductVo> first = orderProducts.stream()
                .filter(r -> r.getSizeDetailId().equals(sizeDetailId)).findFirst();
        ProductVo productVo = new ProductVo();
        if(first.isPresent()) {
            productVo = first.get();
        }
        List<OrderRefund> collect = refunds.stream()
                .filter(r -> sizeDetailId.equals(r.getSizeDetailId()))
                .collect(Collectors.toList());
        ProductVo finalProductVo = productVo;
        int a = 0;
        for (OrderRefund orderRefund : collect) {
            if ( "16".equals(orderRefund.getRefundStatus()) || "18".equals(orderRefund.getRefundStatus())) {
                a +=1;
            } else {
                return "该商品已申请退款, 请勿重复申请";
            }
        }
        if (a > 2) {
            return "退款次数超过上限, 请联系客服处理";
        }
        if (finalProductVo.getNum() < num) {
            return "退货数量错误";
        }
        // ==================================验证完毕=======================================
//         ===================================目前退款只支持一笔=====================================
//        refunds.forEach(r->{
//            if ( !"16".equals(r.getRefundStatus()) && !"18".equals(r.getRefundStatus())) {
//                if (r.getRefundCoinNum() != null) {
//                    orderDetailById.setCoinNum(orderDetailById.getCoinNum() - r.getRefundCoinNum());
//                }
//                if (r.getRefundCoinAmount() != null) {
//                    orderDetailById.setCoinDiscount(orderDetailById.getCoinDiscount() - r.getRefundCoinAmount());
//                }
//                if (r.getRefundMoney() != null) {
//                    orderDetailById.setRealPay(orderDetailById.getRealPay() - r.getRefundMoney());
//                }
//            }
//        });
        String result = "";
        int price = finalProductVo.getPrice();
        Integer refundRealPay = orderDetailById.getRealPay();
        Integer refundCoinNum = orderDetailById.getCoinNum();
        String coinValue = userService.getParamsByKey("coin_value");
        String coinLimit = userService.getParamsByKey("coin_limit");
        Integer maxCoin = (int)(refundRealPay * Double.parseDouble(coinLimit));
        Integer returnCoin = 0;
        Integer returnMoney = price * num;
        Integer returnCoinValue = 0;
        if (maxCoin < refundCoinNum) {
            // 返口袋币
            returnCoin = refundCoinNum - maxCoin;
            returnCoinValue = Integer.parseInt(coinValue) * returnCoin;
            returnMoney -= returnCoinValue;
        }
        OrderRefund refund = new OrderRefund();
        String id = System.currentTimeMillis() + "" + (int)((Math.random()*9+1)*1000000);
        refund.setId(id);
        refund.setOrderId(orderId);
        refund.setSizeDetailId(sizeDetailId);
        refund.setProductNum(num);
        refund.setRefundMoney(returnMoney);
        refund.setRefundCoinNum(returnCoin);
        refund.setPics(imgs);
        refund.setType(refundType);
        refund.setReason(reason);
        refund.setRefundCoinAmount(returnCoinValue);
        refund.setRefundStatus(OrderRefundEnum.REFUND_PRE.getIndex() + "");
        refund.setShopId(orderDetailById.getBusinessId());
        refund.setUserId(orderDetailById.getUserId());
        refund.setDeleteFlag("0");
        refund.setCreateTime(new Date());
        refund.setRemark(remark);
        orderService.addRefund(refund);
        return "退款申请已提交";
    }
    /**
     * 退款退货方法
     * @param orderId
     * @param sizeDetailId
     * @param num
     * @param remark
     * @param imgs
     * @return
     */
    private String refundGoods(
            String orderId ,String sizeDetailId, Integer num,String refundType,
            String reason, String remark, String imgs, String type,Integer amount,
            Date collectDate, String content, String tel, String address,
            String expressName, String expressNo, Integer freight, Date returnDate) {
        OrderDetailVo orderDetailById = orderService.getOrderDetailById(orderId);
        List<ProductVo> orderProducts = orderService.getOrderProductByOrderId(orderId);
        List<OrderRefund> refunds = orderService.getRefundsByOrderId(orderId);
        // ==================验证当前待退款商品数量是否足够==========================
        Optional<ProductVo> first = orderProducts.stream()
                .filter(r -> r.getSizeDetailId().equals(sizeDetailId)).findFirst();
        ProductVo productVo = new ProductVo();
        if(first.isPresent()) {
            productVo = first.get();
        }
        List<OrderRefund> collect = refunds.stream()
                .filter(r -> sizeDetailId.equals(r.getSizeDetailId()))
                .collect(Collectors.toList());
        ProductVo finalProductVo = productVo;
        int a = 0;
        for (OrderRefund orderRefund : collect) {
            if ( "16".equals(orderRefund.getRefundStatus()) || "18".equals(orderRefund.getRefundStatus())) {
                a +=1;
            } else {
                return "该商品已申请退款, 请勿重复申请";
            }
        }
        if (a > 2) {
            return "退款次数超过上限, 请联系客服处理";
        }
        if (finalProductVo.getNum() < num) {
            return "退货数量错误";
        }
        // ==================================验证完毕=======================================

        refunds.forEach(r->{
            if ( !"16".equals(r.getRefundStatus()) && !"18".equals(r.getRefundStatus())) {
                if (r.getRefundCoinNum() != null) {
                    orderDetailById.setCoinNum(orderDetailById.getCoinNum() - r.getRefundCoinNum());
                }
                if (r.getRefundCoinAmount() != null) {
                    orderDetailById.setCoinDiscount(orderDetailById.getCoinDiscount() - r.getRefundCoinAmount());
                }
                if (r.getRefundMoney() != null) {
                    orderDetailById.setRealPay(orderDetailById.getRealPay() - r.getRefundMoney());
                }
            }
        });
        String result = "";
        int price = finalProductVo.getPrice();
        Integer refundRealPay = orderDetailById.getRealPay();
        Integer returnMoney = price * num;
        OrderRefund refund = new OrderRefund();
        String id = System.currentTimeMillis() + "" + (int)((Math.random()*9+1)*1000000);
        refund.setId(id);
        refund.setOrderId(orderId);
        refund.setSizeDetailId(sizeDetailId);
        refund.setProductNum(num);
        refund.setType(refundType);
        refund.setPics(imgs);
        refund.setReason(reason);
        refund.setRefundStatus(OrderRefundEnum.REFUND_GOODS_PRE.getIndex() + "");
        refund.setShopId(orderDetailById.getBusinessId());
        refund.setUserId(orderDetailById.getUserId());
        refund.setDeleteFlag("0");
        refund.setCreateTime(new Date());
        refund.setRemark(remark);
        orderService.addRefund(refund);

        RefundMethod refundMethod = new RefundMethod();
        refundMethod.setId(UUID.randomUUID().toString());
        refundMethod.setRefundId(id);
        refundMethod.setAddress(address);
        refundMethod.setContent(content);
        refundMethod.setExpressCompany(expressName);
        refundMethod.setExpressNumber(expressNo);
        refundMethod.setTel(tel);
        refundMethod.setFetchTime(collectDate);
        refundMethod.setFreight(freight);
        refundMethod.setReturnTime(returnDate);
        refundMethod.setType(type);
        refundMethod.setCreateTime(new Date());
        refundMethod.setDeleteFlag("0");
        orderService.addRefundMethod(refundMethod);
        result = "退款申请已提交";
        return result;
    }

    private OrderRefund returnFreightAndCoins(
            OrderRefund orderRefund, OrderProduct orderProduct , Integer num) {
        return orderRefund;
    }

    /**
     * 调用统一下单接口，并组装生成支付所需参数对象.
     *
     * @param request 统一下单请求参数
     * @param <T>     请使用{@link com.github.binarywang.wxpay.bean.order}包下的类
     * @return 返回 {@link com.github.binarywang.wxpay.bean.order}包下的类对象
     */
   
    /**
     * 统一下单(详见https://pay.weixin.qq.com/wiki/doc/api/jsapi.php?chapter=9_1)
     * 在发起微信支付前，需要调用统一下单接口，获取"预支付交易会话标识"
     * 接口地址：https://api.mch.weixin.qq.com/pay/unifiedorder
     *
     * @param request 请求对象，注意一些参数如appid、mchid等不用设置，方法内会自动从配置对象中获取到（前提是对应配置中已经设置）
     */
   
    @PostMapping("/unifiedOrder")
    public WxPayUnifiedOrderResult unifiedOrder(@RequestBody WxPayUnifiedOrderRequest request) throws WxPayException {
        return this.wxService.unifiedOrder(request);
    }

//    @PostMapping("wxRefundResponse")
//    public String parseRefundNotifyResult(@RequestBody String xmlData) throws WxPayException {
//        final WxPayRefundNotifyResult result = this.wxService.parseRefundNotifyResult(xmlData);
//        String returnCode = result.getReturnCode();
//        if ("success".equalsIgnoreCase(returnCode)) {
//            WxPayRefundNotifyResult.ReqInfo reqInfo = result.getReqInfo();
//            String outRefundNo = reqInfo.getOutRefundNo();
//            String outTradeNo = reqInfo.getOutTradeNo();
//            Integer refundFee = reqInfo.getRefundFee();
//            if (dealRefund(outTradeNo, outRefundNo,refundFee)){
//                return WxPayNotifyResponse.success("成功");
//            }
//        }
//        return WxPayNotifyResponse.failResp("修改退款款失败");
//    }

    @PostMapping("wxRefundResponse")
    public JSONObject wxRefundResponse(@RequestBody String requestJsonStr, @RequestHeader HttpHeaders headers, HttpServletResponse response) {
        System.out.println("微信退款结果通知接受成功，header为：" + headers + "\n body为：" + requestJsonStr);
        //返回通知的应答报文，code(32)：SUCCESS为清算机构接收成功；message(64)：错误原因
        JSONObject responseJson = new JSONObject();
        responseJson.put("code", "FAIL");
        //支付通知http应答码为200或204才会当作正常接收，当回调处理异常时，应答的HTTP状态码应为500，或者4xx。
        response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);

        if (!headers.containsKey("Wechatpay-Serial") || !headers.containsKey("Wechatpay-Timestamp")
                || !headers.containsKey("Wechatpay-Nonce") || !headers.containsKey("Wechatpay-Signature")) {
            System.out.println("回调请求header缺失");
            responseJson.put("message", "回调请求header缺失");
            return responseJson;
        }

        String WechatpaySerial = headers.getFirst("Wechatpay-Serial");//平台证书序列号
        String WechatpayTimestamp = headers.getFirst("Wechatpay-Timestamp");//应答时间戳
        String WechatpayNonce = headers.getFirst("Wechatpay-Nonce");//应答随机串
        String WechatpaySignature = headers.getFirst("Wechatpay-Signature"); //应答签名
        if (!WechatpaySerial.equals(WxPayConfig.api_v3_cert_serial_no)) {
            System.out.println("回调请求证书序列化不一致");
            responseJson.put("message", "回调请求证书序列化不一致");
            return responseJson;
        }

        //获取签名串，验签
        String srcData = WechatpayTimestamp + "\n" + WechatpayNonce + "\n" + requestJsonStr + "\n";
        if (!WxPayUtils.verify(srcData, WechatpaySignature)) {
            System.out.println("验签失败");
            responseJson.put("message", "验签失败");
            return responseJson;
        }

        JSONObject requestJson = JSONObject.parseObject(requestJsonStr);
        //通知的类型，支付成功通知的类型为 TRANSACTION.SUCCESS
        String event_type = requestJson.get("event_type").toString();
        //通知数据
        JSONObject resource = JSONObject.parseObject(requestJson.get("resource").toString());
        String ciphertext = resource.get("ciphertext").toString();
        String associated_data = resource.get("associated_data").toString();
        String nonce = resource.get("nonce").toString();

        //对密文串进行解密
        String verify = WxPayUtils.getNotifyData(associated_data, nonce, ciphertext);
        JSONObject refundSuccess = JSONObject.parseObject(verify);
        String outRefundNo = refundSuccess.get("out_refund_no").toString();
        String outTradeNo = refundSuccess.get("out_trade_no").toString();
        String amount = refundSuccess.get("amount").toString();
        JSONObject amountObj = JSONObject.parseObject(amount);
        String refundFee = amountObj.get("payer_refund").toString();
        System.out.println("====准备处理订单退款回调===参数为: outRefundNo:" + outRefundNo);
        System.out.println("====准备处理订单退款回调===参数为: outTradeNo:" + outTradeNo);
        System.out.println("====准备处理订单退款回调===参数为: refundFee:" + refundFee);
        boolean b = dealRefund(outTradeNo, outRefundNo, Integer.parseInt(refundFee));
        if (b) {
            response.setStatus(HttpServletResponse.SC_OK);
            responseJson.put("code", "SUCCESS");
            responseJson.put("message", "微信支付成功，商户处理成功");
        }
        return responseJson;
    }

    private void dealOrder(String tradeNo, Integer payAmount, String payment, String otherOrderNo) {
        OrderDetailVo order = orderService.getOrderDetailById(tradeNo);
        if ("0".equals(order.getOrderStatus())) {
            String productType = order.getProductType();
            int status = 0;
            if ("1".equals(productType)) {
                status = 1;
            } else {
                status = 2;
            }
            // 修改订单状态
            int changeResult = orderService.changeOrderStatus(tradeNo,status);
            PayResponseVo vo = new PayResponseVo();
            vo.setOrderId(tradeNo);
            vo.setPayAmount(payAmount);
            vo.setPayment(payment);
            vo.setPayTime(new Date());
            vo.setOtherOrderId(otherOrderNo);
            // 修改订单详情
            int i = orderService.dealOrder(vo);
            if (i != 1 || changeResult == 0) {
                // TODO 降级操作
            }
            // 无论如何 从redis中删除此订单过期数据
            redisUtils.delete(RedisKeyEnum.ORDEREXPIREKEY.getMsg() + tradeNo);
        } else if ("8".equalsIgnoreCase(order.getOrderStatus())
                ||"9".equalsIgnoreCase(order.getOrderStatus())
                || "10".equalsIgnoreCase(order.getOrderStatus())) {
            // 退款
            if ("0".equals(payment)) {
                refundWxPay(tradeNo,payAmount );
            } else {
                refundAliPay(tradeNo,payment,payAmount );
            }
        }
    }

    private boolean refundWxPay(String orderId,Integer refundAmount) {
        WxPayRefundRequest request = new WxPayRefundRequest();
        request.setNotifyUrl("https://www.thehomepocket.com/cus/wxRefundResponse");
        request.setOutTradeNo(orderId);
        request.setOutRefundNo(orderId);
        request.setTotalFee(refundAmount);
        request.setRefundFee(refundAmount);
        try {
            WxPayRefundResult refund = this.wxService.refund(request);
            if ("SUCCESS".equals(refund.getReturnCode())) {
                return true;
            } else {
                // 申请失败
                refund = this.wxService.refund(request);
                if ("SUCCESS".equals(refund.getReturnCode())) {
                    return true;
                }
            }
        } catch (WxPayException e) {
            System.out.println("微信立即退款报错===" + orderId);
            e.printStackTrace();
            return false;
        }
        return false;
    }


    private boolean refundWxOrder(String orderId,String refundId, Integer refundAmount) {
        OrderDetailVo order = orderService.getOrderDetailById(orderId);
        Integer realPay = order.getRealPay();
        WxPayRefundRequest request = new WxPayRefundRequest();
        request.setNotifyUrl("https://www.thehomepocket.com/cus/wxRefundResponse");
        request.setOutTradeNo(orderId);
        request.setOutRefundNo(refundId);
        request.setTotalFee(realPay);
        request.setRefundFee(refundAmount);
        try {
            WxPayRefundResult refund = this.wxService.refund(request);
            if ("SUCCESS".equals(refund.getReturnCode())) {
                return true;
            } else {
                // 申请失败
                refund = this.wxService.refund(request);
                if ("SUCCESS".equals(refund.getReturnCode())) {
                    return true;
                }
            }
        } catch (WxPayException e) {
            System.out.println("微信退款报错===" + orderId);
            e.printStackTrace();
            return false;
        }
        return false;
    }

    @PostMapping("withdrawApply")
    public AjaxResult withdrawApply(@RequestParam String subMchId) {
        WxWithDrawVo vo = new WxWithDrawVo();
        vo.setSubMchId(subMchId);
        return AjaxResult.success(wechatService.withdrawApply(vo));
    }




    private boolean refundAliOrder(String orderId, String refundId, Integer refundAmount) {
//        System.out.println("==========进入退款方法=========");
//        OrderDetailVo order = orderService.getOrderDetailById(orderId);
//        String payment = order.getPayment();
//        String url = AlipayConfig.URL;
//        String charset = "UTF-8";
//        String appId = AlipayConfig.ANDROIDAPPID;
//        String appPrivateKey = AlipayConfig.RSA_PRIVATE_KEY;
//        String appPublicKey = AlipayConfig.ALIPAY_PUBLIC_KEY;
//        String sign_type = AlipayConfig.SIGNTYPE;
//        String app_cert_path = AlipayConfig.APP_CERT_PATH;
//        String alipay_cert_path = AlipayConfig.ALIPAY_CERT_PATH;
//        String alipay_root_cert_path = AlipayConfig.ALIPAY_ROOT_CERT_PATH;
//        //构造client
//        CertAlipayRequest certAlipayRequest = new CertAlipayRequest();
//        //设置网关地址
//        certAlipayRequest.setServerUrl("https://openapi.alipay.com/gateway.do");
//        //设置应用Id
//        certAlipayRequest.setAppId(appId);
//        //设置应用私钥
//        certAlipayRequest.setPrivateKey(appPrivateKey);
//        //设置请求格式，固定值json
//        certAlipayRequest.setFormat("json");
//        //设置字符集
//        certAlipayRequest.setCharset(charset);
//        //设置签名类型
//        certAlipayRequest.setSignType(sign_type);
//        //设置应用公钥证书路径
//        certAlipayRequest.setCertPath(app_cert_path);
//        //设置支付宝公钥证书路径
//        certAlipayRequest.setAlipayPublicCertPath(alipay_cert_path);
//        //设置支付宝根证书路径
//        certAlipayRequest.setRootCertPath(alipay_root_cert_path);
//        AlipayClient alipayClient = null;
//        AlipayTradeRefundModel model = new AlipayTradeRefundModel ();
//        model.setOutTradeNo(orderId);
//        model.setOutRequestNo(refundId);
//        double v = Double.parseDouble(refundAmount.toString()) / 100;
//        model.setRefundAmount(Double.toString(v));
//        model.setRefundReason("商品退款");
//        AlipayTradeRefundRequest request = new AlipayTradeRefundRequest();
//        request.setBizModel(model);
//        try{
//            alipayClient = new DefaultAlipayClient(certAlipayRequest);
//            if ("1".equals(payment)) {
//                // 安卓
//                AlipayTradeRefundResponse response = alipayClient.certificateExecute(request);
//                if ("Success".equals(response.getMsg()) || "10000".equals(response.getCode())) {
//                    System.out.println("========支付宝退款安卓初步成功========");
//                    return true;
//                }
//                System.out.println("========支付宝退款安卓失败========");
//            }
//            if ("2".equals(payment)) {
//                // 苹果
//                appId = AlipayConfig.IOSAPPID;
//                appPrivateKey = AlipayConfig.IOS_PRIVATE_KEY;
//                alipay_cert_path = AlipayConfig.IOS_ALIPAY_CERT_PATH;
//                app_cert_path = AlipayConfig.IOS_APP_CERT_PATH;
//                alipay_root_cert_path = AlipayConfig.IOS_ALIPAY_ROOT_CERT_PATH;
//                alipayClient = new DefaultAlipayClient(certAlipayRequest);
//                //设置网关地址
//                certAlipayRequest.setServerUrl("https://openapi.alipay.com/gateway.do");
//                //设置应用Id
//                certAlipayRequest.setAppId(appId);
//                //设置应用私钥
//                certAlipayRequest.setPrivateKey(appPrivateKey);
//                //设置请求格式，固定值json
//                certAlipayRequest.setFormat("json");
//                //设置字符集
//                certAlipayRequest.setCharset(charset);
//                //设置签名类型
//                certAlipayRequest.setSignType(sign_type);
//                //设置应用公钥证书路径
//                certAlipayRequest.setCertPath(app_cert_path);
//                //设置支付宝公钥证书路径
//                certAlipayRequest.setAlipayPublicCertPath(alipay_cert_path);
//                //设置支付宝根证书路径
//                certAlipayRequest.setRootCertPath(alipay_root_cert_path);
//                AlipayTradeRefundResponse response = alipayClient.certificateExecute(request);
//                if ("Success".equals(response.getMsg()) || "10000".equals(response.getCode())) {
//                    System.out.println("========支付宝退款苹果初步成功========");
//                    return true;
//                }
//                System.out.println("========支付宝退款苹果失败========");
//            }
//            System.out.println("=========退款失败==========");
//            System.out.println("==========退款失败=========订单信息为" + payment);
//        }catch(Exception e){
//            e.printStackTrace();
//            System.out.println("=========退款失败(报错)==========");
//        }
        return false;
    }

    private boolean refundAliPay(String orderId, String payment, Integer refundAmount) {
//        System.out.println("==========进入退款方法=========");
//        OrderDetailVo order = orderService.getOrderDetailById(orderId);
//        String url = AlipayConfig.URL;
//        String charset = "UTF-8";
//        String appId = AlipayConfig.ANDROIDAPPID;
//        String appPrivateKey = AlipayConfig.RSA_PRIVATE_KEY;
//        String appPublicKey = AlipayConfig.ALIPAY_PUBLIC_KEY;
//        String sign_type = AlipayConfig.SIGNTYPE;
//        String app_cert_path = AlipayConfig.APP_CERT_PATH;
//        String alipay_cert_path = AlipayConfig.ALIPAY_CERT_PATH;
//        String alipay_root_cert_path = AlipayConfig.ALIPAY_ROOT_CERT_PATH;
//        //构造client
//        CertAlipayRequest certAlipayRequest = new CertAlipayRequest();
//        //设置网关地址
//        certAlipayRequest.setServerUrl("https://openapi.alipay.com/gateway.do");
//        //设置应用Id
//        certAlipayRequest.setAppId(appId);
//        //设置应用私钥
//        certAlipayRequest.setPrivateKey(appPrivateKey);
//        //设置请求格式，固定值json
//        certAlipayRequest.setFormat("json");
//        //设置字符集
//        certAlipayRequest.setCharset(charset);
//        //设置签名类型
//        certAlipayRequest.setSignType(sign_type);
//        //设置应用公钥证书路径
//        certAlipayRequest.setCertPath(app_cert_path);
//        //设置支付宝公钥证书路径
//        certAlipayRequest.setAlipayPublicCertPath(alipay_cert_path);
//        //设置支付宝根证书路径
//        certAlipayRequest.setRootCertPath(alipay_root_cert_path);
//        AlipayClient alipayClient = null;
//        AlipayTradeRefundModel model = new AlipayTradeRefundModel ();
//        model.setOutTradeNo(orderId);
//        model.setOutRequestNo(orderId);
//        double v = Double.parseDouble(refundAmount.toString()) / 100;
//        model.setRefundAmount(Double.toString(v));
//        model.setRefundReason("商品退款");
//        AlipayTradeRefundRequest request = new AlipayTradeRefundRequest();
//        request.setBizModel(model);
//        try{
//            alipayClient = new DefaultAlipayClient(certAlipayRequest);
//            if ("1".equals(payment)) {
//                // 安卓
//                AlipayTradeRefundResponse response = alipayClient.certificateExecute(request);
//                if ("Success".equals(response.getMsg()) || "10000".equals(response.getCode())) {
//                    System.out.println("========支付宝退款安卓初步成功========");
//                    return true;
//                }
//                System.out.println("========支付宝退款安卓失败========");
//            }
//            if ("2".equals(payment)) {
//                // 苹果
//                appId = AlipayConfig.IOSAPPID;
//                appPrivateKey = AlipayConfig.IOS_PRIVATE_KEY;
//                alipay_cert_path = AlipayConfig.IOS_ALIPAY_CERT_PATH;
//                app_cert_path = AlipayConfig.IOS_APP_CERT_PATH;
//                alipay_root_cert_path = AlipayConfig.IOS_ALIPAY_ROOT_CERT_PATH;
//                alipayClient = new DefaultAlipayClient(certAlipayRequest);
//                //设置网关地址
//                certAlipayRequest.setServerUrl("https://openapi.alipay.com/gateway.do");
//                //设置应用Id
//                certAlipayRequest.setAppId(appId);
//                //设置应用私钥
//                certAlipayRequest.setPrivateKey(appPrivateKey);
//                //设置请求格式，固定值json
//                certAlipayRequest.setFormat("json");
//                //设置字符集
//                certAlipayRequest.setCharset(charset);
//                //设置签名类型
//                certAlipayRequest.setSignType(sign_type);
//                //设置应用公钥证书路径
//                certAlipayRequest.setCertPath(app_cert_path);
//                //设置支付宝公钥证书路径
//                certAlipayRequest.setAlipayPublicCertPath(alipay_cert_path);
//                //设置支付宝根证书路径
//                certAlipayRequest.setRootCertPath(alipay_root_cert_path);
//                AlipayTradeRefundResponse response = alipayClient.certificateExecute(request);
//                if ("Success".equals(response.getMsg()) || "10000".equals(response.getCode())) {
//                    System.out.println("========支付宝退款苹果初步成功========");
//                    return true;
//                }
//                System.out.println("========支付宝退款苹果失败========");
//            }
//            System.out.println("==========支付宝立即退款失败=========订单信息为" + payment);
//        }catch(Exception e){
//            e.printStackTrace();
//            System.out.println("=========支付宝立即退款失败(报错)==========");
//        }
        return false;
    }

}
