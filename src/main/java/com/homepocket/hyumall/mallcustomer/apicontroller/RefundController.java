package com.homepocket.hyumall.mallcustomer.apicontroller;

import com.homepocket.hyumall.common.annotation.CheckToken;
import com.homepocket.hyumall.common.annotation.TokenRequired;
import com.homepocket.hyumall.common.annotation.TokenUser;
import com.homepocket.hyumall.mallcustomer.apiservice.RefundService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Liang
 * @date 2020/11/13 16:16
 **/
@RestController
public class RefundController {

    @Autowired
    private RefundService refundService;

    @TokenRequired
    @GetMapping("findOrderRefund")
    public Object findOrderRefund(Integer pageSize, Integer pageNum, String status,@TokenUser String userId){
        return refundService.findOrderRefund(pageSize,pageNum,status,userId);
    }

    @GetMapping("getOrderRefundById")
    public Object getOrderRefundById(String refundId){
        return refundService.getOrderRefundById(refundId);
    }

}
