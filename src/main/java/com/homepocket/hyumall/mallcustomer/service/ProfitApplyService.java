package com.homepocket.hyumall.mallcustomer.service;

import com.homepocket.hyumall.common.domain.AjaxResult;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @Author Liang
 * @Date 2020/11/25 9:27
 * @Description
 **/
public interface ProfitApplyService {
    boolean profitSharing(String orderId);
}
