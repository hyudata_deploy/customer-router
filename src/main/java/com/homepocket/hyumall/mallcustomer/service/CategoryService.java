package com.homepocket.hyumall.mallcustomer.service;

import com.homepocket.hyumall.common.vo.CategoryVo;

import java.util.List;

/**
 * @Author Liang
 * @Date 2020/6/15 16:30
 * @Description
 **/
public interface CategoryService{
    List<CategoryVo> selectCategoryList();
    List<CategoryVo> selectCategoryList(String parentId);
}
