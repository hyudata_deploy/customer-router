package com.homepocket.hyumall.mallcustomer.service.impl;

import com.homepocket.hyumall.common.domain.Order;
import com.homepocket.hyumall.common.domain.OrderRefund;
import com.homepocket.hyumall.common.domain.ShopWater;
import com.homepocket.hyumall.common.enums.OrderRefundEnum;
import com.homepocket.hyumall.common.wxpay.WxProfitVo;
import com.homepocket.hyumall.mallcustomer.apiservice.OrderService;
import com.homepocket.hyumall.mallcustomer.apiservice.WechatService;
import com.homepocket.hyumall.mallcustomer.config.WeChatPayProperties;
import com.homepocket.hyumall.mallcustomer.service.ProfitApplyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * @author Liang
 * @date 2020/11/25 9:27
 **/
@Service
public class ProfitApplyServiceImpl implements ProfitApplyService {

    @Autowired
    private OrderService orderService;

    @Autowired
    private WechatService wechatService;

    @Autowired
    WeChatPayProperties weChatPayProperties;

    @Override
    public boolean profitSharing(String orderId) {
        Order orderEntity = orderService.getOrderEntity(orderId);
        if (orderEntity == null) {
            return false;
        }
        List<OrderRefund> refunds = orderService.getRefundsByOrderId(orderId);
        int reduceAmount = 0;
        String id = UUID.randomUUID().toString().replaceAll("-", "");
        ShopWater shopWater = new ShopWater();
        shopWater.setId(id);
        shopWater.setOrderId(orderId);
        shopWater.setUserId(orderEntity.getUserId());
        shopWater.setShopId(orderEntity.getShopId());
        shopWater.setIsPay("0");
        shopWater.setStatus("0");
        shopWater.setPayment("1".equalsIgnoreCase(orderEntity.getPayment())? "1" : "0");
        shopWater.setType("0");
        shopWater.setIfRefund("0");
        Integer amount = orderEntity.getRealPay();
        for (OrderRefund refund : refunds) {
            if (
                    refund.getRefundStatus().equals(OrderRefundEnum.REFUND_GOODS_SUCCESS.getIndex() + "")
                            ||  refund.getRefundStatus().equals(OrderRefundEnum.REFUND_PRE.getIndex() + "")
                            ||  refund.getRefundStatus().equals(OrderRefundEnum.REFUND_ING.getIndex() + "")
            ) {
                shopWater.setIsPay("1");
                amount = orderEntity.getRealPay();
                shopWater.setAmount(amount);
                shopWater.setDeductMoney(calculateMoney(amount));
                orderService.addShopWater(shopWater);
                // todo 等待退款结束 分账
                return false;
            } else if (
                    refund.getRefundStatus().equals(OrderRefundEnum.REFUND_SUCCESS.getIndex() + "")
            ) {
                // 退款成功 分账金额相应减少
                reduceAmount += refund.getRefundMoney();
                amount = orderEntity.getRealPay() - reduceAmount;
                shopWater.setIfRefund("1");
            }
        }
        shopWater.setAmount(amount);
        shopWater.setCreateTime(new Date());
        shopWater.setDeleteFlag("0");
        shopWater.setDeductMoney(calculateMoney(amount));
        orderService.addShopWater(shopWater);
        WxProfitVo profitVo = new WxProfitVo();
        profitVo.setOutOrderNo(id);
        profitVo.setPlatformAmount(calculateMoney(amount));
        profitVo.setSubMchId(orderEntity.getWxMchId());
        String wxOrderId = orderEntity.getWxOrderId();
        String aliOrderId = orderEntity.getAliOrderId();
        String payment = orderEntity.getPayment();
        if ("0".equals(payment)) {
            profitVo.setType("0");
            profitVo.setWxOrderId(wxOrderId);
            wechatService.profitSharing(profitVo);
        } else if ("1".equals(payment)) {

        } else {
            profitVo.setType("1");
            profitVo.setWxOrderId(wxOrderId);
            wechatService.profitSharing(profitVo);
        }
        return true;
    }

    /**
     * 计算平台佣金
     * @return
     */
    private Integer calculateMoney(Integer totalAmount) {
        if (totalAmount >= 10) {
            return new BigDecimal(totalAmount)
                    .multiply(new BigDecimal("3"))
                    .divide(new BigDecimal(100), 0, BigDecimal.ROUND_UP).intValue();
        }
        return 0;
    }

}
