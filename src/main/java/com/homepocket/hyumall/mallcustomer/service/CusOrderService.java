package com.homepocket.hyumall.mallcustomer.service;

import com.homepocket.hyumall.common.vo.order.CalculateTotalVo;

/**
 * @author Liang
 * @date 2020/7/20 13:50
 **/
public interface CusOrderService {

    CalculateTotalVo calculatePrice(
            String userId, String addressId, String orderJsonData,
            Integer couponFlag, String couponId, Integer coinFlag);
}
