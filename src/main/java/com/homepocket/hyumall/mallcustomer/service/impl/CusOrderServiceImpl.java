package com.homepocket.hyumall.mallcustomer.service.impl;

import com.alibaba.fastjson.JSON;
import com.homepocket.hyumall.common.domain.ProductFreight;
import com.homepocket.hyumall.common.utils.StringUtils;
import com.homepocket.hyumall.common.vo.order.*;
import com.homepocket.hyumall.common.vo.product.ProductDetailVo;
import com.homepocket.hyumall.common.vo.product.SizePriceVo;
import com.homepocket.hyumall.common.vo.shop.ShopDetailVo;
import com.homepocket.hyumall.common.vo.user.UserAddrVo;
import com.homepocket.hyumall.mallcustomer.apiservice.BusinessService;
import com.homepocket.hyumall.mallcustomer.apiservice.ProductService;
import com.homepocket.hyumall.mallcustomer.apiservice.UserService;
import com.homepocket.hyumall.mallcustomer.domain.UserEntity;
import com.homepocket.hyumall.mallcustomer.enums.CalculateErrorEnum;
import com.homepocket.hyumall.mallcustomer.service.CusOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

/**
 * @author Liang
 * @date 2020/7/20 13:50
 **/
@Service
public class CusOrderServiceImpl implements CusOrderService {

    @Autowired
    private UserService userService;

    @Autowired
    private ProductService productService;

    @Autowired
    private BusinessService businessService;

    @Override
    public CalculateTotalVo calculatePrice(
            String userId, String addressId, String orderJsonData,
                                      Integer couponFlag, String couponId, Integer coinFlag) {
        CalculateTotalVo returnVo = new CalculateTotalVo();
        CalculateVo vo = new CalculateVo();
        CalVo calVo = new CalVo();
        List<OrderProductVo> list1 = JSON.parseArray(orderJsonData, OrderProductVo.class);
        // 先把数据按照店铺ID排序
        list1.sort(Comparator.comparing(OrderProductVo::getShopId));
        vo.setErrorCode("0");
        calVo.setErrorCode("0");
        boolean groupFlag = false;
        // 获取用户地址列表
        List<UserAddrVo> list = userService.findUserAddrAll(userId);
        Integer totalPrice = 0;
        Integer totalNum = 0;
        Integer totalDiscount = 0;
        Integer totalFreight = 0;
        Integer totalCouponDiscount = 0;
        Integer coinNum = 0;
        Integer coinDiscount = 0;
        Integer groupTotal = 0;
        if (list.size() > 0) {
            // 找到选中地址
            Optional<UserAddrVo> first = list.stream().filter(r -> r.getId().equals(addressId)).findFirst();
            if (first.isPresent()) {
                // 地址存在
                UserAddrVo userAddrVo = first.get();
                String province = userAddrVo.getProvince();
                String city = userAddrVo.getCity();
                String block = userAddrVo.getArea();
                // 当前店铺ID
                String shopId = "";
                // 当前店铺总金额
                Integer shopTotal = 0;
                // 当前店铺总优惠
                Integer shopDiscount = 0;
                // 当前店铺总运费
                Integer shopFreight = 0;
                Integer shopCoinNum = 0;
                Integer shopCoinDiscount = 0;
                String shopRemark = "";
                List<CalculateShopVo> shopVoList = new ArrayList<>();
                List<CalculateProductVo> productVos = new ArrayList<>();
                List<CalShopVo> shopList = new ArrayList<>();
                List<CalProductVo> products = new ArrayList<>();
                int a = 0;
                for (OrderProductVo orderProductVo : list1) {
                    CalculateShopVo calculateShopVo = new CalculateShopVo();
                    CalShopVo calSHopVo = new CalShopVo();
                    CalculateProductVo productVo = new CalculateProductVo();
                    CalProductVo calProductVo = new CalProductVo();
                    shopRemark = orderProductVo.getRemark();
                    totalNum += orderProductVo.getNum();
                    // 如果当前店铺ID和前一个不同 则为下一个店铺的商品
                    if (!orderProductVo.getShopId().equals(shopId)) {
                        // 为了使用java8 定义常量.
                        String u = shopId;
                        if (StringUtils.isNotEmpty(u)) {
                            Optional<CalculateShopVo> first1 = shopVoList.stream()
                                    .filter(
                                        r -> r.getBusinessId().equals(u)
                                    ).findFirst();
                            // 如果有上一个店铺 结算上个店铺的总金额
                            if (first1.isPresent()) {
                                calculateShopVo = first1.get();
                                shopVoList.remove(calculateShopVo);
                                calculateShopVo.setFreight(shopFreight);
                                calculateShopVo.setProductTotalPrice(shopTotal);
                                calculateShopVo.setShopDiscount(shopDiscount);
                                calculateShopVo.setProductVos(productVos);
                                calculateShopVo.setRemark(shopRemark);
                                int shouldpay = coinFlag == 0? shopTotal + shopFreight - shopDiscount :
                                        shopTotal + shopFreight - shopDiscount - totalCouponDiscount - shopCoinDiscount;
                                calculateShopVo.setShopShouldPay(shouldpay);
                                shopVoList.add(calculateShopVo);
                            }
                        }
                        if (StringUtils.isNotEmpty(u)) {
                            Optional<CalShopVo> first1 = shopList.stream()
                                    .filter(
                                            r -> r.getBusinessId().equals(u)
                                    ).findFirst();
                            // 如果有上一个店铺 结算上个店铺的总金额
                            if (first1.isPresent()) {
                                calSHopVo = first1.get();
                                shopList.remove(calSHopVo);
                                calSHopVo.setFreight(shopFreight);
                                calSHopVo.setProductTotalPrice(shopTotal);
                                calSHopVo.setShopDiscount(shopDiscount);
                                calSHopVo.setProductVos(products);
                                calSHopVo.setRemark(shopRemark);
                                shopList.add(calSHopVo);
                            }
                        }
                        // 重置店铺信息
                        shopTotal = 0;
                        shopDiscount = 0;
                        shopFreight = 0;
                        products = new ArrayList<>();
                        productVos = new ArrayList<>();
                        shopId = orderProductVo.getShopId();
                        calculateShopVo = new CalculateShopVo();
                        calSHopVo = new CalShopVo();
                        List<ShopDetailVo> businessById = businessService.getBusinessById(userId, shopId);
                        ShopDetailVo business = new ShopDetailVo();
                        if (businessById != null && businessById.size() > 0) {
                            business = businessById.get(0);
                        }
                        calculateShopVo.setBusinessId(shopId);
                        calculateShopVo.setBusinessName(business.getBusinessName());
                        calculateShopVo.setCenterName(business.getCenterName());
                        calculateShopVo.setAliMchId(business.getAliMchId());
                        calculateShopVo.setWxMchId(business.getWxMchId());
                        shopVoList.add(calculateShopVo);
                        calSHopVo.setBusinessId(shopId);
                        calSHopVo.setBusinessName(business.getBusinessName());
                        calSHopVo.setCenterName(business.getCenterName());
                        shopList.add(calSHopVo);
                    }
                    // ===================计算当前商品价格开始==============================
                    int boughtNum = orderProductVo.getNum();
                    String productId = orderProductVo.getProductId();
                    String sizeDetailId = orderProductVo.getSizeDetailId();
                    SizePriceVo size = productService.getSizeById(sizeDetailId);
                    if (Integer.parseInt(size.getStock()) == 0) {
                        vo.setErrorCode(CalculateErrorEnum.NumberError.getCode().toString());
                        vo.setErrorMsg(CalculateErrorEnum.NumberError.getMessage());
                        calVo.setErrorCode(CalculateErrorEnum.NumberError.getCode().toString());
                        calVo.setErrorMsg(CalculateErrorEnum.NumberError.getMessage());
                    }

                    ProductDetailVo product = productService.getProductById(productId);
                    String type = product.getType();
                    productVo.setProductId(productId);
                    productVo.setProductName(product.getProductName());
                    productVo.setProductOriPrice(Integer.parseInt(size.getOriSizePrice()));
                    productVo.setProductPic(size.getSizePic());
                    productVo.setSizePriceVo(size);
                    productVo.setProductType(type);
                    productVo.setProductDetailVo(product);
                    if ("1".equals(product.getIfSale())) {
                        productVo.setSendFlag(0);
                    } else {
                        productVo.setSendFlag(3);
                    }
                    if ("1".equals(product.getIfSale())) {
                        calProductVo.setSendFlag(0);
                    } else {
                        calProductVo.setSendFlag(3);
                    }
                    calProductVo.setProductId(productId);
                    calProductVo.setProductName(product.getProductName());
                    calProductVo.setProductOriPrice(Integer.parseInt(size.getOriSizePrice()));
                    calProductVo.setProductPic(size.getSizePic());
                    calProductVo.setProductType(type);
                    List<ProductFreight> freightByModel
                            = productService.getFreightByModel(productId, province, city, block, null);
                    if (freightByModel != null && freightByModel.size() > 0) {
                        ProductFreight productFreight = freightByModel.get(0);
                        String price = productFreight.getPrice();
                        Integer freight = Integer.parseInt(price);
                        shopFreight += freight;
                        totalFreight += freight;
                    } else {
                        productVo.setSendFlag(1);
                        calProductVo.setSendFlag(1);
                        vo.setErrorCode(CalculateErrorEnum.AddressSupportError.getCode().toString());
                        vo.setErrorMsg(CalculateErrorEnum.AddressSupportError.getMessage());
                        calVo.setErrorCode(CalculateErrorEnum.AddressSupportError.getCode().toString());
                        calVo.setErrorMsg(CalculateErrorEnum.AddressSupportError.getMessage());
                    }
                    if ("0".equals(type)) {
                        // 普通商品
                        Integer discountPrice = Integer.parseInt(size.getSizePrice());
                        int totalProduct = discountPrice * boughtNum;
                        productVo.setProductPrice(discountPrice);
                        productVo.setProductTotalPrice(totalProduct);
                        calProductVo.setProductPrice(discountPrice);
                        calProductVo.setProductTotalPrice(totalProduct);
                        totalPrice += totalProduct;
                        shopTotal += totalProduct;

                    } else if ("1".equals(type)) {
                        productVo.setSendFlag(2);
                        calProductVo.setSendFlag(2);
                        Integer discountPrice = size.getDiscount2();
                        int totalProduct = discountPrice * boughtNum;
                        productVo.setProductPrice(discountPrice);
                        productVo.setProductTotalPrice(totalProduct);
                        groupTotal = size.getDiscount10() * boughtNum;
                        calProductVo.setProductPrice(discountPrice);
                        calProductVo.setProductTotalPrice(totalProduct);
                        totalPrice += totalProduct;
                        shopTotal += totalProduct;
                        groupFlag = true;
                    } else if ("2".equals(type)) {
                        Integer discountPrice = size.getDiscount();
                        int totalProduct = discountPrice * boughtNum;
                        productVo.setProductPrice(discountPrice);
                        productVo.setProductTotalPrice(totalProduct);
                        calProductVo.setProductPrice(discountPrice);
                        calProductVo.setProductTotalPrice(totalProduct);
                        totalPrice += totalProduct;
                        shopTotal += totalProduct;
                    }
                    productVo.setNum(boughtNum);
                    productVo.setSizeName(size.getSizeName());
                    productVo.setSizeDetailId(sizeDetailId);
                    productVo.setProductPic(size.getSizePic());
                    productVo.setStock(Integer.parseInt(size.getStock()));
                    productVos.add(productVo);

                    calProductVo.setNum(boughtNum);
                    calProductVo.setSizeName(size.getSizeName());
                    calProductVo.setSizeDetailId(sizeDetailId);
                    calProductVo.setProductPic(size.getSizePic());
                    calProductVo.setStock(Integer.parseInt(size.getStock()));
                    products.add(calProductVo);
                    a ++;
                    if (a == list1.size()) {
                        // 最后一个商品 统计当前店铺总价
                        String u = shopId;
                        if (StringUtils.isNotEmpty(u)) {
                            Optional<CalculateShopVo> first1 = shopVoList.stream()
                                    .filter(
                                            r -> r.getBusinessId().equals(u)
                                    ).findFirst();
                            // 如果有上一个店铺 结算上个店铺的总金额
                            if (first1.isPresent()) {
                                calculateShopVo = first1.get();
                                shopVoList.remove(calculateShopVo);
                                calculateShopVo.setFreight(shopFreight);
                                calculateShopVo.setProductTotalPrice(shopTotal);
                                calculateShopVo.setShopDiscount(shopDiscount);
                                calculateShopVo.setProductVos(productVos);
                                calculateShopVo.setRemark(shopRemark);
                                int shouldpay = coinFlag == 0? shopTotal + shopFreight - shopDiscount :
                                        shopTotal + shopFreight - shopDiscount - totalCouponDiscount - shopCoinDiscount;
                                calculateShopVo.setShopShouldPay(shouldpay);
                                shopVoList.add(calculateShopVo);
                            }
                        }

                        if (StringUtils.isNotEmpty(u)) {
                            Optional<CalShopVo> first1 = shopList.stream()
                                    .filter(
                                            r -> r.getBusinessId().equals(u)
                                    ).findFirst();
                            // 如果有上一个店铺 结算上个店铺的总金额
                            if (first1.isPresent()) {
                                calSHopVo = first1.get();
                                shopList.remove(calSHopVo);
                                calSHopVo.setFreight(shopFreight);
                                calSHopVo.setProductTotalPrice(shopTotal);
                                calSHopVo.setShopDiscount(shopDiscount);
                                calSHopVo.setProductVos(products);
                                calSHopVo.setRemark(shopRemark);
                                shopList.add(calSHopVo);
                            }
                        }
                    }
                }

                vo.setShopVos(shopVoList);
                calVo.setShopVos(shopList);

                UserEntity userById = userService.getUserById(userId);
                // 获取用户口袋币余额
                Integer coins = userById.getCoins();
                coins = coins == null? 0: coins;
                String coinValue = userService.getParamsByKey("coin_value");
                String coinLimit = userService.getParamsByKey("coin_limit");
                // 订单除口袋币其它金额
                int orderTotal =
                        groupTotal != 0
                        ? (groupTotal + totalFreight - totalCouponDiscount - totalDiscount)
                        :(totalPrice + totalFreight - totalCouponDiscount - totalDiscount);
                // 可用于抵扣的口袋币
                if (coinValue != null && coinLimit != null && !"0".equals(coinValue) && !"0".equals(coinLimit) ) {
                    // 用户余额
                    int existValue = (int) (coins * Double.parseDouble(coinValue));
                    List<CalculateShopVo> costCoinVos = new ArrayList<>();
                    for (CalculateShopVo calculateShopVo : shopVoList) {
                        // 本店铺最多抵扣金额
                        int maxValue = 0;
                        Integer shopShouldPay = calculateShopVo.getShopShouldPay();
                        maxValue = (int)(shopShouldPay * Double.parseDouble(coinLimit));
                        if (groupFlag) {
                            maxValue = (int)(orderTotal * Double.parseDouble(coinLimit));
                        }
                        int ssp = 0 == coinFlag ? shopShouldPay : (shopShouldPay - maxValue);
                        if (existValue >= maxValue) {
                            int costNum = (int) (maxValue / Double.parseDouble(coinValue));
                            calculateShopVo.setShopCoinDiscount(0==coinFlag? 0:maxValue);
                            calculateShopVo.setShopCoinNum(0==coinFlag? 0:costNum);
                            calculateShopVo.setShopShouldPay(ssp);
                            coinDiscount += maxValue;
                            coinNum += costNum;
                            existValue -= maxValue;
                        } else {
                            int costNum = (int) (existValue / Double.parseDouble(coinValue));
                            calculateShopVo.setShopCoinDiscount(0==coinFlag? 0:existValue);
                            calculateShopVo.setShopCoinNum(0==coinFlag? 0:costNum);
                            calculateShopVo.setShopShouldPay(ssp);
                            coinDiscount += existValue;
                            coinNum += costNum;
                            existValue = 0;
                        }
                        costCoinVos.add(calculateShopVo);
                    }
                    vo.setShopVos(costCoinVos);
                }

                int shouldpay = coinFlag == 0? totalPrice + totalFreight - totalDiscount - totalCouponDiscount :
                        totalPrice + totalFreight - totalDiscount - totalCouponDiscount - coinDiscount;
                boolean flag = coinFlag == 0;
                vo.setTotalPrice(totalPrice);
                vo.setFreight(totalFreight);
                vo.setCouponPrice(totalCouponDiscount);
                vo.setShouldPay(shouldpay);
                vo.setTotalNum(totalNum);
                vo.setCoinFlag(flag);
                vo.setAddress(userAddrVo);
                vo.setCoinNumber((int)(coinDiscount / Double.parseDouble(coinValue)));
                vo.setCoinAmount(coinDiscount);

                calVo.setAddress(userAddrVo);
                calVo.setTotalNum(totalNum);
                calVo.setCoinFlag(flag);
                calVo.setTotalPrice(totalPrice);
                calVo.setFreight(totalFreight);
                calVo.setCouponPrice(totalCouponDiscount);
                calVo.setShouldPay(shouldpay);
                calVo.setCoinNumber((int)(coinDiscount / Double.parseDouble(coinValue)));
                calVo.setCoinAmount(coinDiscount);
            } else {
                vo.setErrorCode(CalculateErrorEnum.AddressSelectError.getCode().toString());
                vo.setErrorMsg(CalculateErrorEnum.AddressSelectError.getMessage());

                calVo.setErrorCode(CalculateErrorEnum.AddressSelectError.getCode().toString());
                calVo.setErrorMsg(CalculateErrorEnum.AddressSelectError.getMessage());
            }
        } else {
            vo.setErrorCode(CalculateErrorEnum.AddressNuLLError.getCode().toString());
            vo.setErrorMsg(CalculateErrorEnum.AddressNuLLError.getMessage());

            calVo.setErrorCode(CalculateErrorEnum.AddressNuLLError.getCode().toString());
            calVo.setErrorMsg(CalculateErrorEnum.AddressNuLLError.getMessage());
        }
        returnVo.setCalculateVo(vo);
        returnVo.setCalVo(calVo);
        return returnVo;


//        // =======================================运费相关开始======================================================
//        // 获取当前收货地址 查运费/能否配送
//        String addrDetail = "";
//
//
//        // =======================================运费相关结束======================================================
//        // =======================================商品相关开始======================================================
//        ProductDetailVo product = productService.getProductById(productId);
//        List<ShopDetailVo> businessById = businessService.getBusinessById(userId, product.getBusinessId());
//        ShopDetailVo business = new ShopDetailVo();
//        if (businessById != null && businessById.size() > 0) {
//            business = businessById.get(0);
//        }
//
//        String type = product.getType();
//        if (type == null) {
//            type = "0";
//        }
//        vo.setOrderType(type);
//        switch (type){
//            // 原价无活动
//            case "0":
//                productPrice = Integer.parseInt(size.getOriSizePrice());
//                productDiscount = Integer.parseInt(size.getSizePrice());
//                break;
//            // 团购
//            case "1":
//                productPrice = Integer.parseInt(size.getOriSizePrice());
//                productDiscount = size.getDiscount2();
//                count10 = size.getDiscount10();
//                break;
//            //秒杀
//            case "2":
//                productPrice = Integer.parseInt(size.getOriSizePrice());
//                productDiscount = size.getDiscount();
//                break;
//            default:
//                vo.setErrorCode(CalculateErrorEnum.OnsaleError.getCode().toString());
//                vo.setErrorMsg(CalculateErrorEnum.OnsaleError.getMessage());
//        }
//        discountTotal = (productPrice - productDiscount) * boughtNum;
//        if (Integer.parseInt(size.getStock()) >= boughtNum) {
//            productTotalPrice = boughtNum * productPrice;
//            productDiscountPrice = boughtNum * productDiscount;
//            if ("1".equalsIgnoreCase(type)) {
//                count10 = boughtNum * count10;
//            }
//        } else {
//            vo.setErrorCode(CalculateErrorEnum.NumberError.getCode().toString());
//            vo.setErrorMsg(CalculateErrorEnum.NumberError.getMessage());
//        }
//        // =======================================商品相关结束======================================================
//        // =======================================优惠券暂时不做====================================================
//        couponDiscount = 0;
//        // =======================================口袋币相关开始====================================================
//        totalPrice = productDiscountPrice + freight - couponDiscount;
//        UserEntity userById = userService.getUserById(userId);
//        // 获取用户口袋币余额
//        Integer coins = userById.getCoins();
//        coins = coins == null? 0: coins;
//        // 获取口袋币抵扣多少钱 以及口袋币抵扣上限
//        String coinValue = userService.getParamsByKey("coin_value");
//        String coinLimit = userService.getParamsByKey("coin_limit");
//        if (coinValue != null && coinLimit != null && !"0".equals(coinValue) && !"0".equals(coinLimit)) {
//            int maxDiscount = (int) (totalPrice * Double.parseDouble(coinLimit));
//            if ("1".equalsIgnoreCase(type)) {
//                count10 = count10 + freight - couponDiscount;
//                maxDiscount =(int) (count10 * Double.parseDouble(coinLimit)) ;
//            }
//            int existValue = (int) (coins * Double.parseDouble(coinValue));
//            if (existValue <= maxDiscount) {
//                coinDiscount = existValue;
//                costCoin = coins;
//            } else {
//                coinDiscount = maxDiscount;
//                costCoin = (int) (maxDiscount / Double.parseDouble(coinValue));
//            }
//        }
//        totalPrice =
//                coinFlag == 1
//                ? totalPrice - coinDiscount
//                : totalPrice;
//        // =======================================口袋币相关结束====================================================
//        // =======================================封装VO开始========================================================
//        vo.setDiscountAmount(discountTotal);
//        vo.setCoinFlag(coinFlag==1);
//        vo.setFreight(freight);
//        vo.setProductOriPrice(Integer.parseInt(size.getOriSizePrice()));
//        vo.setCouponId(couponId);
//        vo.setCoinAmount(coinDiscount);
//        vo.setCoinNumber(costCoin);
//        vo.setTotalPrice(totalPrice);
//        vo.setShouldPay(totalPrice);
//        vo.setOrderSum(totalPrice);
//        vo.setProductId(productId);
//        vo.setBusinessId(product.getBusinessId());
//        vo.setBusinessName(business.getBusinessName());
//        vo.setProductPrice(Integer.parseInt(size.getSizePrice()));
//        vo.setProductName(product.getProductName());
//        vo.setCenterName(business.getCenterName());
//        vo.setProductTotalPrice(productTotalPrice);
//        vo.setProductName(product.getProductName());
//        vo.setNum(boughtNum);
//        vo.setShopDiscount(discountTotal);
//        vo.setSizeName(size.getSizeName());
//        vo.setProductPic(size.getSizePic());

    }
}