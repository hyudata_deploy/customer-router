package com.homepocket.hyumall.mallcustomer.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.homepocket.hyumall.mallcustomer.domain.BusinessCenterEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Author Liang
 * @Date 2020/6/15 23:17
 * @Description
 **/
@Mapper
public interface BusinessCenterMapper extends BaseMapper<BusinessCenterEntity> {
}
