package com.homepocket.hyumall.mallcustomer.utils;

import com.alibaba.fastjson.JSON;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.geo.*;
import org.springframework.data.redis.connection.RedisGeoCommands;
import org.springframework.data.redis.core.*;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * Redis工具类
 *
 * @author liang
 */
@Component
public class RedisUtils {
    @Autowired
    private RedisTemplate redisTemplate;

    @Resource(name = "redisTemplate")
    private ValueOperations<String, String> valueOperations;

    @Resource(name = "redisTemplate")
    private HashOperations<String, String, Object> hashOperations;

    @Resource(name = "redisTemplate")
    private ListOperations<String, Object> listOperations;

    @Resource(name = "redisTemplate")
    private SetOperations<String, Object> setOperations;

    @Resource(name = "redisTemplate")
    private ZSetOperations<String, Object> zSetOperations;

    @Resource(name = "redisTemplate")
    private GeoOperations<String, Object> geoOperations;

    /**
     * 默认过期时长，单位：秒
     */
    public final static long DEFAULT_EXPIRE = 60 * 60 * 24;

    /**
     * 不设置过期时长
     */
    public final static long NOT_EXPIRE = -1;

    /**
     * 插入缓存默认时间
     *
     * @param key   键
     * @param value 值
     * @author zmr
     */
    public void set(String key, Object value) {
        set(key, value, DEFAULT_EXPIRE);
    }

    /**
     * 插入缓存
     *
     * @param key    键
     * @param value  值
     * @param expire 过期时间(s)
     * @author zmr
     */
    public void set(String key, Object value, long expire) {
        valueOperations.set(key, toJson(value));
        if (expire != NOT_EXPIRE) {
            redisTemplate.expire(key, expire, TimeUnit.SECONDS);
        }
    }

    /**
     * 返回字符串结果
     *
     * @param key 键
     * @return
     * @author zmr
     */
    public String get(String key) {
        return valueOperations.get(key);
    }

    /**
     * 返回指定类型结果
     *
     * @param key   键
     * @param clazz 类型class
     * @return
     * @author zmr
     */
    public <T> T get(String key, Class<T> clazz) {
        String value = valueOperations.get(key);
        return value == null ? null : fromJson(value, clazz);
    }


    /**
     * 实现命令：HSET key field value，将哈希表 key中的域 field的值设为 value
     *
     * @param key
     * @param field
     * @param value
     */
    public void hset(String key, String field, Object value) {
        hashOperations.put(key, field, value);
    }

    /**
     * 实现命令：HGET key field，返回哈希表 key中给定域 field的值
     *
     * @param key
     * @param field
     * @return
     */
    public String hget(String key, String field) {
        return (String) hashOperations.get(key, field);
    }
    public Object hgetObj(String key, String field) {
        return hashOperations.get(key, field);
    }

    /**
     * 实现命令：HDEL key field [field ...]，删除哈希表 key 中的一个或多个指定域，不存在的域将被忽略。
     *
     * @param key
     * @param fields
     */
    public void hdel(String key, Object... fields) {
        hashOperations.delete(key, fields);
    }

    /**
     * 实现命令：HGETALL key，返回哈希表 key中，所有的域和值。
     *
     * @param key
     * @return
     */
    public Map<Object, Object> hgetall(String key) {
        return redisTemplate.opsForHash().entries(key);
    }

    // 存储Set相关操作

    /**
     * 往Set中存入数据
     *
     * @param key    Redis键
     * @param values 值
     * @return 存入的个数
     */
    public long sSet(String key, Object... values) {
        Long count = setOperations.add(key, values);
        return count == null ? 0 : count;
    }

    /**
     * 删除Set中的数据
     *
     * @param key    Redis键
     * @param values 值
     * @return 移除的个数
     */
    public long sDel(String key, Object... values) {
        Long count = setOperations.remove(key, values);
        return count == null ? 0 : count;
    }

    // 存储List相关操作

    /**
     * 往List中存入数据
     *
     * @param key   Redis键
     * @param value 数据
     * @return 存入的个数
     */
    public void lPush(String key, Object value) {
        lPush(key, value, DEFAULT_EXPIRE);
    }

    /**
     * 往List中存入数据
     *
     * @param key   Redis键
     * @param value 数据
     * @return 存入的个数
     */
    public long lPush(String key, Object value, long expire) {
        Long count = listOperations.rightPush(key, toJson(value));
        redisTemplate.expire(key, expire, TimeUnit.SECONDS);
        return count == null ? 0 : count;
    }

    /**
     * 往List中存入多个数据
     *
     * @param key    Redis键
     * @param values 多个数据
     * @return 存入的个数
     */
    public void lPushAll(String key, Collection<Object> values) {
        lPushAll(key, values, DEFAULT_EXPIRE);
    }

    /**
     * 往List中存入多个数据
     *
     * @param key    Redis键
     * @param values 多个数据
     * @return 存入的个数
     */
    public long lPushAll(String key, Collection<Object> values, long expire) {
        Long count = listOperations.rightPushAll(key, toJson(values));
        redisTemplate.expire(key, expire, TimeUnit.SECONDS);
        return count == null ? 0 : count;
    }

    /**
     * 往List中存入多个数据
     *
     * @param key    Redis键
     * @param values 多个数据
     * @return 存入的个数
     */
    public long lPushAll(String key, Object... values) {
        Long count = listOperations.rightPushAll(key, values);
        return count == null ? 0 : count;
    }

    /**
     * 从List中获取begin到end之间的元素
     *
     * @param key   Redis键
     * @param start 开始位置
     * @param end   结束位置（start=0，end=-1表示获取全部元素）
     * @return List对象
     */
    public List<Object> lGet(String key, int start, final int end) {
        return listOperations.range(key, start, end);
    }


    /**
     * 删除缓存
     *
     * @param key 键
     * @author zmr
     */
    public void delete(String key) {
        redisTemplate.delete(key);
    }

    /**
     * 添加地理位置
     *
     * @param key
     * @param lng
     * @param lat
     * @param m
     */
    public void geoAdd(String key, Double lng, Double lat, String m) {
        geoOperations.add(key, new Point(lng, lat), m);
    }

    /**
     * 根据经纬度检索附近范围的点
     *
     * @param key
     * @param lng
     * @param lat
     */
    public Map<Long, Double> nearByXY(String key, Double distance, Double lng, Double lat) {
//        Circle circle = new Circle(lat, lng, Metrics.KILOMETERS.getMultiplier());
        GeoOperations<String, Object> ops = redisTemplate.opsForGeo();
        Point center = new Point(lng, lat);
        Distance radius = new Distance(distance, Metrics.KILOMETERS);
        Circle circle = new Circle(center, radius);
//        RedisGeoCommands.GeoRadiusCommandArgs args = RedisGeoCommands.GeoRadiusCommandArgs.newGeoRadiusArgs().includeDistance().includeCoordinates().sortAscending().limit(5);
        RedisGeoCommands.GeoRadiusCommandArgs args = RedisGeoCommands.GeoRadiusCommandArgs.newGeoRadiusArgs().includeDistance().sortAscending().limit(50);
        GeoResults<RedisGeoCommands.GeoLocation<Object>> results = ops.radius(key, circle, args);
//        GeoResults<RedisGeoCommands.GeoLocation<Object>> results = geoOperations.radius(key, circle, args);
        //HashMap自动对key进行排序，不能用它
//        Map<Long, Double> data = new HashMap<>();
        System.out.println("ops:" + ObjectUtils.anyNotNull(ops) + " key:" + key + " circle:" + circle + " args:" + ObjectUtils.anyNotNull(args));
        Map<Long, Double> data = new LinkedHashMap<>();
        if (null != results) {
            results.forEach(item -> {
                data.put(Long.valueOf(item.getContent().getName().toString()), item.getDistance().getValue());
            });
        }
        return data;
    }

    /**
     * 计算两点距离
     *
     * @param member1
     * @param member2
     * @return
     */
    public Double geoDist(String member1, String member2) {
        Distance distance = geoOperations.distance("orderGeo", member1, member2, RedisGeoCommands.DistanceUnit.KILOMETERS);
        BigDecimal bd = new BigDecimal(distance.getValue());
        Double tem = bd.setScale(1, BigDecimal.ROUND_FLOOR).doubleValue();
        return tem;
    }

    /**
     * 删除位置
     *
     * @param key
     * @param member
     * @return
     */
    public Long geoDel(String key, String member) {
        return geoOperations.remove(key, member);
    }

    /**
     * Object转成JSON数据
     */
    private String toJson(Object object) {
        if (object instanceof Integer || object instanceof Long || object instanceof Float || object instanceof Double
                || object instanceof Boolean || object instanceof String) {
            return String.valueOf(object);
        }
        return JSON.toJSONString(object);
    }

    /**
     * JSON数据，转成Object
     */
    private <T> T fromJson(String json, Class<T> clazz) {
        return JSON.parseObject(json, clazz);
    }
}