package com.homepocket.hyumall.mallcustomer.utils;

import org.springframework.web.multipart.MultipartFile;

import java.io.File;

/**
 * @author niy
 * @date 2020/7/22 15:04
 **/
public class UploadUtils {

    public static String uploadFile(MultipartFile file , String userId , String filepath , String type){

        String name = file.getOriginalFilename();
        int length = name.lastIndexOf(".");
        String filename = filepath+type+ userId + name.substring(length,name.length());

        File localFile = new File(filepath);
        if (!localFile.exists()) {
            localFile.mkdirs();
        }
        try {
            file.transferTo(new File(filename));
            return type+userId + name.substring(length,name.length());
        }catch (Exception e){
            e.printStackTrace();
            return "";
        }

    }
}
