package com.homepocket.hyumall.mallcustomer.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import com.homepocket.hyumall.common.domain.User;


/**
 * @Author Anson
 * @Date 2020/6/18
 * @Description
 **/
@TableName("hyu_user")
public class UserEntity extends User {
}
