package com.homepocket.hyumall.mallcustomer.domain;

/**
 * @author Liang
 * @date 2020/10/26 11:23
 **/
public class ExtendParamsEntity {

    //卡类型
    private String cardType;

    //使用花呗分期要进行的分期数
    private String hbFqNum;

    //使用花呗分期需要卖家承担的手续费比例的百分值，传入100代表100%
    private String hbFqSellerPercent;

    //行业数据回流信息, 详见：地铁支付接口参数补充说明
    private String industryRefluxInfo;

    //系统商编号 该参数作为系统商返佣数据提取的依据，请填写系统商签约协议的PID
    private String sysServiceProviderId;

    public String getCardType() {
        return cardType;
    }

    public void setCardType(String cardType) {
        this.cardType = cardType;
    }

    public String getHbFqNum() {
        return hbFqNum;
    }

    public void setHbFqNum(String hbFqNum) {
        this.hbFqNum = hbFqNum;
    }

    public String getHbFqSellerPercent() {
        return hbFqSellerPercent;
    }

    public void setHbFqSellerPercent(String hbFqSellerPercent) {
        this.hbFqSellerPercent = hbFqSellerPercent;
    }

    public String getIndustryRefluxInfo() {
        return industryRefluxInfo;
    }

    public void setIndustryRefluxInfo(String industryRefluxInfo) {
        this.industryRefluxInfo = industryRefluxInfo;
    }

    public String getSysServiceProviderId() {
        return sysServiceProviderId;
    }

    public void setSysServiceProviderId(String sysServiceProviderId) {
        this.sysServiceProviderId = sysServiceProviderId;
    }
}
