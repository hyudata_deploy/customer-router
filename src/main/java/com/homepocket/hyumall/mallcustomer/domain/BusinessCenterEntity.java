package com.homepocket.hyumall.mallcustomer.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import com.homepocket.hyumall.common.domain.BusinessCenter;

/**
 * @Author Liang
 * @Date 2020/6/15 23:18
 * @Description
 **/
@TableName("hyu_business_center")
public class BusinessCenterEntity extends BusinessCenter {
}
