package com.homepocket.hyumall.mallcustomer.domain;

import com.alipay.api.domain.SettleInfo;

import java.util.List;

/**
 * @author Liang
 * @date 2020/10/26 10:43
 **/
public class OrderDetailEntity {

    //订单明细的应用唯一标识（16位纯数字），指商家的app_id。
    private String appId;

    //	对交易或商品的描述
    private String body;

    //业务扩展参数
    private ExtendParamsEntity extendParams;

    //订单包含的商品列表信息.Json格式.
    //其它说明详见：“商品明细说明”
    private List<GoodsDetailEntity> goodsDetail;

    //商户订单号,64个字符以内、只能包含字母、数字、下划线；需保证在商户端不重复
    private String outTradeNo;

    //公用回传参数，如果请求时传递了该参数，则返回给商户时会回传该参数。支付宝只会在同步返回（包括跳转回商户网站）和异步通知时将该参数原样返回。本参数必须进行UrlEncode之后才可以发送给支付宝。
    private String passbackParams;

    //销售产品码，与支付宝签约的产品码名称
    private String productCode;

    //卖家支付宝用户ID。
    //如果该值为空，则默认为商户签约账号对应的支付宝用户ID
    private String sellerId;

    //支持手机和Email格式,如果同时使用参数seller_logon_id和seller_id,以seller_id为准
    private String sellerLogonId;

    //描述结算信息，json格式，详见结算参数说明;
    //直付通场景下必传
    private SettleInfo settleInfo;

    //商品的展示地址
    private String showUrl;

    //二级商户信息，当前只对直付通特定场景下使用此字段
    private SubMerchantEntity subMerchant;

    //订单标题
    private String subject;

    //订单总金额，单位为元，精确到小数点后两位，取值范围[0.01,100000000]
    private String totalAmount;

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public ExtendParamsEntity getExtendParams() {
        return extendParams;
    }

    public void setExtendParams(ExtendParamsEntity extendParams) {
        this.extendParams = extendParams;
    }

    public List<GoodsDetailEntity> getGoodsDetail() {
        return goodsDetail;
    }

    public void setGoodsDetail(List<GoodsDetailEntity> goodsDetail) {
        this.goodsDetail = goodsDetail;
    }

    public String getOutTradeNo() {
        return outTradeNo;
    }

    public void setOutTradeNo(String outTradeNo) {
        this.outTradeNo = outTradeNo;
    }

    public String getPassbackParams() {
        return passbackParams;
    }

    public void setPassbackParams(String passbackParams) {
        this.passbackParams = passbackParams;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getSellerId() {
        return sellerId;
    }

    public void setSellerId(String sellerId) {
        this.sellerId = sellerId;
    }

    public String getSellerLogonId() {
        return sellerLogonId;
    }

    public void setSellerLogonId(String sellerLogonId) {
        this.sellerLogonId = sellerLogonId;
    }

    public SettleInfo getSettleInfo() {
        return settleInfo;
    }

    public void setSettleInfo(SettleInfo settleInfo) {
        this.settleInfo = settleInfo;
    }

    public String getShowUrl() {
        return showUrl;
    }

    public void setShowUrl(String showUrl) {
        this.showUrl = showUrl;
    }

    public SubMerchantEntity getSubMerchant() {
        return subMerchant;
    }

    public void setSubMerchant(SubMerchantEntity subMerchant) {
        this.subMerchant = subMerchant;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(String totalAmount) {
        this.totalAmount = totalAmount;
    }
}
