package com.homepocket.hyumall.mallcustomer.domain;

import java.util.List;

/**
 * @author Liang
 * @date 2020/10/26 10:39
 **/
public class AliPayEntity {

    //父订单Id
    //如果已经和支付宝约定要求子订单明细必须同时支付成功或者同时支付失败则必须传入此参数，且该参数必须在商户端唯一，否则可以不需要填。
    private String outMergeNo;

    //请求合并的所有订单允许的最晚付款时间，逾期将关闭交易。取值范围：1m～15d。m-分钟，h-小时，d-天，1c-当天
    // （1c-当天的情况下，无论交易何时创建，都在0点关闭）。 该参数数值不接受小数点， 如 1.5h，可转换为90m。
    //如果已经和支付宝约定要求子订单明细必须同时支付成功或者同时支付失败，则不需要填入该字段。
    private String timeoutExpress;

    //子订单详情
    private List<OrderDetailEntity> orderDetails;

    public String getOutMergeNo() {
        return outMergeNo;
    }

    public void setOutMergeNo(String outMergeNo) {
        this.outMergeNo = outMergeNo;
    }

    public String getTimeoutExpress() {
        return timeoutExpress;
    }

    public void setTimeoutExpress(String timeoutExpress) {
        this.timeoutExpress = timeoutExpress;
    }

    public List<OrderDetailEntity> getOrderDetails() {
        return orderDetails;
    }

    public void setOrderDetails(List<OrderDetailEntity> orderDetails) {
        this.orderDetails = orderDetails;
    }
}
