package com.homepocket.hyumall.mallcustomer.domain;

/**
 * @author Liang
 * @date 2020/10/26 11:23
 **/
public class SubMerchantEntity {

    //间连受理商户的支付宝商户编号，通过间连商户入驻后得到。间连业务下必传，并且需要按规范传递受理商户编号。
    private String merchantId;

    //商户id类型
    private String merchantType;

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public String getMerchantType() {
        return merchantType;
    }

    public void setMerchantType(String merchantType) {
        this.merchantType = merchantType;
    }
}
