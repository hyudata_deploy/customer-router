package com.homepocket.hyumall.mallcustomer.enums;

/**
 * @author Liang
 * @date 2020/7/18 15:35
 **/
public enum  CalculateErrorEnum {


    // 当前地址不支持配送
    AddressSupportError(-1,"当前地址不支持配送"),
    // 地址选择错误
    AddressSelectError(-2,"地址选择错误"),
    // 商品活动类型错误
    OnsaleError(-3,"商品活动类型错误"),
    AddressNuLLError(-4,"您当前没有收货地址 请先添加"),
//    NumberError(-5,"商品数量不足"),
    // 商品数量不足
    NumberError(-6,"商品数量不足");

    private Integer code;
    private String message;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    CalculateErrorEnum(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public static String getMessage(Integer code) {
        if (code != null) {
            for (CalculateErrorEnum u : CalculateErrorEnum.values()) {
                if (u.getCode().equals(code)) {
                    return u.message;
                }
            }
        }
        return null;
    }
}
