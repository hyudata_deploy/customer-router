package com.homepocket.hyumall.mallcustomer;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;


@EnableFeignClients
@SpringBootApplication(scanBasePackages = "com.homepocket.hyumall")
public class MallCustomerApplication {

    public static void main(String[] args) {
        SpringApplication.run(MallCustomerApplication.class, args);
    }

}
